package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import capa_Logica.DarseDeBaja;

import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.beans.PropertyChangeEvent;

public class DarseDeBajaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfID;
	private JTextField tfNombre;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final Action action = new SwingAction();
	private final Action action_1 = new SwingAction_1();

	private DarseDeBajaGUI ventanaBaja =null;
	private JButton bComprobar;

	private JLabel lNombre;
	private JLabel lTitulacion;
	private JLabel lFechaAlta;
	private JLabel lListas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DarseDeBajaGUI frame = new DarseDeBajaGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DarseDeBajaGUI() {
		setTitle("Darse de baja");
		ventanaBaja=this;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		tfID = new JTextField();
		tfID.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {

				limpiar();

			}
		});
		tfID.setBounds(207, 79, 151, 22);
		contentPane.add(tfID);
		tfID.setColumns(10);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 126, 621, 12);
		contentPane.add(separator);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(91, 151, 56, 16);
		contentPane.add(lblNombre);

		JLabel lblTitulacin = new JLabel("Titulaci\u00F3n:");
		lblTitulacin.setBounds(91, 180, 79, 16);
		contentPane.add(lblTitulacin);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 292, 621, 12);
		contentPane.add(separator_1);



		JLabel lid = new JLabel("ID:");
		lid.setBounds(152, 82, 56, 16);
		contentPane.add(lid);

		lNombre = new JLabel("--");
		lNombre.setBounds(182, 151, 348, 16);
		contentPane.add(lNombre);

		lFechaAlta = new JLabel("--");
		lFechaAlta.setBounds(182, 209, 196, 16);
		contentPane.add(lFechaAlta);

		lListas = new JLabel("--");
		lListas.setBounds(182, 238, 277, 16);
		contentPane.add(lListas);

		lTitulacion = new JLabel("--");
		lTitulacion.setBounds(182, 180, 348, 16);
		contentPane.add(lTitulacion);

		bComprobar = new JButton("Comprobar");
		bComprobar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Busca en la base de datos el ID
				if (!DarseDeBaja.comprobarID(tfID.getText().trim(),lNombre,lTitulacion,lFechaAlta, lListas)) {
					JOptionPane.showMessageDialog(null,"No existe ningun colegiado con ese DNI" ,"Error " , JOptionPane.ERROR_MESSAGE);
				}



			}
		});
		bComprobar.setBounds(376, 78, 116, 25);
		contentPane.add(bComprobar);





		JButton bBaja = new JButton("Darse de baja");
		bBaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {


				if(lNombre.getText().trim().equals("--")) {
					JOptionPane.showMessageDialog(null,"Debe seleccionar un colegiado con el boton de 'Comprobar' o 'Buscar'" ,"Selecciona colegiado " , JOptionPane.ERROR_MESSAGE);
				}
				else {

					if(JOptionPane.showConfirmDialog(null, "�Est� seguro? Esta acci�n borrar� de todas las listas al colegiado","Confirmar",JOptionPane.YES_NO_OPTION)==0) {

						if (DarseDeBaja.darBaja(tfID.getText().trim())) {
							
							DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
							String fecha_baja = df.format(new Date()).toString(); // Fecha actual

							JOptionPane.showMessageDialog(null,"El colegiado"+lNombre.getText()+" , n�mero de colegiado '"+tfID.getText()+ "' se ha dado de baja con exito en el dia "+fecha_baja+". \nHa sido borrado de todas las listas.\n" ,"Exito al darse de baja " , JOptionPane.PLAIN_MESSAGE);
							limpiar();
							tfID.setText("");
							tfNombre.setText("");

						}
						else {

							JOptionPane.showMessageDialog(null,"No se ha podido dar de baja" ,"Error " , JOptionPane.ERROR_MESSAGE);
						}
					}

				}

			}
		});
		bBaja.setBounds(207, 308, 203, 32);
		contentPane.add(bBaja);

		JLabel lblFechaDeAlta = new JLabel("Fecha de alta:");
		lblFechaDeAlta.setBounds(91, 209, 82, 16);
		contentPane.add(lblFechaDeAlta);

		JLabel lblListas = new JLabel("Listas:");
		lblListas.setBounds(91, 238, 56, 16);
		contentPane.add(lblListas);

		JButton bBuscar = new JButton("Buscar");
		bBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				new DarseDeBajaGUI2(ventanaBaja).setVisible(true);


			}
		});
		bBuscar.setBounds(376, 26, 116, 25);
		contentPane.add(bBuscar);

		tfNombre = new JTextField();
		tfNombre.setBounds(207, 27, 151, 22);
		contentPane.add(tfNombre);
		tfNombre.setColumns(10);

		JLabel lblNombreYApellidos = new JLabel("Nombre y apellidos:");
		lblNombreYApellidos.setBounds(80, 30, 116, 16);
		contentPane.add(lblNombreYApellidos);

		JRadioButton rbNombre = new JRadioButton("");
		buttonGroup.add(rbNombre);
		rbNombre.setBounds(47, 26, 25, 25);
		contentPane.add(rbNombre);

		JRadioButton rbID = new JRadioButton("");
		rbID.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {

				boolean state=rbID.isSelected();	

				tfID.setEnabled(state);
				bComprobar.setEnabled(state);

				tfNombre.setEnabled(!state);
				bBuscar.setEnabled(!state);

				if (state)
					tfNombre.setText("");
				else
					tfID.setText("");



			}
		});
		rbID.setAction(action);
		buttonGroup.add(rbID);
		rbID.setSelected(true);
		rbID.setBounds(47, 79, 25, 25);
		contentPane.add(rbID);



		tfID.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				limpiar();

			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				limpiar();

			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				limpiar();

			}
		});

		tfNombre.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				limpiar();
				tfID.setText("");

			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				limpiar();
				tfID.setText("");
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				limpiar();
				tfID.setText("");

			}
		});


	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "SwingAction_1");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}

	public void ponerID(String id) {

		tfID.setText(id);
		buscarID(lNombre,lTitulacion,lFechaAlta,lListas);


	}
	public void buscarID(JLabel Nom, JLabel Tit, JLabel Fech, JLabel List) {
		//Busca en la base de datos el ID
		if (!DarseDeBaja.comprobarID(tfID.getText().trim(),Nom,Tit,Fech, List)) {
			JOptionPane.showMessageDialog(null,"No existe ningun colegiado con ese DNI" ,"Error " , JOptionPane.ERROR_MESSAGE);
		}
	}

	public void limpiar() {
		lNombre.setText("--");
		lTitulacion.setText("--");
		lFechaAlta.setText("--");
		lListas.setText("--");

	}

	public String getNombre() {
		return tfNombre.getText();
	}
}
