package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import capa_Datos.DriverDB;
import capa_Logica.NuevaEdicionCurso;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import java.awt.Color;
import javax.swing.border.BevelBorder;

public class NuevaEdicionCursoGUI extends JFrame {

	private final DefaultListModel model=new DefaultListModel();

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NuevaEdicionCursoGUI frame = new NuevaEdicionCursoGUI();
					frame.setVisible(true);


				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NuevaEdicionCursoGUI() {
		setResizable(false);
		setTitle("Crear nueva edicion de un curso");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 572, 498);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);


		String listaCursos[]=null;




		JLabel lblSeleccionarCurso = new JLabel("Seleccionar curso:");
		lblSeleccionarCurso.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSeleccionarCurso.setBounds(67, 24, 130, 16);
		panel.add(lblSeleccionarCurso);

		JList lCursos = new JList();
		

		JButton bCrearEdicion = new JButton("Crear nueva edicion del curso");
		bCrearEdicion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { //llama a crear curso pero con datos del seleccionado

				if(lCursos.getSelectedValue()==null) {
					JOptionPane.showMessageDialog(null,"Debe elegir un curso" ,"Error " , JOptionPane.ERROR_MESSAGE);
				}
				else {
					
					System.out.println(lCursos.getSelectedValue());
					if(!NuevaEdicionCurso.callCrearCurso((String) lCursos.getSelectedValue()))
						JOptionPane.showMessageDialog(null,"Error con la base de datos" ,"Error " , JOptionPane.ERROR_MESSAGE);
					
					
				}

			}
		});
		bCrearEdicion.setBounds(146, 387, 251, 41);
		panel.add(bCrearEdicion);


		lCursos.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lCursos.setBackground(Color.WHITE);
		lCursos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lCursos.setBounds(88, 53, 389, 305);
		panel.add(lCursos);
		//

		lCursos.setModel(model);

		NuevaEdicionCurso.obtenerCursos(model);


	}
}
