package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.TextArea;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JustificanteAltaPerito {

	public JFrame frame;
	public static JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JustificanteAltaPerito window = new JustificanteAltaPerito();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JustificanteAltaPerito() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblJustificanteDeInscripcin = new JLabel("Justificante de inscripci\u00F3n");
		lblJustificanteDeInscripcin.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblJustificanteDeInscripcin.setBounds(109, 11, 220, 14);
		frame.getContentPane().add(lblJustificanteDeInscripcin);
		
		Button bImprimir = new Button("Imprimir");
		bImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		bImprimir.setBounds(286, 217, 70, 22);
		frame.getContentPane().add(bImprimir);
		
		Button bAceptar = new Button("Aceptar");
		bAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		bAceptar.setBounds(73, 217, 70, 22);
		frame.getContentPane().add(bAceptar);
		
		textArea = new JTextArea();
		textArea.setBounds(29, 36, 363, 157);
		frame.getContentPane().add(textArea);
	}
}
