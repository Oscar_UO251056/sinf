package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import capa_Logica.BalanceEconomico;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JSplitPane;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class BalanceEconomicoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfAgno;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BalanceEconomicoGUI frame = new BalanceEconomicoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BalanceEconomicoGUI() {
		setResizable(false);
		setTitle("Balance econ\u00F3mico");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 636, 548);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblSeleccionarAo = new JLabel("Seleccionar a\u00F1o:");
		lblSeleccionarAo.setBounds(89, 45, 121, 16);
		contentPane.add(lblSeleccionarAo);

		tfAgno = new JTextField();
		tfAgno.setBounds(222, 42, 116, 22);
		contentPane.add(tfAgno);
		tfAgno.setColumns(10);

		JTextArea taInfo = new JTextArea();
		taInfo.setFont(new Font("Monospaced", Font.BOLD, 15));
		taInfo.setEditable(false);
		taInfo.setBounds(12, 128, 606, 372);
		contentPane.add(taInfo);

		JLabel lTardar = new JLabel("*Esta operacion puede tardar unos segundos");
		lTardar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lTardar.setBounds(12, 99, 320, 16);
		contentPane.add(lTardar);
		lTardar.setVisible(false);

		JButton bAceptar = new JButton("Aceptar");
		bAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if(bAceptar.isEnabled()) {

					bAceptar.setEnabled(false);
					lTardar.setVisible(true);

					String agno=tfAgno.getText().trim();
					if(BalanceEconomico.isValidYear( agno )  ) {

						if(BalanceEconomico.isSameYear(agno) ) {
							JOptionPane.showMessageDialog(null,"El a�o seleccionado es el mismo que el actual.\nAl no haber finalizado el a�o los resultados obtenidos pueden variar." ,"A�o actual " , JOptionPane.WARNING_MESSAGE);
						}	

						taInfo.setText(BalanceEconomico.balanceTotal(agno)) ;

					}
					else {

						JOptionPane.showMessageDialog(null,"Introduzca un a�o valido 'YYYY' \n(N�merico,menor que el a�o actual y no negativo)" ,"Error " , JOptionPane.ERROR_MESSAGE);
					}

					
					lTardar.setVisible(false);
					bAceptar.setEnabled(true);

				}
			}
		});
		bAceptar.setBounds(359, 41, 97, 25);
		contentPane.add(bAceptar);




	}
}
