package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.TextArea;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.List;
import javax.swing.JList;
import javax.swing.JOptionPane;

import capa_Logica.EnviarLoteVerificacion;
import java.awt.Font;
import javax.swing.JScrollPane;

public class EnviarLoteVerificacionGUI {


	//private final DefaultListModel model=new DefaultListModel();
	private JFrame frEnviarLote;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EnviarLoteVerificacionGUI window = new EnviarLoteVerificacionGUI();
					window.frEnviarLote.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EnviarLoteVerificacionGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frEnviarLote = new JFrame();
		frEnviarLote.setTitle("Enviar Lote Verificacion Ministerio");
		frEnviarLote.setBounds(100, 100, 679, 398);
		frEnviarLote.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frEnviarLote.getContentPane().setLayout(null);

		DefaultListModel<String> modelo=new DefaultListModel<String>();

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 69, 226, 253);
		frEnviarLote.getContentPane().add(scrollPane);

		JList lPrecolegiados = new JList();
		scrollPane.setViewportView(lPrecolegiados);
		lPrecolegiados.setModel(modelo);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(425, 69, 216, 253);
		frEnviarLote.getContentPane().add(scrollPane_1);
		JList lEnviarLote = new JList();
		scrollPane_1.setViewportView(lEnviarLote);

		JLabel lblListaPrecolegiados = new JLabel("Lista colegiados pendiente de verificaci\u00F3n");
		lblListaPrecolegiados.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblListaPrecolegiados.setBounds(10, 23, 265, 14);
		frEnviarLote.getContentPane().add(lblListaPrecolegiados);

		JLabel lblLoteParaVerificar = new JLabel("Lote para verificar");
		lblLoteParaVerificar.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblLoteParaVerificar.setBounds(425, 23, 136, 14);
		frEnviarLote.getContentPane().add(lblLoteParaVerificar);





		//LISTA DE PRE-COLEGIADOS PENDIENTES DE CONFIRMACION 
		EnviarLoteVerificacion logica=new EnviarLoteVerificacion();
		ArrayList<String> precolegiados=new ArrayList<String>();
		precolegiados=logica.ListaPrecolegiados();

		for (int i=0; i<precolegiados.size();i++){
			modelo.addElement(precolegiados.get(i));

		}

		//LISTA PARA ENVIAR LOTE AL MINISTERIO
		DefaultListModel<String> modelo2=new DefaultListModel<String>();

		//BOTON MOVER A LA DERECHA
		JButton bDerecha = new JButton(">");
		bDerecha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					//Obtenemos la seleccion
					int i=lPrecolegiados.getSelectedIndex();
					//Lo a�adimos a la otra lista
					modelo2.addElement(modelo.getElementAt(i));
					//Borramos de la otra
					modelo.remove(i);
					lPrecolegiados.setModel(modelo);
					lEnviarLote.setModel(modelo2);
				}catch(Exception e){
					System.out.println("Debe de seleccionar un elemento");
				}
			}
		});
		bDerecha.setBounds(276, 67, 110, 23);
		frEnviarLote.getContentPane().add(bDerecha);

		//MOVER A LA IZQUIERDA
		JButton bIzquierda = new JButton("<");
		bIzquierda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					//Obtenemos la seleccion
					int i=lEnviarLote.getSelectedIndex();
					//Lo a�adimos a la otra lista
					modelo.addElement(modelo2.getElementAt(i));
					//lEnviarLote.add(lPrecolegiados.getSelectedIndices());
					//Borramos el elemento de la lista 
					modelo2.remove(i);
					lPrecolegiados.setModel(modelo);
					lEnviarLote.setModel(modelo2);
				}catch(Exception e){
					System.out.println("Debe de seleccionar un elemento");
				}
			}
		});
		bIzquierda.setBounds(276, 101, 110, 23);
		frEnviarLote.getContentPane().add(bIzquierda);

		//Todos los pre-colegiados pasaran a la lista de enviar lote para el ministerio
		JButton bTodos = new JButton("Todos");
		bTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i=0; i< modelo.size();i++){
					modelo2.addElement(modelo.getElementAt(i));

				}
				modelo.removeAllElements();
				lPrecolegiados.setModel(modelo);
				lEnviarLote.setModel(modelo2);

			}
		});
		bTodos.setBounds(276, 131, 110, 23);
		frEnviarLote.getContentPane().add(bTodos);

		JButton bNinguno = new JButton("Ninguno");
		bNinguno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i=0; i< modelo2.size();i++){
					modelo.addElement(modelo2.getElementAt(i));

				}
				modelo2.removeAllElements();
				lPrecolegiados.setModel(modelo);
				lEnviarLote.setModel(modelo2);

			}
		});
		bNinguno.setBounds(278, 165, 108, 23);
		frEnviarLote.getContentPane().add(bNinguno);



		//BOTON ENVIAR LOTE AL MINISTERIO

		JButton btnEnviarLote = new JButton("Enviar Lote");
		btnEnviarLote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> LoteMinisterio=new ArrayList<String>();
				EnviarLoteVerificacion logica=new EnviarLoteVerificacion();

				for(int i=0; i<modelo2.size();i++){
					LoteMinisterio.add(modelo2.getElementAt(i));

				}
				modelo2.removeAllElements();
				lEnviarLote.setModel(modelo2);
				//LoteMinisterio METER A UN FICHERO
				if(logica.enviarLote(LoteMinisterio)){
					JOptionPane.showMessageDialog(null,"Se ha creado el fichero en la carpeta 'LoteMinisterio'." ,"Exito al crear lote a enviar " , JOptionPane.PLAIN_MESSAGE);
				}
				else{
					JOptionPane.showMessageDialog(null,"No se ha podido crear el fichero con el lote." ,"Error" , JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnEnviarLote.setBounds(278, 199, 108, 23);
		frEnviarLote.getContentPane().add(btnEnviarLote);

		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frEnviarLote.dispose();
			}
		});
		btnSalir.setBounds(278, 233, 108, 23);
		frEnviarLote.getContentPane().add(btnSalir);
		
		JLabel lblNombrednititulacionaoObtencion = new JLabel("Nombre-DNI-Titulacion-A\u00F1o obtencion");
		lblNombrednititulacionaoObtencion.setFont(new Font("Tahoma", Font.ITALIC, 10));
		lblNombrednititulacionaoObtencion.setBounds(10, 44, 226, 14);
		frEnviarLote.getContentPane().add(lblNombrednititulacionaoObtencion);
		
		JLabel label = new JLabel("Nombre-DNI-Titulacion-A\u00F1o obtencion");
		label.setFont(new Font("Tahoma", Font.ITALIC, 10));
		label.setBounds(425, 44, 226, 14);
		frEnviarLote.getContentPane().add(label);




	}
}
