package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import capa_Logica.RegistroCuotas;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class RegistroCuotasGUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfRuta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistroCuotasGUI frame = new RegistroCuotasGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistroCuotasGUI() {
		setTitle("Registrar los pagos de las cuotas");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 531, 251);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton bAbrir = new JButton("Abrir"); //Para elegir archivo
		bAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(bAbrir);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					System.out.println(file.getPath());
					System.out.println(file.getName());


					if( RegistroCuotas.isCSV(file.getName()) ) {

						tfRuta.setText(file.getPath());		
					}
					else {
						JOptionPane.showMessageDialog(null,"El fichero debe ser .CSV'." ,"Error " , JOptionPane.ERROR_MESSAGE);
					}

				} else {
					JOptionPane.showMessageDialog(null,"Al abrir el archivo'." ,"Error " , JOptionPane.ERROR_MESSAGE);

				}



			}
		});
		bAbrir.setBounds(313, 61, 78, 24);
		panel.add(bAbrir);

		tfRuta = new JTextField();
		tfRuta.setEditable(false);
		tfRuta.setBounds(88, 61, 222, 24);
		panel.add(tfRuta);
		tfRuta.setColumns(10);

		JButton btnVerRegistros = new JButton("Ver registros");
		btnVerRegistros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				RegistroCuotasGUI2.main(null);
				
			}
		});
		btnVerRegistros.setBounds(271, 132, 120, 25);
		panel.add(btnVerRegistros);

		JButton btnNewButton = new JButton("Registar pagos");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				switch ( RegistroCuotas.leerFichero(tfRuta.getText())   ) {
				case -1: JOptionPane.showMessageDialog(null,"El fichero esta vacio" ,"Error " , JOptionPane.ERROR_MESSAGE);
					break;
				case 0:  JOptionPane.showMessageDialog(null,"Exito al actualizar los recibos e incidencias" ,"Exito " , JOptionPane.INFORMATION_MESSAGE);
					break;
				case 1:  JOptionPane.showMessageDialog(null,"Error al leer el fichero" ,"Error " , JOptionPane.ERROR_MESSAGE);

					break;
				case 2:	 JOptionPane.showMessageDialog(null,"EL recibo no existe en la base de datos" ,"Error " , JOptionPane.ERROR_MESSAGE);
					break;
					
				case 3:  JOptionPane.showMessageDialog(null,"Error al escribir en el fichero de incidencias" ,"Error " , JOptionPane.ERROR_MESSAGE);
				}
			
				
			}
		});
		btnNewButton.setBounds(88, 132, 145, 25);
		panel.add(btnNewButton);

		JLabel lblRuta = new JLabel("Ruta:");
		lblRuta.setBounds(88, 44, 42, 16);
		panel.add(lblRuta);
	}
}
