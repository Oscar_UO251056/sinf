//HISTORIA 7

package capa_Presentacion;

//import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import capa_Logica.EmisionRecibo;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class EmisionReciboGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2487491240846519891L;
	private JPanel contentPane;
	private JTextField tfNombreFichero;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmisionReciboGUI frame = new EmisionReciboGUI();
					frame.setVisible(true);				
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EmisionReciboGUI() {
		setTitle("Recibos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 241);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JButton btnGenerarRecibos = new JButton("Generar recibos");
		
		btnGenerarRecibos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(EmisionRecibo.escribirRecibos(tfNombreFichero.getText()))
					JOptionPane.showMessageDialog(null,"Se ha creado el fichero en la carpeta 'Cobros'." ,"Exito al crear recibos " , JOptionPane.PLAIN_MESSAGE);

				else
					JOptionPane.showMessageDialog(null,"No se ha podido crear el fichero con los recibos." ,"Error" , JOptionPane.ERROR_MESSAGE);

					
				
				
				
				
			}
		});
		btnGenerarRecibos.setBounds(111, 119, 207, 34);
		contentPane.add(btnGenerarRecibos);
		
		tfNombreFichero = new JTextField();
		tfNombreFichero.setBounds(203, 55, 159, 22);
		contentPane.add(tfNombreFichero);
		tfNombreFichero.setColumns(10);
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha_actual = df.format(new Date()).toString(); 
		tfNombreFichero.setText(fecha_actual);
		
		
		JLabel lblNombreDelFichero = new JLabel("Nombre del fichero:");
		lblNombreDelFichero.setBounds(54, 58, 116, 16);
		contentPane.add(lblNombreDelFichero);
		
		
		
	}
}
