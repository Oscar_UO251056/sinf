package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.TextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;

public class JustificanteVerificarColegiados {

	public static JFrame frame;//
	public static JList<String> lJustificante;
	public static JList<String> lCancelado;
	public static JList<String> lInscrito;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JustificanteVerificarColegiados window = new JustificanteVerificarColegiados();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JustificanteVerificarColegiados() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Justificante");
		frame.setBounds(100, 100, 665, 308);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("SALIR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnNewButton.setBounds(10, 211, 629, 23);
		frame.getContentPane().add(btnNewButton);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 48, 188, 138);
		frame.getContentPane().add(scrollPane);

		lJustificante = new JList<String>();
		scrollPane.setViewportView(lJustificante);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(222, 48, 194, 138);
		frame.getContentPane().add(scrollPane_1);

		lCancelado = new JList<String>();
		scrollPane_1.setViewportView(lCancelado);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(456, 48, 183, 138);
		frame.getContentPane().add(scrollPane_2);

		lInscrito = new JList<String>();
		scrollPane_2.setViewportView(lInscrito);
		
		JLabel lblSeHaVerificado = new JLabel("Se ha verificado la titulaci\u00F3n de:");
		lblSeHaVerificado.setBounds(10, 11, 194, 14);
		frame.getContentPane().add(lblSeHaVerificado);
		
		JLabel lblSeHaCancelado = new JLabel("Se ha cancelado la titulaci\u00F3n de:");
		lblSeHaCancelado.setBounds(222, 11, 194, 14);
		frame.getContentPane().add(lblSeHaCancelado);
		
		JLabel lblInscritosConAnterioridad = new JLabel("Inscritos con anterioridad:");
		lblInscritosConAnterioridad.setBounds(454, 11, 185, 14);
		frame.getContentPane().add(lblInscritosConAnterioridad);
	}
}
