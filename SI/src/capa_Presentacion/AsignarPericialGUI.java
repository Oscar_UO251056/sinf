package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSeparator;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import capa_Logica.AsignarPericial;

import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.List;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.TextArea;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class AsignarPericialGUI {

	private JFrame frmAsignarPericial;
	private JTextField tfCliente;
	private JTextField tfTelefono;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AsignarPericialGUI window = new AsignarPericialGUI();
					window.frmAsignarPericial.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AsignarPericialGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAsignarPericial = new JFrame();
		frmAsignarPericial.setTitle("Asignar pericial");
		frmAsignarPericial.setBounds(100, 100, 523, 300);
		frmAsignarPericial.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAsignarPericial.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Datos pericial");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 11, 93, 14);
		frmAsignarPericial.getContentPane().add(lblNewLabel);

		JLabel lblCliente = new JLabel("Cliente");
		lblCliente.setBounds(10, 33, 46, 14);
		frmAsignarPericial.getContentPane().add(lblCliente);

		tfCliente = new JTextField();
		tfCliente.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar();
				if (!(Character.isAlphabetic(c) || c==KeyEvent.VK_SPACE || c==KeyEvent.VK_DELETE)){
					e.consume();
					
					
				}
			}
		});
		tfCliente.setBounds(73, 36, 86, 20);
		frmAsignarPericial.getContentPane().add(tfCliente);
		tfCliente.setColumns(10);


		TextArea tfDescripcion = new TextArea();
		tfDescripcion.setBounds(10, 114, 149, 104);
		frmAsignarPericial.getContentPane().add(tfDescripcion);

		JLabel lblTelfono = new JLabel("Tel\u00E9fono");
		lblTelfono.setBounds(10, 58, 64, 14);
		frmAsignarPericial.getContentPane().add(lblTelfono);

		tfTelefono = new JTextField();
		tfTelefono.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c=e.getKeyChar();
				if (!(Character.isDigit(c)/*|| c==KeyEvent.VK_SPACE || c==KeyEvent.VK_DELETE*/)){
					e.consume();
					
					
				}
			}
		});
		tfTelefono.setBounds(72, 61, 86, 20);
		frmAsignarPericial.getContentPane().add(tfTelefono);
		tfTelefono.setColumns(10);

		JLabel lblDescripcin = new JLabel("Descripci\u00F3n");
		lblDescripcin.setBounds(10, 94, 107, 14);
		frmAsignarPericial.getContentPane().add(lblDescripcin);


		List ListaPeritos = new List();
		ListaPeritos.setBounds(342, 33, 123, 133);
		frmAsignarPericial.getContentPane().add(ListaPeritos);

		JButton btnGuardar = new JButton("Guardar y asignar");
		btnGuardar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				if (tfCliente.getText().isEmpty()||tfTelefono.getText().isEmpty()||tfDescripcion.getText().isEmpty()){
					System.out.println("Todos los campos han de estar cubiertos");
					JOptionPane.showMessageDialog(null,"Todos los campos han de estar cubiertos" ,"Error al guardar la pericial" , JOptionPane.PLAIN_MESSAGE);
				}else{
					AsignarPericial logica=new AsignarPericial();

					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String fecha_solicituds = df.format(new Date()).toString(); // Fecha actual

					int id_pericial=logica.SiguientePericial();
					int id_perito=logica.peritoAsignado();

					if (logica.GuardaPericiales(id_pericial, tfCliente.getText(), tfDescripcion.getText(), fecha_solicituds, id_perito, fecha_solicituds)){
						System.out.println("Se ha guardado la pericial y se asignara a un perito");

						//Al asignar el perito privado, debemos mover el turno
						// y mostrarlo en la lista
						//AsignarPericial logica=new AsignarPericial();
						ArrayList<String> peritos=new ArrayList<String>();

						//RotarTurnos ..
						if (logica.RotarTurnos()){
							//Sacar peritos actuales
							peritos=logica.peritosActual();

							//Ahora que tenemos los peritos los mostramos en la JList llamada ListaPeritos
							for(int i=0; i<peritos.size();i++){
								ListaPeritos.add(peritos.get(i));
							}
						}
						
						btnGuardar.setEnabled(false);
						
					}else{
						System.out.println("No se ha podido guardar la pericial y tampoco se asigna");
					}
				}
			}
		});
		btnGuardar.setBounds(177, 80, 141, 43);
		frmAsignarPericial.getContentPane().add(btnGuardar);

		JLabel lblListaPeritosPrivados = new JLabel("Lista peritos privados");
		lblListaPeritosPrivados.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblListaPeritosPrivados.setBounds(332, 11, 134, 14);
		frmAsignarPericial.getContentPane().add(lblListaPeritosPrivados);


		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAsignarPericial.dispose();
			}
		});
		btnSalir.setBounds(376, 195, 89, 23);
		frmAsignarPericial.getContentPane().add(btnSalir);
		
		JButton bNueva = new JButton("Nueva pericial");
		bNueva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tfCliente.setText("");
				tfTelefono.setText("");
				tfDescripcion.setText("");
				ListaPeritos.removeAll();
				btnGuardar.setEnabled(true);
				
			}
		});
		bNueva.setBounds(177, 180, 141, 38);
		frmAsignarPericial.getContentPane().add(bNueva);

	}
}
