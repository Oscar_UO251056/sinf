package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import capa_Logica.AltaPerito2;

import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

public class AltaPerito2GUI {

	public JFrame frame;
	public JTextField tfNsocio;
	public JTextField tfNombre;
	public JTextField tfTelefono;
	public JTextField tfAnyoPericiales;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AltaPerito2GUI window = new AltaPerito2GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AltaPerito2GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNSocio = new JLabel("N\u00BA socio:");
		lblNSocio.setBounds(23, 33, 54, 14);
		frame.getContentPane().add(lblNSocio);
		
		tfNsocio = new JTextField();
		tfNsocio.setBounds(103, 30, 86, 20);
		frame.getContentPane().add(tfNsocio);
		tfNsocio.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AltaPerito2 logica=new AltaPerito2();
				if (!AltaPerito2.isNumber(tfAnyoPericiales.getText())){
					JOptionPane.showMessageDialog(null, "El a�o debe ser un numero", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else{
				
				try{
					tfNombre.setText(logica.Nombre(tfNsocio.getText()));
					tfTelefono.setText(logica.Telefono(tfNsocio.getText()));
					}
					catch(NumberFormatException e1){
						e1.printStackTrace(); 
						}
				
				}
			}
		});
		btnOk.setBounds(290, 29, 89, 23);
		frame.getContentPane().add(btnOk);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 92, 434, 14);
		frame.getContentPane().add(separator);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(23, 132, 46, 14);
		frame.getContentPane().add(lblNombre);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono: ");
		lblTelfono.setBounds(23, 157, 70, 14);
		frame.getContentPane().add(lblTelfono);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(103, 129, 86, 20);
		frame.getContentPane().add(tfNombre);
		tfNombre.setColumns(10);
		
		tfTelefono = new JTextField();
		tfTelefono.setBounds(103, 154, 86, 20);
		frame.getContentPane().add(tfTelefono);
		tfTelefono.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("A\u00F1o periciales:");
		lblNewLabel.setBounds(23, 67, 80, 14);
		frame.getContentPane().add(lblNewLabel);
		
		tfAnyoPericiales = new JTextField();
		tfAnyoPericiales.setBounds(103, 61, 86, 20);
		frame.getContentPane().add(tfAnyoPericiales);
		tfAnyoPericiales.setColumns(10);
		
		JButton btnInscribirse = new JButton("Inscribirse");
		btnInscribirse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//FECHA ACTUAL
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String fecha_inscripcion = df.format(new Date()).toString(); 
				
				String nsocio=tfNsocio.getText();
				
				AltaPerito2 logica=new AltaPerito2();
				
				
				if(logica.introducirPerito(nsocio)){
					System.out.println("Se ha inscrito a la lista de peritos");
					
					//No se abre el justificante, hasta aqui todo perfe
					
					JustificanteAltaPerito justificante=new JustificanteAltaPerito();
					justificante.textArea.setText("El colegiado numero:"+tfNsocio.getText()+"ha realizado su solicitud en la lista de peritos a fecha de:"+fecha_inscripcion+"");
							justificante.frame.setVisible(true);
							System.out.println("Inscrito en la lista de peritos");
							
				}
				else{
					System.out.println("No se puede inscribir");
					
					
					//No se abre el justificante, hasta aqui todo perfe
					/*JustificanteAltaPerito justificante=new JustificanteAltaPerito();
					JustificanteAltaPerito.tAperitojust.setText("El colegiado numero:"+nsocio+
							"no se ha podido inscribir");
							justificante.frame.setVisible(true);*/

				}
				
				
			}
		});
		btnInscribirse.setBounds(290, 153, 89, 23);
		frame.getContentPane().add(btnInscribirse);
	}

}
