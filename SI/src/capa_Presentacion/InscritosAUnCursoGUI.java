package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import capa_Logica.InscribirseACurso;
import capa_Logica.InscritosAUnCurso;

import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.TextArea;
import java.awt.Font;

public class InscritosAUnCursoGUI {

	public JFrame frame;
	private static JTextField tfIngresos;
	public static TextArea textArea;
	public static JComboBox cb;
	public static JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InscritosAUnCursoGUI window = new InscritosAUnCursoGUI();
					window.frame.setVisible(true);
					InscritosAUnCurso logica = new InscritosAUnCurso();
					String Cursos=logica.DameCursos();
					int numeroCursos=Cursos.split("\n").length;
					for(int cont=0;cont<numeroCursos;cont++){
						cb.addItem(Cursos.split("\n")[cont]);	
						
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InscritosAUnCursoGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 673, 398);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblInscritosAlCurso = new JLabel("INSCRITOS AL CURSO");
		lblInscritosAlCurso.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblInscritosAlCurso.setBounds(227, 11, 301, 23);
		frame.getContentPane().add(lblInscritosAlCurso);
		
		JLabel lblIntroduceElN = new JLabel("N\u00BA del curso:");
		lblIntroduceElN.setBounds(60, 74, 89, 14);
		frame.getContentPane().add(lblIntroduceElN);
		
		JButton btnActualizarPagos = new JButton("Actualizar pagos");
		btnActualizarPagos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText("");
				InscritosAUnCurso logica=new InscritosAUnCurso();
				logica.ActualizarPagos(label.getText());
				logica.CancelarInscripcionesFueraPlazo(label.getText());
			}
		});
		btnActualizarPagos.setBounds(449, 205, 138, 59);
		frame.getContentPane().add(btnActualizarPagos);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(21, 136, 600, 2);
		frame.getContentPane().add(separator);
		
		JButton bComprobar = new JButton("Comprobar");
		bComprobar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscritosAUnCurso logica = new InscritosAUnCurso();
				tfIngresos.setText("");
				textArea.setText("");
				String inscritos= logica.InscritosColegiadosACurso(label.getText());
				String inscritos2= logica.InscritosExternosACurso(label.getText());
				int numeroInscritosC=inscritos.split("\n").length;
				int numeroInscritosE=inscritos2.split("\n").length;
				for(int i=0;i<numeroInscritosC;i++){
					textArea.setText(textArea.getText()+inscritos.split("\n")[i]+"\n");
				}
				for(int i=0;i<numeroInscritosE;i++){
					textArea.setText(textArea.getText()+inscritos2.split("\n")[i]+"\n");
				}
				tfIngresos.setText(""+logica.ingresos(label.getText()));
			}
		});
		bComprobar.setBounds(60, 102, 527, 23);
		frame.getContentPane().add(bComprobar);
		
		JLabel lblNombreestadoprecio = new JLabel("Nombre-Estado-Precio");
		lblNombreestadoprecio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombreestadoprecio.setBounds(126, 149, 221, 14);
		frame.getContentPane().add(lblNombreestadoprecio);
		
		JLabel lblIngresosActuales = new JLabel("Ingresos actuales");
		lblIngresosActuales.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIngresosActuales.setBounds(462, 149, 148, 14);
		frame.getContentPane().add(lblIngresosActuales);
		
		tfIngresos = new JTextField();
		tfIngresos.setBounds(449, 174, 138, 20);
		frame.getContentPane().add(tfIngresos);
		tfIngresos.setColumns(10);
		
		JButton bAceptar = new JButton("Aceptar");
		bAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		bAceptar.setBounds(449, 275, 138, 23);
		frame.getContentPane().add(bAceptar);
		
		JLabel lblSeleccionaCurso = new JLabel("Selecciona curso:");
		lblSeleccionaCurso.setBounds(60, 49, 103, 14);
		frame.getContentPane().add(lblSeleccionaCurso);
		
		cb = new JComboBox();
		cb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String curso=(String) cb.getSelectedItem();
				label.setText(curso.split(" ")[0]);
			}
		});
		cb.setBounds(176, 46, 411, 20);
		frame.getContentPane().add(cb);
		
		textArea = new TextArea();
		textArea.setBounds(28, 176, 371, 160);
		frame.getContentPane().add(textArea);
		
		label = new JLabel("");
		label.setBounds(176, 74, 185, 14);
		frame.getContentPane().add(label);
	}
}
