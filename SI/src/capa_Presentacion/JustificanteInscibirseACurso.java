package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import java.awt.Cursor;
import javax.swing.DropMode;
import java.awt.TextArea;

public class JustificanteInscibirseACurso {

	public JFrame frame;
	public static TextArea textArea;
	public static JButton bAceptar;
	public static JButton bImprimir;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JustificanteInscibirseACurso window = new JustificanteInscibirseACurso();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JustificanteInscibirseACurso() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 436, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblJustificanteDeInscripcin = new JLabel("JUSTIFICANTE DE INSCRIPCI\u00D3N");
		lblJustificanteDeInscripcin.setBounds(116, 11, 250, 14);
		frame.getContentPane().add(lblJustificanteDeInscripcin);
		
		bAceptar = new JButton("Aceptar");
		bAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		bAceptar.setBounds(58, 214, 119, 23);
		frame.getContentPane().add(bAceptar);
		
		bImprimir = new JButton("Imprimir");
		bImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		bImprimir.setBounds(247, 214, 119, 23);
		frame.getContentPane().add(bImprimir);
		
		textArea = new TextArea();
		textArea.setBounds(30, 36, 380, 160);
		frame.getContentPane().add(textArea);
	}
}