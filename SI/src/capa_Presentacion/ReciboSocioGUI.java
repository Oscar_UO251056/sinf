package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import capa_Logica.AltaPerito2;
import capa_Logica.InscritosAUnCurso;
import capa_Logica.ReciboSocio;

import javax.swing.JButton;
import java.awt.TextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class ReciboSocioGUI {

	private JFrame frame;
	private JTextField tfSocio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReciboSocioGUI window = new ReciboSocioGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ReciboSocioGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblRecibos = new JLabel("Recibos");
		lblRecibos.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRecibos.setBounds(169, 11, 63, 14);
		frame.getContentPane().add(lblRecibos);
		
		JLabel lblNSocio = new JLabel("N\u00BA Socio: ");
		lblNSocio.setBounds(10, 51, 63, 14);
		frame.getContentPane().add(lblNSocio);
		
		JLabel lbNombre = new JLabel("");
		lbNombre.setBounds(58, 93, 46, 14);
		frame.getContentPane().add(lbNombre);
		
		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(10, 93, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 76, 424, 6);
		frame.getContentPane().add(separator);
		
		tfSocio = new JTextField();
		tfSocio.setBounds(69, 48, 86, 20);
		frame.getContentPane().add(tfSocio);
		tfSocio.setColumns(10);
		
		TextArea textArea = new TextArea();
		textArea.setBounds(10, 133, 288, 122);
		frame.getContentPane().add(textArea);
		
		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReciboSocio logica=new ReciboSocio();
				try{
					
					lbNombre.setText(logica.Nombre(tfSocio.getText()));
					
					}
					catch(NumberFormatException e1){
						e1.printStackTrace(); 
						}
				
				textArea.setText(logica.recibosAnuales(tfSocio.getText()));
				
				if (logica.recibosAnuales(tfSocio.getText()).equals("")){
					textArea.setText("No tiene recibos emitidos");
				}
				
			}
		});
		btnOK.setBounds(276, 47, 89, 23);
		frame.getContentPane().add(btnOK);
		
		
		
		JLabel lblIdreciboEstado = new JLabel("ID_recibo - Estado - Cantidad");
		lblIdreciboEstado.setBounds(10, 108, 156, 14);
		frame.getContentPane().add(lblIdreciboEstado);
		
		
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnSalir.setBounds(318, 177, 89, 23);
		frame.getContentPane().add(btnSalir);
	}

}
