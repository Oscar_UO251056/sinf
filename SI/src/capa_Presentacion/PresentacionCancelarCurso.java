package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import capa_Logica.LogicaVisorCursos;

public class PresentacionCancelarCurso extends JFrame {

	private JPanel contentPane;
	
	public static PresentacionCancelarCurso thisFrame;
	public JLabel lbTitulo;
	public JLabel lbEspacio;
	public JLabel lbInscritos;
	public JLabel lbTotal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					thisFrame = new PresentacionCancelarCurso();
					thisFrame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PresentacionCancelarCurso() {
		setTitle("Curso cancelado");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 518, 231);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSeHaCancelado = new JLabel("Se ha cancelado con \u00E9xito el curso con la siguiente informaci\u00F3n:");
		lblSeHaCancelado.setBounds(10, 11, 408, 14);
		contentPane.add(lblSeHaCancelado);
		
		lbTitulo = new JLabel("T\u00EDtulo: Curso ejemplo 1");
		lbTitulo.setBounds(10, 36, 424, 14);
		contentPane.add(lbTitulo);
		
		lbEspacio = new JLabel("Espacio: Aula ejemplo 1");
		lbEspacio.setBounds(10, 61, 424, 14);
		contentPane.add(lbEspacio);
		
		lbInscritos = new JLabel("Inscritos: 0/0");
		lbInscritos.setBounds(10, 86, 424, 14);
		contentPane.add(lbInscritos);
		
		JLabel lblSeHaGenerado = new JLabel("Se ha generado el archivo devoluciones.csv destinado al banco en la carpeta local.");
		lblSeHaGenerado.setBounds(10, 111, 492, 14);
		contentPane.add(lblSeHaGenerado);
		
		lbTotal = new JLabel("El importe total a devolver asciende a: 0.00 \u20AC");
		lbTotal.setBounds(10, 136, 402, 14);
		contentPane.add(lbTotal);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LogicaVisorCursos.limpiar();
				thisFrame.dispose();
			}
		});
		btnAceptar.setBounds(413, 168, 89, 23);
		contentPane.add(btnAceptar);
	}
}
