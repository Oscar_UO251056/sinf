package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import capa_Presentacion.PresentacionIntroducirFormador;
import capa_Logica.LogicaSesiones;
import javax.swing.JOptionPane;
import capa_Presentacion.Sesion;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import capa_Presentacion.PresentacionPlanificarCurso;
import java.awt.Font;
import java.awt.Color;

public class PresentacionSesiones extends JFrame {

	private JPanel contentPane;
	private JTextField tfFecha;
	private JTextField tfDuracion;
	private static PresentacionSesiones frame;
	private LinkedList<LinkedList<String>> formadores;
	JComboBox cbFormador; 
	JLabel lbCamposNulos;
	private LinkedList<Sesion> sesiones;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new PresentacionSesiones(null, null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private PresentacionSesiones() {}

	/**
	 * Create the frame.
	 */
	
	public PresentacionSesiones(LinkedList<Sesion> sesiones, PresentacionPlanificarCurso parent) {
		frame = this;
		this.sesiones = sesiones;
		
		setTitle("Nueva sesi\u00F3n");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 253);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFechaYHora = new JLabel("Fecha y hora: (yyyy-MM-dd HH:mm):");
		lblFechaYHora.setBounds(10, 27, 227, 14);
		contentPane.add(lblFechaYHora);
		
		tfFecha = new JTextField();
		tfFecha.setBounds(248, 24, 176, 20);
		contentPane.add(tfFecha);
		tfFecha.setColumns(10);
		tfFecha.setText("2018-08-08 08:00");
		
		JLabel lblFormador = new JLabel("Formador: ");
		lblFormador.setBounds(10, 75, 109, 14);
		contentPane.add(lblFormador);
		
		cbFormador = new JComboBox();
		cbFormador.setBounds(129, 72, 295, 20);
		contentPane.add(cbFormador);
		
		JLabel lblDuracinh = new JLabel("Duraci\u00F3n (h): ");
		lblDuracinh.setBounds(10, 125, 109, 14);
		contentPane.add(lblDuracinh);
		
		tfDuracion = new JTextField();
		tfDuracion.setBounds(129, 122, 86, 20);
		contentPane.add(tfDuracion);
		tfDuracion.setColumns(10);
		tfDuracion.setText("2");
		
		JButton btnIntroducir = new JButton("Introducir");
		btnIntroducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				if (cbFormador.getSelectedIndex() == -1)
					JOptionPane.showMessageDialog(null, "Formador seleccionado no v�lido", "Error", JOptionPane.ERROR_MESSAGE);
				else if (tfFecha.getText().isEmpty() || tfDuracion.getText().isEmpty()) {
					lbCamposNulos.setVisible(true);
				} else {
					try {
						String id_formador = formadores.get(cbFormador.getSelectedIndex()).get(0);
						String nombre = formadores.get(cbFormador.getSelectedIndex()).get(1);
						Date fecha = formateador.parse(tfFecha.getText());
						int duracion = Integer.parseInt(tfDuracion.getText());
						
						if (fechaRepetida(fecha))
							JOptionPane.showMessageDialog(null, "Ya hay una sesi�n con esta fecha.", "Error", JOptionPane.ERROR_MESSAGE);
						else if (fecha.compareTo(new Date()) < 0) 
							JOptionPane.showMessageDialog(null, "La fecha no puede estar en el pasado.", "Error", JOptionPane.ERROR_MESSAGE);
						else if (duracion <= 0) {
							JOptionPane.showMessageDialog(null, "La duraci�n debe ser mayor que cero.", "Error", JOptionPane.ERROR_MESSAGE);
						} else {
							sesiones.add(new Sesion(id_formador, nombre, duracion, fecha));
							JOptionPane.showMessageDialog(null, "Sesi�n a�adida con �xito.", "Exito", JOptionPane.INFORMATION_MESSAGE);
							parent.actualizarSesiones();
							frame.setVisible(false);
						}
					} catch (ParseException e) {
						JOptionPane.showMessageDialog(null, "Formato de fecha no v�lido.", "Error", JOptionPane.ERROR_MESSAGE);
					} catch (NumberFormatException e) {
						JOptionPane.showMessageDialog(null, "Duraci�n debe ser un n�mero.", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnIntroducir.setBounds(10, 180, 132, 23);
		contentPane.add(btnIntroducir);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		btnCancelar.setBounds(292, 180, 132, 23);
		contentPane.add(btnCancelar);
		
		JButton btnNuevoFormador = new JButton("Nuevo formador");
		btnNuevoFormador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PresentacionIntroducirFormador.main(null);
			}
		});
		btnNuevoFormador.setBounds(283, 103, 141, 23);
		contentPane.add(btnNuevoFormador);
		
		lbCamposNulos = new JLabel("Ningun campo puede ser nulo.");
		lbCamposNulos.setForeground(Color.RED);
		lbCamposNulos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbCamposNulos.setBounds(20, 155, 217, 14);
		contentPane.add(lbCamposNulos);
		lbCamposNulos.setVisible(false);
		
		formadores = new LinkedList<LinkedList<String>>();
	}
	
	public void actualizaFormadores() {
		formadores.clear();
		cbFormador.removeAllItems();
		
		LinkedList<LinkedList<String>> nuevosFormadores = LogicaSesiones.getFormadores();
		
		for (int i = 0; i < nuevosFormadores.size(); i++) {
			formadores.add(nuevosFormadores.get(i));
			cbFormador.addItem(String.format("%s -- %s", nuevosFormadores.get(i).get(0), nuevosFormadores.get(i).get(1)));
		}
		
	}
	
	private boolean fechaRepetida(Date fecha) {
		for (int i = 0; i < sesiones.size(); i++) {
			if (sesiones.get(i).fecha.compareTo(fecha) == 0)
				return true;
		}
		return false;
	}
}
