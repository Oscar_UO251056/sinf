package capa_Presentacion;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import capa_Logica.LogicaAbrirCurso;
import java.util.LinkedList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import javax.swing.JOptionPane;

public class PresentacionAbrirCurso {

	private JFrame frmAperturaDeCurso;
	private JTextField tfPlazas;
	private JTextField tfFechaInicio;
	private JTextField tfFechaFin;
	private JLabel labelCamposNulos;
	JComboBox comboCurso;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PresentacionAbrirCurso window = new PresentacionAbrirCurso();
					window.frmAperturaDeCurso.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PresentacionAbrirCurso() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAperturaDeCurso = new JFrame();
		frmAperturaDeCurso.setTitle("Apertura de curso");
		frmAperturaDeCurso.setResizable(false);
		frmAperturaDeCurso.setBounds(100, 100, 554, 256);
		frmAperturaDeCurso.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAperturaDeCurso.getContentPane().setLayout(null);
		
		JLabel lblCurso = new JLabel("Cursos planificados:");
		lblCurso.setBounds(20, 14, 154, 14);
		frmAperturaDeCurso.getContentPane().add(lblCurso);
		
		comboCurso = new JComboBox();
		comboCurso.setBounds(184, 11, 333, 20);
		frmAperturaDeCurso.getContentPane().add(comboCurso);
		
		JLabel lblPlazasDisponibles = new JLabel("Plazas disponibles:");
		lblPlazasDisponibles.setBounds(20, 53, 120, 14);
		frmAperturaDeCurso.getContentPane().add(lblPlazasDisponibles);
		
		tfPlazas = new JTextField();
		tfPlazas.setText("30");
		tfPlazas.setBounds(150, 50, 58, 20);
		frmAperturaDeCurso.getContentPane().add(tfPlazas);
		tfPlazas.setColumns(10);
		
		JLabel lblPlazoDeInscripcin = new JLabel("Plazo de inscripci\u00F3n (yyyy-MM-dd):  del\r\n");
		lblPlazoDeInscripcin.setBounds(20, 94, 218, 14);
		frmAperturaDeCurso.getContentPane().add(lblPlazoDeInscripcin);
		
		tfFechaInicio = new JTextField();
		tfFechaInicio.setText("2018-08-08");
		tfFechaInicio.setBounds(260, 91, 71, 20);
		frmAperturaDeCurso.getContentPane().add(tfFechaInicio);
		tfFechaInicio.setColumns(10);
		
		JLabel lblAl = new JLabel("al");
		lblAl.setBounds(352, 94, 20, 14);
		frmAperturaDeCurso.getContentPane().add(lblAl);
		
		tfFechaFin = new JTextField();
		tfFechaFin.setText("2018-08-08");
		tfFechaFin.setBounds(382, 91, 86, 20);
		frmAperturaDeCurso.getContentPane().add(tfFechaFin);
		tfFechaFin.setColumns(10);
		
		labelCamposNulos = new JLabel("Ning\u00FAn valor puede ser nulo.");
		labelCamposNulos.setForeground(Color.RED);
		labelCamposNulos.setFont(new Font("Tahoma", Font.BOLD, 11));
		labelCamposNulos.setBounds(20, 144, 171, 14);
		frmAperturaDeCurso.getContentPane().add(labelCamposNulos);
		labelCamposNulos.setVisible(false);
		
		JButton btnAbrirCurso = new JButton("Abrir curso");
		btnAbrirCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfPlazas.getText().isEmpty() || tfFechaInicio.getText().isEmpty() || tfFechaFin.getText().isEmpty()) {
					labelCamposNulos.setVisible(true);
				} else {
					int plazas;
					Date fechaInicio;
					Date fechaFin;
					try {
						plazas = Integer.parseInt(tfPlazas.getText());
						DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
						formateador.setLenient(false);
						fechaInicio = formateador.parse(tfFechaInicio.getText());
						fechaFin = formateador.parse(tfFechaFin.getText());
						if (comboCurso.getSelectedIndex() == -1) {
							JOptionPane.showMessageDialog(frmAperturaDeCurso, "Error: curso no seleccionado.", "Error", JOptionPane.ERROR_MESSAGE );
						} else if (fechaFin.compareTo(fechaInicio) == -1 || fechaInicio.compareTo(new Date()) == -1) {
							JOptionPane.showMessageDialog(frmAperturaDeCurso, "Error: las fechas no pueden pertenecer al pasado o ser la fecha de fin anterior a la fecha de inicio.", "Error", JOptionPane.ERROR_MESSAGE );
						}else if (plazas <= 0) {
							JOptionPane.showMessageDialog(frmAperturaDeCurso, "Error: el n�mero de plazas debe ser mayor que 0.", "Error", JOptionPane.ERROR_MESSAGE );
						}else{
							LogicaAbrirCurso.abreCurso(comboCurso.getSelectedIndex(), fechaInicio, fechaFin, plazas);
							JOptionPane.showMessageDialog(frmAperturaDeCurso, "Curso abierto con �xito.", "Exito", JOptionPane.INFORMATION_MESSAGE );
							actualizaCursos();
						}
					} catch (ParseException excepcion) {
						JOptionPane.showMessageDialog(frmAperturaDeCurso, "Error: formato de fecha no permitido.", "Error", JOptionPane.ERROR_MESSAGE );
					} catch (NumberFormatException excepcion) {
						JOptionPane.showMessageDialog(frmAperturaDeCurso, "Error: plazas tienen que ser un n�mero entero.", "Error", JOptionPane.ERROR_MESSAGE );
					}
				}
			}
		});
		btnAbrirCurso.setBounds(10, 184, 130, 23);
		frmAperturaDeCurso.getContentPane().add(btnAbrirCurso);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmAperturaDeCurso.dispose();
			}
		});
		btnCancelar.setBounds(428, 184, 89, 23);
		frmAperturaDeCurso.getContentPane().add(btnCancelar);
		LogicaAbrirCurso.init();
		actualizaCursos();
	}
	
	private void actualizaCursos() {
		comboCurso.removeAllItems();
		
		LinkedList<LinkedList<String>> cursos = LogicaAbrirCurso.getCursos();
		
		for (int i = 0; i < cursos.size(); i++) {
			LinkedList<String> curso = cursos.get(i);
			comboCurso.addItem(String.format("%s -- %s (Espacio: %s)", curso.get(0), curso.get(1), curso.get(2)));
		}
			
	}
}
