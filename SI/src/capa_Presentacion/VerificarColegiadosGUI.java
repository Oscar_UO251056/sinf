package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

import capa_Logica.RegistroCuotas;
import capa_Logica.VerificarColegiados;

import java.awt.Component;
import javax.swing.Box;
import javax.swing.DefaultListModel;

import java.awt.Dimension;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.List;
import java.awt.Color;

public class VerificarColegiadosGUI {

	public JFrame frmVerificarColegiados;
	public JTextField tfRuta;

	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerificarColegiadosGUI window = new VerificarColegiadosGUI();
					window.frmVerificarColegiados.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VerificarColegiadosGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmVerificarColegiados = new JFrame();
		frmVerificarColegiados.setTitle("Verificar colegiados");
		frmVerificarColegiados.setBounds(100, 100, 450, 411);
		frmVerificarColegiados.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmVerificarColegiados.getContentPane().setLayout(null);

		JLabel lblNewResolucinMinisterio = new JLabel("Resoluci\u00F3n ministerio: ");
		lblNewResolucinMinisterio.setFont(new Font("Sitka Subheading", Font.BOLD, 13));
		lblNewResolucinMinisterio.setBounds(246, 23, 178, 14);
		frmVerificarColegiados.getContentPane().add(lblNewResolucinMinisterio);
		
		JLabel lComentario = new JLabel("No se ha verificado ninguna titulaci\u00F3n");
		lComentario.setForeground(Color.RED);
		lComentario.setFont(new Font("Tahoma", Font.BOLD, 11));
		lComentario.setBounds(10, 218, 219, 23);
		frmVerificarColegiados.getContentPane().add(lComentario);
		lComentario.setVisible(false);

		JLabel lblDninmeroid = new JLabel("DNI-Nombre-Codigo resoluci\u00F3n");
		lblDninmeroid.setFont(new Font("Tahoma", Font.ITALIC, 10));
		lblDninmeroid.setBounds(226, 51, 208, 14);
		frmVerificarColegiados.getContentPane().add(lblDninmeroid);

		//JScrollPane scrollPane = new JScrollPane();


		DefaultListModel<String> modelo=new DefaultListModel<String>();
		//scrollPane.setViewportView(lResolucion);

		JButton btnNewButton = new JButton("Activar colegiados");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Verificar la resolucion del ministerio
				lComentario.setVisible(false);
				//Modelo3 -> lJustificante ( Se ha cambiado estado a inscrito )
				DefaultListModel<String> modelo3=new DefaultListModel<String>();
				//Modelo 4 -> lCancelado ( Se ha cambiado el estado a cancelado )
				DefaultListModel<String> modelo4=new DefaultListModel<String>();
				//Modelo 5 -> lInscrito ( El miembro ya estaba inscrito anteriormente)
				DefaultListModel<String> modelo5=new DefaultListModel<String>();

				for(int i=0; i< modelo.size();i++){
					//VerificarColegiados.Clasificar(modelo.getElementAt(i));

					if((VerificarColegiados.Clasificar(modelo.getElementAt(i)))==1){
						String[] fila=modelo.getElementAt(i).split("-");
						String linea=fila[0]+" "+fila[1];
						modelo3.addElement(linea);
					}
					else if ((VerificarColegiados.Clasificar(modelo.getElementAt(i)))==2){
						System.out.println("ENTRA INSCRITOS CON ANTERIORIDAD");
						String[] fila=modelo.getElementAt(i).split("-");
						String linea=fila[0]+" "+fila[1];
						modelo5.addElement(linea);
					}
					else if ((VerificarColegiados.Clasificar(modelo.getElementAt(i)))==3){
						System.out.println("ENTRA A CANCELADO");
						String[] fila=modelo.getElementAt(i).split("-");
						String linea=fila[0]+" "+fila[1];
						modelo4.addElement(linea);
					}

				}
				//modelo3.addElement("HOLA");
				if ( (!modelo3.isEmpty()) || (!modelo4.isEmpty()) || (!modelo5.isEmpty())){
					System.out.println("JUSTIFICANTE");
					//HASTA AQUI
					JustificanteVerificarColegiados justificante=new JustificanteVerificarColegiados();
					justificante.lJustificante.setModel(modelo3);
					justificante.lCancelado.setModel(modelo4);
					justificante.lInscrito.setModel(modelo5);
					justificante.frame.setVisible(true);
				}else{
					System.out.println("No se ha modificado ninguna solicitud");
					lComentario.setVisible(true);
					
			
				}

			}
		});
		btnNewButton.setBounds(246, 294, 178, 23);
		frmVerificarColegiados.getContentPane().add(btnNewButton);

		JLabel lblRuta = new JLabel("Ruta:");
		lblRuta.setBounds(10, 23, 65, 14);
		frmVerificarColegiados.getContentPane().add(lblRuta);

		tfRuta = new JTextField();
		tfRuta.setEditable(false);
		tfRuta.setBounds(10, 48, 178, 20);
		frmVerificarColegiados.getContentPane().add(tfRuta);
		tfRuta.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(226, 76, 208, 189);
		frmVerificarColegiados.getContentPane().add(scrollPane);

		JList<String> lResolucion1 = new JList<String>();
		scrollPane.setViewportView(lResolucion1);
		lResolucion1.setModel(modelo);

		JButton bAbrir = new JButton("Abrir");
		bAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				lComentario.setVisible(false);
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(bAbrir);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					System.out.println(file.getPath());
					System.out.println(file.getName());


					if( VerificarColegiados.isCSV(file.getName()) ) {

						modelo.removeAllElements();
						//Seleccionar y leer el fichero csv 
						tfRuta.setText(file.getPath());	
						VerificarColegiados.leerFichero(tfRuta.getText());
						//Modificar la JLIST
						for (int i=0; i < VerificarColegiados.leerFichero(tfRuta.getText()).size(); i++){
							//modelo.addElement(VerificarColegiados.leerFichero(tfRuta.getText()).get(i));
							String[] datos=(VerificarColegiados.leerFichero(tfRuta.getText()).get(i)).split(";");
							if (VerificarColegiados.PerteneceBBDD(datos[0])){
								System.out.println("Pertenece a la BBDD");
								String dni=datos[0];
								String nombre=VerificarColegiados.Nombre(dni);
								String codigo=datos[1];
								String linea=dni+"-"+nombre+"-"+codigo;
								modelo.addElement(linea);
							}else{
								System.out.println("NO PERTENECEE");
							}
						}
						lResolucion1.setModel(modelo);
						
						
					}


					else {
						JOptionPane.showMessageDialog(null,"El fichero debe ser .CSV'." ,"Error " , JOptionPane.ERROR_MESSAGE);
					}

				} else {
					JOptionPane.showMessageDialog(null,"Al abrir el archivo'." ,"Error " , JOptionPane.ERROR_MESSAGE);

				}
			}
		});
		bAbrir.setBounds(10, 73, 95, 23);
		frmVerificarColegiados.getContentPane().add(bAbrir);
		
		
//hola

	}
}
