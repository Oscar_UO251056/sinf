package capa_Presentacion;

import java.util.Date;

public class Sesion {

	public String id_formador;
	public String nombre;
	public int duracion;
	public Date fecha;
	
	private Sesion() {}
	
	public Sesion (String id_formador, String nombre, int duracion, Date fecha) {
		this.id_formador = id_formador;
		this.nombre = nombre;
		this.duracion = duracion;
		this.fecha = fecha;
	}
}
