//HISTORIA 1

package capa_Presentacion;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import capa_Logica.SolicitarAlta;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class SolicitarAltaGUI {

	private JFrame frmCoiipaDarse;
	private static JTextField tfDNI;
	private static JTextField tfNombre;
	private static JTextField tfApellidos;
	private static JTextField tfDireccion;
	private static JTextField tfPoblacion;
	private static JTextField tfTelefono;
	private static JTextField tfTitulacion;
	private static JTextField tfCentro;
	private static JTextField tfAgno_obtencion;
	private static JTextField tfNum_cuenta;
	private static JButton bConfirmar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {



					SolicitarAltaGUI window = new SolicitarAltaGUI();
					window.frmCoiipaDarse.setVisible(true);

					DocumentListener docListener = new DocumentListener() {

						@Override
						public void removeUpdate(DocumentEvent e) {
							checkForText();
						}

						@Override
						public void insertUpdate(DocumentEvent e) {
							checkForText();
						}

						@Override
						public void changedUpdate(DocumentEvent e) {
							checkForText();
						}

						private void checkForText() {
							boolean textOK = !tfNombre.getText().trim().isEmpty() && !tfApellidos.getText().trim().isEmpty()
									&& !tfDNI.getText().trim().isEmpty() /*&& !tfTitulacion.getText().trim().isEmpty()
									&& !tfCentro.getText().trim().isEmpty() && !tfAgno_obtencion.getText().trim().isEmpty()*/
									&& !tfDireccion.getText().trim().isEmpty() && !tfPoblacion.getText().trim().isEmpty()
									&& !tfTelefono.getText().trim().isEmpty() && !tfNum_cuenta.getText().trim().isEmpty();
							bConfirmar.setEnabled(textOK);
						}

					};
					tfNombre.getDocument().addDocumentListener(docListener);
					tfApellidos.getDocument().addDocumentListener(docListener);
					tfDNI.getDocument().addDocumentListener(docListener);
					//tfTitulacion.getDocument().addDocumentListener(docListener);
					//tfCentro.getDocument().addDocumentListener(docListener);
					tfDireccion.getDocument().addDocumentListener(docListener);
					//tfAgno_obtencion.getDocument().addDocumentListener(docListener);
					tfPoblacion.getDocument().addDocumentListener(docListener);
					tfNum_cuenta.getDocument().addDocumentListener(docListener);
					tfTelefono.getDocument().addDocumentListener(docListener);




				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SolicitarAltaGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCoiipaDarse = new JFrame();
		frmCoiipaDarse.setTitle("COIIPA - Darse de alta");
		frmCoiipaDarse.setBounds(100, 100, 568, 620);
		frmCoiipaDarse.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();
		frmCoiipaDarse.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		tfDNI = new JTextField();
		tfDNI.setBounds(139, 71, 80, 24);
		panel.add(tfDNI);
		tfDNI.setColumns(10);

		JLabel lblDNI = new JLabel("DNI:");
		lblDNI.setBounds(76, 75, 56, 16);
		panel.add(lblDNI);

		JLabel lblNewLabel = new JLabel("Nombre: ");
		lblNewLabel.setBounds(76, 120, 56, 16);
		panel.add(lblNewLabel);

		JLabel lblApellido = new JLabel("Apellidos:");
		lblApellido.setBounds(76, 149, 80, 16);
		panel.add(lblApellido);

		JLabel lblDireccin = new JLabel("Direcci\u00F3n:");
		lblDireccin.setBounds(76, 196, 66, 16);
		panel.add(lblDireccin);

		JLabel lblPoblacin = new JLabel("Poblaci\u00F3n:");
		lblPoblacin.setBounds(76, 225, 66, 16);
		panel.add(lblPoblacin);

		JLabel lblTelefono = new JLabel("Tel\u00E9fono:");
		lblTelefono.setBounds(76, 261, 56, 16);
		panel.add(lblTelefono);

		JLabel lblTitulacin = new JLabel("Titulaci\u00F3n:");
		lblTitulacin.setBounds(76, 332, 80, 16);
		panel.add(lblTitulacin);

		JLabel lblCentro = new JLabel("Centro:");
		lblCentro.setBounds(76, 361, 56, 16);
		panel.add(lblCentro);

		JLabel lblAoDeObtencin = new JLabel("A\u00F1o de obtenci\u00F3n:");
		lblAoDeObtencin.setBounds(76, 390, 116, 16);
		panel.add(lblAoDeObtencin);

		JLabel lblDatosBancarios = new JLabel("Numero de cuenta:");
		lblDatosBancarios.setBounds(76, 448, 116, 16);
		panel.add(lblDatosBancarios);

		tfNombre = new JTextField();
		tfNombre.setBounds(139, 117, 160, 22);
		panel.add(tfNombre);
		tfNombre.setColumns(10);

		tfApellidos = new JTextField();
		tfApellidos.setBounds(139, 146, 242, 22);
		panel.add(tfApellidos);
		tfApellidos.setColumns(10);

		tfDireccion = new JTextField();
		tfDireccion.setBounds(139, 193, 242, 22);
		panel.add(tfDireccion);
		tfDireccion.setColumns(10);

		tfPoblacion = new JTextField();
		tfPoblacion.setBounds(139, 225, 160, 22);
		panel.add(tfPoblacion);
		tfPoblacion.setColumns(10);

		tfTelefono = new JTextField();
		tfTelefono.setBounds(139, 258, 160, 22);
		panel.add(tfTelefono);
		tfTelefono.setColumns(10);

		JSeparator separator = new JSeparator();
		separator.setForeground(Color.GRAY);
		separator.setBounds(0, 312, 548, 16);
		panel.add(separator);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 47, 548, 11);
		panel.add(separator_1);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(0, 433, 548, 16);
		panel.add(separator_2);

		tfTitulacion = new JTextField();
		tfTitulacion.setBounds(139, 329, 242, 22);
		panel.add(tfTitulacion);
		tfTitulacion.setColumns(10);

		tfCentro = new JTextField();
		tfCentro.setBounds(139, 358, 160, 22);
		panel.add(tfCentro);
		tfCentro.setColumns(10);

		tfAgno_obtencion = new JTextField();
		tfAgno_obtencion.setBounds(193, 387, 106, 22);
		panel.add(tfAgno_obtencion);
		tfAgno_obtencion.setColumns(10);

		tfNum_cuenta = new JTextField();
		tfNum_cuenta.setBounds(193, 445, 188, 22);
		panel.add(tfNum_cuenta);
		tfNum_cuenta.setColumns(10);

		bConfirmar = new JButton("Confirmar");
		bConfirmar.setEnabled(false);
		bConfirmar.addActionListener(new ActionListener() { //Al pulsar Confirmar
			public void actionPerformed(ActionEvent arg0) {

				if(!SolicitarAlta.isNumber(tfAgno_obtencion.getText())) {
					JOptionPane.showMessageDialog(null,"A�o de obtenci�n incorrecto." ,"Error" , JOptionPane.ERROR_MESSAGE);
				}
				else {

					boolean exito=SolicitarAlta.introducirSocios(tfDNI.getText(), tfNombre.getText(), tfApellidos.getText(),
							tfDireccion.getText(), tfPoblacion.getText(), tfTelefono.getText(),
							tfTitulacion.getText(),tfCentro.getText(), tfAgno_obtencion.getText(), tfNum_cuenta.getText());


					if(exito)
						JOptionPane.showMessageDialog(null,"Ha sido dado de alta" ,"Dado de alta" , JOptionPane.WARNING_MESSAGE);
					else {
						if(!SolicitarAlta.isDNIValid(tfDNI.getText()))
							JOptionPane.showMessageDialog(null,"El DNI del usuario ya ha sido registrado. " ,"Error" , JOptionPane.ERROR_MESSAGE);
							
						JOptionPane.showMessageDialog(null,"Error, no se ha podido dar de alta. " ,"Error" , JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		bConfirmar.setBounds(399, 503, 116, 33);
		panel.add(bConfirmar);

		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(0, 488, 549, 2);
		panel.add(separator_3);

		JLabel lblDatosPersonales = new JLabel("Datos personales");
		lblDatosPersonales.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblDatosPersonales.setBounds(32, 28, 124, 16);
		panel.add(lblDatosPersonales);

		JLabel lblDat = new JLabel("Datos acad\u00E9micos (Opcional)");
		lblDat.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblDat.setBounds(32, 293, 214, 16);
		panel.add(lblDat);

		JLabel lblDatosBancarios_1 = new JLabel("Datos bancarios");
		lblDatosBancarios_1.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblDatosBancarios_1.setBounds(32, 419, 110, 16);
		panel.add(lblDatosBancarios_1);
	}
}
