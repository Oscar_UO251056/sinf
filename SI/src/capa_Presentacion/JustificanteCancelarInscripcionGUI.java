package capa_Presentacion;

import java.awt.EventQueue;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;

public class JustificanteCancelarInscripcionGUI {
	public JFrame frame;
	public static TextArea textArea;
	public static JButton bAceptar;
	public static JButton bImprimir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JustificanteCancelarInscripcionGUI window = new JustificanteCancelarInscripcionGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JustificanteCancelarInscripcionGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame = new JFrame();
		frame.setBounds(100, 100, 436, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lJustificante = new JLabel("JUSTIFICANTE DE CANCELACI\u00D3N");
		lJustificante.setFont(new Font("Tahoma", Font.BOLD, 18));
		lJustificante.setBounds(46, 11, 345, 19);
		frame.getContentPane().add(lJustificante);
		
		bAceptar = new JButton("Aceptar");
		bAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		bAceptar.setBounds(58, 214, 119, 23);
		frame.getContentPane().add(bAceptar);
		
		bImprimir = new JButton("Imprimir");
		bImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		bImprimir.setBounds(247, 214, 119, 23);
		frame.getContentPane().add(bImprimir);
		
		textArea = new TextArea();
		textArea.setBounds(30, 36, 380, 160);
		frame.getContentPane().add(textArea);
	}

}
