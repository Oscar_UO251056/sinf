package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import capa_Logica.CancelarInscripcion;
import capa_Logica.InscritosAUnCurso;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CancelarInscripcionGUI {

	public JFrame frame;
	public JTextField tfDNI;
	public static JComboBox cb;
	public static JLabel lCurso;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CancelarInscripcionGUI window = new CancelarInscripcionGUI();
					window.frame.setVisible(true);
					InscritosAUnCurso logica = new InscritosAUnCurso();
					String Cursos=logica.DameCursos();
					int numeroCursos=Cursos.split("\n").length;
					for(int cont=0;cont<numeroCursos;cont++){
						cb.addItem(Cursos.split("\n")[cont]);	
						
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CancelarInscripcionGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 568, 373);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCancelarInscripciones = new JLabel("CANCELAR INSCRIPCIONES");
		lblCancelarInscripciones.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCancelarInscripciones.setBounds(154, 11, 267, 22);
		frame.getContentPane().add(lblCancelarInscripciones);
		
		JLabel lblSeleccionaUnCurso = new JLabel("Selecciona un curso:");
		lblSeleccionaUnCurso.setBounds(46, 55, 163, 14);
		frame.getContentPane().add(lblSeleccionaUnCurso);
		
		cb = new JComboBox();
		cb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String curso=(String) cb.getSelectedItem();
				lCurso.setText(curso.split(" ")[0]);
			}
		});
		cb.setBounds(46, 80, 456, 20);
		frame.getContentPane().add(cb);
		
		JLabel lblCurso = new JLabel("Curso:");
		lblCurso.setBounds(46, 111, 59, 14);
		frame.getContentPane().add(lblCurso);
		
		lCurso = new JLabel("");
		lCurso.setBounds(96, 111, 59, 14);
		frame.getContentPane().add(lCurso);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(46, 136, 456, 14);
		frame.getContentPane().add(separator);
		
		JLabel lblIntroduzcaDni = new JLabel("Introduzca DNI:");
		lblIntroduzcaDni.setBounds(46, 159, 108, 14);
		frame.getContentPane().add(lblIntroduzcaDni);
		
		tfDNI = new JTextField();
		tfDNI.setBounds(181, 156, 321, 20);
		frame.getContentPane().add(tfDNI);
		tfDNI.setColumns(10);
		
		JButton btnCancelarCurso = new JButton("Cancelar inscripci\u00F3n");
		btnCancelarCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CancelarInscripcion logica = new CancelarInscripcion();
				JustificanteCancelarInscripcionGUI justificante=new JustificanteCancelarInscripcionGUI();
				justificante.textArea.setText("");
				if (tfDNI.getText().equals("")){
					JOptionPane.showMessageDialog(null,"Introduzca DNI", "Atenci�n",JOptionPane.WARNING_MESSAGE);
					
				}else{
					if (!logica.Colegiado(tfDNI.getText()) && !logica.Externo(tfDNI.getText())){
						System.out.println("No pertenece a la BD");
						JOptionPane.showMessageDialog(null,"No existes en nuestra BD. No est�s inscrito en ning�n curso", "Atenci�n",JOptionPane.WARNING_MESSAGE);
					}else if (!logica.ExisteInscripcion(tfDNI.getText(), lCurso.getText())){
						System.out.println("No existe la insc");
						JOptionPane.showMessageDialog(null,"No est�s inscrito en ning�n curso", "Atenci�n",JOptionPane.WARNING_MESSAGE);
					}else if (logica.InscripcionYaCancelada(tfDNI.getText(), lCurso.getText())){
						System.out.println("Incripcion ya cancelada");
						JOptionPane.showMessageDialog(null,"Esta inscripci�n ya estaba cancelada", "Atenci�n",JOptionPane.WARNING_MESSAGE);
					}else if(logica.InscripcionPreinscrita(tfDNI.getText(), lCurso.getText())){
						System.out.println("Incripcion en pre-inscrito");
						JOptionPane.showMessageDialog(null,"Por el momento est�s pre-inscrito en este curso. Si no desea pertenecer al mismo, basta con no abonar la cantidad requerida dentro del plazo de dos d�as desde que realizaste tu pre-inscripci�n", "Atenci�n",JOptionPane.WARNING_MESSAGE);
					}
					else if (!logica.DentroPlazo(lCurso.getText())){
						System.out.println("Fuera del plazo");
						JOptionPane.showMessageDialog(null,"No puedes cancelar la inscripci�n a este curso. Fuera de plazo.", "Atenci�n",JOptionPane.WARNING_MESSAGE);
					}else if (logica.Colegiado(tfDNI.getText())){
						System.out.println("Cancelando inscripcion colegiado");
						double cantidad=logica.CancelarInscripcion(tfDNI.getText(), lCurso.getText());
						//Justificante
						justificante.textArea.setText("Hemos cancelado su inscripci�n al curso "+lCurso.getText()+". \nLe abonaremos en los pr�ximo d�as la cantidad de "+cantidad+"�.\nDicha cantidad equivale al 70% de que usted, con DNI "+tfDNI.getText()+" abon� inicialmente");
						justificante.frame.setVisible(true);
					}else if (logica.Externo(tfDNI.getText())){
						System.out.println("Cancelando inscripcion externo");
						double cantidad=logica.CancelarInscripcion(tfDNI.getText(), lCurso.getText());
						//Justificante
						justificante.textArea.setText("Hemos cancelado su inscripci�n al curso "+lCurso.getText()+". \nLe abonaremos en los pr�ximo d�as la cantidad de "+cantidad+"�.\nDicha cantidad equivale al 70% de que usted, con DNI "+tfDNI.getText()+", abon� inicialmente");
						justificante.frame.setVisible(true);
					}else{
						System.out.println("Otra cosa");
						JOptionPane.showMessageDialog(null,"No puedes cancelar la inscripci�n a este curso por problemas t�cnicos. Intentelo m�s tarde", "Atenci�n",JOptionPane.WARNING_MESSAGE);
						justificante.frame.setVisible(true);
					}
				}
			}
		});
		btnCancelarCurso.setBounds(46, 212, 456, 23);
		frame.getContentPane().add(btnCancelarCurso);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnAceptar.setBounds(413, 280, 89, 23);
		frame.getContentPane().add(btnAceptar);
	}
}
