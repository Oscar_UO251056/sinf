package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

import capa_Logica.LogicaPagoFormador;
import java.util.LinkedList;
import java.text.ParseException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PresentacionPagoFormador {

	private JFrame frmRegistroDePago;
	private JTextField tfCodigo;
	private JTextField tfDNI;
	private JTextField tfConcepto;
	private JTextField tfHoras;
	private JTextField tfPrecio;
	private JTextField tfEmision;
	private JTextField tfPago;
	private JLabel lbCamposVacios;
	private JLabel lbTotal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PresentacionPagoFormador window = new PresentacionPagoFormador();
					window.frmRegistroDePago.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PresentacionPagoFormador() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRegistroDePago = new JFrame();
		frmRegistroDePago.setResizable(false);
		frmRegistroDePago.setTitle("Registro de pago a formador");
		frmRegistroDePago.setBounds(100, 100, 459, 466);
		frmRegistroDePago.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmRegistroDePago.getContentPane().setLayout(null);
		
		JLabel lblCurso = new JLabel("Curso:");
		lblCurso.setBounds(10, 25, 106, 14);
		frmRegistroDePago.getContentPane().add(lblCurso);
		
		JLabel lblFormador = new JLabel("Formador:");
		lblFormador.setBounds(10, 64, 115, 14);
		frmRegistroDePago.getContentPane().add(lblFormador);
		
		JComboBox cbCurso = new JComboBox();
		cbCurso.setBounds(110, 22, 333, 20);
		frmRegistroDePago.getContentPane().add(cbCurso);
		
		JComboBox cbFormador = new JComboBox();
		cbFormador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfDNI.setText(LogicaPagoFormador.getDNI(cbFormador.getSelectedIndex()));
			}
		});
		cbFormador.setBounds(107, 61, 336, 20);
		frmRegistroDePago.getContentPane().add(cbFormador);
		
		JLabel lblDatosDeFactura = new JLabel("Datos de factura");
		lblDatosDeFactura.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDatosDeFactura.setBounds(10, 116, 156, 14);
		frmRegistroDePago.getContentPane().add(lblDatosDeFactura);
		
		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setBounds(10, 160, 46, 14);
		frmRegistroDePago.getContentPane().add(lblCdigo);
		
		tfCodigo = new JTextField();
		tfCodigo.setBounds(80, 157, 86, 20);
		frmRegistroDePago.getContentPane().add(tfCodigo);
		tfCodigo.setColumns(10);
		
		JLabel lblDni = new JLabel("DNI:");
		lblDni.setBounds(227, 160, 46, 14);
		frmRegistroDePago.getContentPane().add(lblDni);
		
		tfDNI = new JTextField();
		tfDNI.setBounds(272, 157, 138, 20);
		frmRegistroDePago.getContentPane().add(tfDNI);
		tfDNI.setColumns(10);
		
		JLabel lblConcepto = new JLabel("Concepto:");
		lblConcepto.setBounds(10, 206, 95, 14);
		frmRegistroDePago.getContentPane().add(lblConcepto);
		
		tfConcepto = new JTextField();
		tfConcepto.setBounds(80, 203, 330, 20);
		frmRegistroDePago.getContentPane().add(tfConcepto);
		tfConcepto.setColumns(10);
		
		JLabel lblHoras = new JLabel("Horas:");
		lblHoras.setBounds(10, 257, 46, 14);
		frmRegistroDePago.getContentPane().add(lblHoras);
		
		tfHoras = new JTextField();
		tfHoras.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
			@Override
			public void keyReleased(KeyEvent e) {
				try {
					int horas = Integer.parseInt(tfHoras.getText());
					double precio = Double.parseDouble(tfPrecio.getText());
				
					lbTotal.setText(String.format("= %.2f �", horas * precio));
				} catch (NumberFormatException exc) {
					
				}
			}
		});
		tfHoras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		tfHoras.setBounds(80, 254, 54, 20);
		frmRegistroDePago.getContentPane().add(tfHoras);
		tfHoras.setColumns(10);
		
		JLabel lblX = new JLabel("X");
		lblX.setBounds(162, 257, 46, 14);
		frmRegistroDePago.getContentPane().add(lblX);
		
		tfPrecio = new JTextField();
		tfPrecio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				try {
					int horas = Integer.parseInt(tfHoras.getText());
					double precio = Double.parseDouble(tfPrecio.getText());
				
					lbTotal.setText(String.format("= %.2f �", horas * precio));
				} catch (NumberFormatException exc) {
					
				}
			}
		});
		tfPrecio.setBounds(197, 254, 66, 20);
		frmRegistroDePago.getContentPane().add(tfPrecio);
		tfPrecio.setColumns(10);
		
		JLabel lblhora = new JLabel("\u20AC/hora");
		lblhora.setBounds(283, 257, 46, 14);
		frmRegistroDePago.getContentPane().add(lblhora);
		
		JLabel lblFechaDeEmisin = new JLabel("Fecha de emisi\u00F3n (yyyy-MM-dd):");
		lblFechaDeEmisin.setBounds(10, 313, 205, 14);
		frmRegistroDePago.getContentPane().add(lblFechaDeEmisin);
		
		tfEmision = new JTextField();
		tfEmision.setBounds(10, 339, 156, 20);
		frmRegistroDePago.getContentPane().add(tfEmision);
		tfEmision.setColumns(10);
		
		tfPago = new JTextField();
		tfPago.setBounds(227, 338, 156, 20);
		frmRegistroDePago.getContentPane().add(tfPago);
		tfPago.setColumns(10);
		
		JLabel lblFechaDePago = new JLabel("Fecha de pago (yyyy-MM-dd):");
		lblFechaDePago.setBounds(227, 313, 198, 14);
		frmRegistroDePago.getContentPane().add(lblFechaDePago);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaPagoFormador.limpiar();
				if (tfCodigo.getText().isEmpty() || tfDNI.getText().isEmpty() || tfConcepto.getText().isEmpty() || tfHoras.getText().isEmpty() || tfPrecio.getText().isEmpty() ||
						tfEmision.getText().isEmpty() || tfPago.getText().isEmpty()) {
					lbCamposVacios.setVisible(true);
				} else {
					if (cbCurso.getSelectedIndex() == -1 || cbFormador.getSelectedIndex() == -1) {
						JOptionPane.showMessageDialog(null, "Curso o formador seleccionado no v�lido", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						int codigo;
						String dni;
						String concepto;
						int horas;
						double precio;
						Date fechaEmision;
						Date fechaPago;
						try {
							codigo = Integer.parseInt(tfCodigo.getText());
							dni = tfDNI.getText();
							concepto = tfConcepto.getText();
							horas = Integer.parseInt(tfHoras.getText());
							precio = Double.parseDouble(tfPrecio.getText());
							
							DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
							formateador.setLenient(false);
							
							fechaEmision = formateador.parse(tfEmision.getText());
							fechaPago = formateador.parse(tfPago.getText());
							
							Date ahora = new Date();
							
							if (fechaEmision.compareTo(ahora) == 1 || fechaPago.compareTo(fechaEmision) == -1) {
								JOptionPane.showMessageDialog(null, "La fecha de emisi�n no puede estar en el futuro ni ser posterior a la fecha de pago.", "Error", JOptionPane.ERROR_MESSAGE);
							} else {
								if (horas < 0 || precio < 0) {
									JOptionPane.showMessageDialog(null, "Las horas y el precio por hora deben ser positivos.", "Error", JOptionPane.ERROR_MESSAGE);
								} else {
									if (LogicaPagoFormador.codigoRepetido(codigo)) {
										JOptionPane.showMessageDialog(null, "C�digo de factura repetido.", "Error", JOptionPane.ERROR_MESSAGE);
									} else {
										LogicaPagoFormador.nuevaFactura(cbCurso.getSelectedIndex(), cbFormador.getSelectedIndex(), codigo, dni, concepto, horas, precio, fechaEmision, fechaPago);
										JOptionPane.showMessageDialog(null, "Factura introducida con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE);
									}
								}
									
							}
							
						} catch (NumberFormatException exc) {
							JOptionPane.showMessageDialog(null, "El c�digo y el n�mero de horas deben ser n�meros enteros. El precio debe ser un n�mero real.", "Error", JOptionPane.ERROR_MESSAGE);
						} catch (ParseException exc) {
							JOptionPane.showMessageDialog(null, "Formato de fecha no v�lido.", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnRegistrar.setBounds(10, 403, 89, 23);
		frmRegistroDePago.getContentPane().add(btnRegistrar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LogicaPagoFormador.limpiar();
				frmRegistroDePago.dispose();
			}
			
		});
		btnCancelar.setBounds(354, 403, 89, 23);
		frmRegistroDePago.getContentPane().add(btnCancelar);
		
		lbCamposVacios = new JLabel("No puede haber campos vac\u00EDos");
		lbCamposVacios.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbCamposVacios.setForeground(Color.RED);
		lbCamposVacios.setBounds(10, 378, 198, 14);
		lbCamposVacios.setVisible(false);
		frmRegistroDePago.getContentPane().add(lbCamposVacios);
		
		Date ahora = new Date();
		DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		tfPago.setText(formateador.format(ahora));
		
		lbTotal = new JLabel("= 0.00 �");
		lbTotal.setBounds(337, 257, 106, 14);
		frmRegistroDePago.getContentPane().add(lbTotal);
		
		LinkedList<LinkedList<String>> cursos = LogicaPagoFormador.getCursos();
		
		for (int i = 0; i < cursos.size(); i++) {
			cbCurso.addItem(String.format("%s -- %s (Espacio: %s)", cursos.get(i).get(0), cursos.get(i).get(1), cursos.get(i).get(2)));
		}
		
		LinkedList<LinkedList<String>> formadores = LogicaPagoFormador.getFormadores();
		
		for (int i = 0; i < formadores.size(); i++) {
			cbFormador.addItem(String.format("%s -- %s", formadores.get(i).get(0), formadores.get(i).get(1)));
		}
	}
}
