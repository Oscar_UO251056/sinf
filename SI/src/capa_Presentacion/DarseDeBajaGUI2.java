package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import capa_Datos.DriverDB;
import capa_Logica.DarseDeBaja;
import capa_Logica.NuevaEdicionCurso;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import java.awt.Color;
import javax.swing.border.BevelBorder;

public class DarseDeBajaGUI2 extends JFrame {

	private final DefaultListModel model=new DefaultListModel();

	private JPanel contentPane;
	private DarseDeBajaGUI baja1;
	private DarseDeBajaGUI2 actual;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DarseDeBajaGUI2 frame = new DarseDeBajaGUI2();
					frame.setVisible(true);


				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	private DarseDeBajaGUI2() {
		new DarseDeBajaGUI2(null);
	}
	
	
	public DarseDeBajaGUI2(DarseDeBajaGUI baja) {
		
		baja1=baja;
		actual=this;
		setResizable(false);
		setTitle("Dar de baja");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 572, 498);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);


		String listaCursos[]=null;




		JLabel lblSeleccionarColegiado = new JLabel("Seleccionar colegiado:");
		lblSeleccionarColegiado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSeleccionarColegiado.setBounds(67, 24, 165, 16);
		panel.add(lblSeleccionarColegiado);

		JList lColegiados = new JList();
		

		JButton bSeleccionar = new JButton("Seleccionar");
		bSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { //llama a crear curso pero con datos del seleccionado

				if(lColegiados.getSelectedValue()==null) {
					JOptionPane.showMessageDialog(null,"Debe elegir un colegiado" ,"Error " , JOptionPane.ERROR_MESSAGE);
				}
				else {
					
					String linea=lColegiados.getSelectedValue().toString();
					System.out.println(linea);
					String id_col =linea.split(":")[0];
					baja1.ponerID(id_col);
					actual.dispose();
					
				}

			}
		});
		bSeleccionar.setBounds(200, 400, 153, 25);
		panel.add(bSeleccionar);


		lColegiados.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lColegiados.setBackground(Color.WHITE);
		lColegiados.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lColegiados.setBounds(88, 53, 389, 305);
		panel.add(lColegiados);
		//

		lColegiados.setModel(model);

		DarseDeBaja.obtenerColegiados(model,baja1.getNombre());


	}
}
