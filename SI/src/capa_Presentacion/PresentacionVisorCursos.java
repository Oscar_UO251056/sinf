package capa_Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import capa_Logica.LogicaVisorCursos;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;


public class PresentacionVisorCursos {

	private JFrame frmVisorDeCursos;
	
	private JLabel lbFecha;
	private JLabel lbInscripciones;
	private JLabel lbPatrocinios;
	private JLabel lbFormadores;
	private JLabel lbInstalaciones;
	private JLabel lbBalance;
	private JComboBox cbCurso;
	
	private PresentacionCancelarCurso cancelar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PresentacionVisorCursos window = new PresentacionVisorCursos();
					window.frmVisorDeCursos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PresentacionVisorCursos() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		PresentacionCancelarCurso.main(new String[0]);
		
		frmVisorDeCursos = new JFrame();
		frmVisorDeCursos.setResizable(false);
		frmVisorDeCursos.setTitle("Visor de cursos");
		frmVisorDeCursos.setBounds(100, 100, 439, 355);
		frmVisorDeCursos.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmVisorDeCursos.getContentPane().setLayout(null);
		
		JLabel lblCurso = new JLabel("Curso:");
		lblCurso.setBounds(10, 34, 46, 14);
		frmVisorDeCursos.getContentPane().add(lblCurso);
		
		cbCurso = new JComboBox();
		cbCurso.setBounds(89, 31, 217, 20);
		frmVisorDeCursos.getContentPane().add(cbCurso);
		
		JButton btnVisualizar = new JButton("Visualizar");
		btnVisualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaVisorCursos.limpiar();
				if (cbCurso.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog(null, "Curso seleccionado no v�lido.", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					int index = cbCurso.getSelectedIndex();
					
					int inscritos = LogicaVisorCursos.getInscritos(index);
					int plazas = LogicaVisorCursos.getPlazas(index);
					double inscripciones = LogicaVisorCursos.getInscripciones(index);
					
					lbInscripciones.setText(String.format("Inscripciones (%d/%d): %.2f �", inscritos, plazas, inscripciones));
					
					double patrocinios = LogicaVisorCursos.getPatrocinios(index);
					
					lbPatrocinios.setText(String.format("Patrocinios: %.2f �", patrocinios));
					
					double formadores = LogicaVisorCursos.getFormadores(index);
					
					lbFormadores.setText(String.format("Formadores: %.2f �", formadores));
					
					double instalaciones = LogicaVisorCursos.getInstalaciones(index);
					
					lbInstalaciones.setText(String.format("Instalaciones: %.2f �", instalaciones));
					
					double balance = LogicaVisorCursos.getBalance(index);
					
					lbBalance.setText(String.format("Balance: %.2f �", balance));
				}
			}
		});
		btnVisualizar.setBounds(316, 30, 107, 23);
		frmVisorDeCursos.getContentPane().add(btnVisualizar);
		
		JLabel lblNewLabel = new JLabel("Ingresos");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 80, 128, 28);
		frmVisorDeCursos.getContentPane().add(lblNewLabel);
		
		lbInscripciones = new JLabel("Inscripciones (0/0): 0.00 \u20AC");
		lbInscripciones.setBounds(10, 119, 177, 14);
		frmVisorDeCursos.getContentPane().add(lbInscripciones);
		
		lbPatrocinios = new JLabel("Patrocinios: 0.00 \u20AC");
		lbPatrocinios.setBounds(10, 144, 177, 14);
		frmVisorDeCursos.getContentPane().add(lbPatrocinios);
		
		JLabel lblGastos = new JLabel("Gastos");
		lblGastos.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblGastos.setBounds(246, 85, 98, 19);
		frmVisorDeCursos.getContentPane().add(lblGastos);
		
		lbFormadores = new JLabel("Formadores: 0.00 \u20AC");
		lbFormadores.setBounds(246, 119, 160, 14);
		frmVisorDeCursos.getContentPane().add(lbFormadores);
		
		lbInstalaciones = new JLabel("Instalaciones: 0.00 \u20AC");
		lbInstalaciones.setBounds(246, 144, 177, 14);
		frmVisorDeCursos.getContentPane().add(lbInstalaciones);
		
		lbBalance = new JLabel("Balance: 0 \u20AC");
		lbBalance.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbBalance.setBounds(154, 199, 217, 60);
		frmVisorDeCursos.getContentPane().add(lbBalance);
		
		JButton btnCancelarCurso = new JButton("Cancelar curso");
		btnCancelarCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaVisorCursos.limpiar();
				LogicaVisorCursos.rellenar(cbCurso.getSelectedIndex());
				PresentacionCancelarCurso.thisFrame.setVisible(true);
				
				LogicaVisorCursos.genDevs(cbCurso.getSelectedIndex());
				
				LogicaVisorCursos.cancelarCurso(cbCurso.getSelectedIndex());
				
				LinkedList<LinkedList<String>> cursos = LogicaVisorCursos.getCursos();
				
				cbCurso.removeAllItems();
				for (int i = 0; i < cursos.size(); i++) {
					cbCurso.addItem(String.format("%s -- %s (Espacio: %s)", cursos.get(i).get(0), cursos.get(i).get(1), cursos.get(i).get(2)));
					
				}
			}
		});
		btnCancelarCurso.setBounds(10, 292, 128, 23);
		frmVisorDeCursos.getContentPane().add(btnCancelarCurso);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaVisorCursos.limpiar();
				PresentacionCancelarCurso.thisFrame.dispose();
				frmVisorDeCursos.dispose();
			}
		});
		btnSalir.setBounds(334, 292, 89, 23);
		frmVisorDeCursos.getContentPane().add(btnSalir);
		
		lbFecha = new JLabel("fecha actual: 0000-00-00");
		lbFecha.setBounds(264, 6, 159, 14);
		frmVisorDeCursos.getContentPane().add(lbFecha);
		
		Date ahora = new Date();
		
		DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		
		lbFecha.setText(String.format("Fecha actual: %s", formateador.format(ahora)));
		
		LinkedList<LinkedList<String>> cursos = LogicaVisorCursos.getCursos();
		
		for (int i = 0; i < cursos.size(); i++) {
			cbCurso.addItem(String.format("%s -- %s (Espacio: %s)", cursos.get(i).get(0), cursos.get(i).get(1), cursos.get(i).get(2)));
		}
	}

}
