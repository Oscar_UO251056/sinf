package capa_Presentacion;

import javax.swing.ButtonGroup;
import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JSeparator;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;

import capa_Datos.DriverDB;
import capa_Logica.SolicitarAlta;
import capa_Logica.InscribirseACurso;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSplitPane;
import javax.swing.AbstractAction;
import javax.swing.Action;

public class InscribirseACursoGUI {

	public static JFrame frame;
	public JTextField tfSocio;
	public JTextField tfNombre;
	public JTextField tfNumero;
	public static JButton btnAceptar;
	public static JButton bAceptar;
	public static JButton bVerificarExt;
	public static JComboBox cb;
	public static JLabel tfCurso;
	public static JTextField tfDNIext;
	public JTextField tfNombreExterno;
	public JTextField tfDireccion;
	public JTextField tfTelefono;
	public JTextField tfEmpresa;
	public JTextField tfPoblacion;
	public JRadioButton SI;
	public JRadioButton NO;
	public ButtonGroup grupo1;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InscribirseACursoGUI window = new InscribirseACursoGUI();
					window.frame.setVisible(true);
					InscribirseACurso logica = new InscribirseACurso();
					String CursosAbiertos=logica.rellenarCombo();
					int numeroCursos=CursosAbiertos.split("\n").length;
					for(int cont=0;cont<numeroCursos;cont++){
						cb.addItem(CursosAbiertos.split("\n")[cont]);
							
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InscribirseACursoGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 804, 616);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		
		JLabel lbleresEstudiante = new JLabel("\u00BFEres estudiante?");
		lbleresEstudiante.setBounds(416, 460, 125, 14);
		frame.getContentPane().add(lbleresEstudiante);
		
		SI = new JRadioButton("SI");
		SI.setSelected(true);
		SI.setBounds(563, 456, 46, 23);
		frame.getContentPane().add(SI);
		
		NO = new JRadioButton("NO");
		NO.setBounds(621, 456, 109, 23);
		frame.getContentPane().add(NO);
		
		grupo1 = new ButtonGroup();
		grupo1.add(SI);
		grupo1.add(NO);
		
		bAceptar = new JButton("Aceptar");
		bAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscribirseACurso logica=new InscribirseACurso();
				if(!logica.VerificarExterno(tfDNIext.getText())){
					if(tfDNIext.getText().equals("") || tfNombreExterno.getText().equals("") || tfDireccion.getText().equals("") || tfPoblacion.getText().equals("") || tfTelefono.getText().equals("")){
						JOptionPane.showMessageDialog(null,"Te faltan campos por rellenar \nEl �nico campo no indispensable es: Empresa", "INFORMACI�N",JOptionPane.WARNING_MESSAGE);
					}else{
						if(SI.isSelected()){
							logica.insertarExterno(tfDNIext.getText(), tfNombreExterno.getText(), tfDireccion.getText(), tfPoblacion.getText(), tfTelefono.getText(), "SI", tfEmpresa.getText());
							String id= logica.SacarIdExterno(tfDNIext.getText());
							if (logica.InscribirExterno(id, tfCurso.getText())){
								DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
								String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
								double cantidad=logica.SacarCantidadAAbonarExterno(tfCurso.getText(), logica.EstudianteOtro(id));
								JustificanteInscibirseACurso justificante=new JustificanteInscibirseACurso();
								justificante.textArea.setText("Nombre: "+tfNombreExterno.getText()+"\nSiendo usted "+logica.EstudianteOtro(id)+"\n"+
								"\nFecha de solicitud: "+fecha_inscripcion+"\nCantidad a abonar: "+cantidad+"\nDNI: "+tfDNIext.getText()+
								"\nEl n�mero de cuenta donde tiene que hacer el ingreso es: 4125 2369 4785 2156 4123 5896");
								System.out.println("ENHORABUENA:se ha pre-inscrito a este curso \nTiene 48 horas para pagar");
								justificante.frame.setVisible(true);
							}
						}
					}
					System.out.println(SI.isSelected());
					
				}else{
					String id=logica.SacarIdExterno(tfDNIext.getText());
					if (logica.InscribirExterno(id, tfCurso.getText())){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
						double cantidad=logica.SacarCantidadAAbonarExterno(tfCurso.getText(), logica.EstudianteOtro(id));
						JustificanteInscibirseACurso justificante=new JustificanteInscibirseACurso();
						justificante.textArea.setText("Nombre: "+tfNombreExterno.getText()+"\nSiendo usted "+logica.EstudianteOtro(id)+"\n"+
						"\nFecha de solicitud: "+fecha_inscripcion+"\nCantidad a abonar: "+cantidad+"\nDNI: "+tfDNIext.getText()+
						"\nEl n�mero de cuenta donde tiene que hacer el ingreso es: 4125 2369 4785 2156 4123 5896");
						System.out.println("ENHORABUENA:se ha pre-inscrito a este curso \nTiene 48 horas para pagar");
						justificante.frame.setVisible(true);
					}
				}
				
				
			}
		});
		bAceptar.setBounds(422, 501, 308, 23);
		frame.getContentPane().add(bAceptar);
		
		JLabel label = new JLabel("Nombre:");
		label.setBounds(416, 297, 74, 14);
		frame.getContentPane().add(label);
		
		tfPoblacion = new JTextField();
		tfPoblacion.setBounds(522, 388, 208, 20);
		frame.getContentPane().add(tfPoblacion);
		tfPoblacion.setColumns(10);
		
		JLabel lblPoblacin = new JLabel("Poblaci\u00F3n:");
		lblPoblacin.setBounds(416, 385, 74, 26);
		frame.getContentPane().add(lblPoblacin);
		
		
		tfCurso = new JLabel("");
		tfCurso.setBounds(165, 102, 259, 14);
		frame.getContentPane().add(tfCurso);
		
		tfNombreExterno = new JTextField();
		tfNombreExterno.setBounds(522, 294, 208, 20);
		frame.getContentPane().add(tfNombreExterno);
		tfNombreExterno.setColumns(10);
		
		tfDireccion = new JTextField();
		tfDireccion.setBounds(522, 325, 208, 20);
		frame.getContentPane().add(tfDireccion);
		tfDireccion.setColumns(10);
		
		tfTelefono = new JTextField();
		tfTelefono.setBounds(522, 357, 208, 20);
		frame.getContentPane().add(tfTelefono);
		tfTelefono.setColumns(10);
		
		tfEmpresa = new JTextField();
		tfEmpresa.setBounds(522, 419, 208, 20);
		frame.getContentPane().add(tfEmpresa);
		tfEmpresa.setColumns(10);
		
		JLabel lblInscripcinACurso = new JLabel("INSCRIPCI\u00D3N A CURSO");
		lblInscripcinACurso.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblInscripcinACurso.setBounds(260, 11, 259, 23);
		frame.getContentPane().add(lblInscripcinACurso);
		
		JLabel lblNewLabel = new JLabel("N\u00BA del curso:");
		lblNewLabel.setBounds(80, 102, 74, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("N\u00BA de socio:");
		lblNewLabel_1.setBounds(33, 204, 74, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 275, 339, 2);
		frame.getContentPane().add(separator);
		
		JLabel lblNewLabel_2 = new JLabel("Nombre:");
		lblNewLabel_2.setBounds(33, 303, 74, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("DNI:");
		lblNewLabel_3.setBounds(33, 328, 74, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setEnabled(false);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscribirseACurso logica = new InscribirseACurso();
				try {
					if (logica.inscripcionCurso(tfSocio.getText(), tfCurso.getText())){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
						double cantidad=logica.SacarCantidadAAbonar(tfCurso.getText(), logica.ColegiadoOPrecolegiado(tfSocio.getText()));
						String numeroCuenta=logica.SacarNumeroCuenta(tfSocio.getText());
						JustificanteInscibirseACurso justificante=new JustificanteInscibirseACurso();
						justificante.textArea.setText("Nombre: "+tfNombre.getText()+"\nN�mero de "+logica.ColegiadoOPrecolegiado(tfSocio.getText())+": "+tfSocio.getText()+"\n"+
						"\nFecha de solicitud: "+fecha_inscripcion+"\nCantidad a abonar: "+cantidad+"\nDNI: "+numeroCuenta+
						"\nEl n�mero de cuenta donde tiene que hacer el ingreso es: 4125 2369 4785 2156 4123 5896");
						System.out.println("ENHORABUENA:se ha pre-inscrito a este curso \nTiene 48 horas para pagar");
						justificante.frame.setVisible(true);
					}else{
						JOptionPane.showMessageDialog(null,"Lo sentimos, no quedan plazas libres para este curso", "Aviso",JOptionPane.WARNING_MESSAGE);
						if(!tfSocio.getText().equals("") && !tfCurso.getText().equals("")){
							if(logica.ExisteElSocio(tfSocio.getText()) && !logica.ExisteLaInscripcion(tfSocio.getText(),tfCurso.getText())){
								btnAceptar.setEnabled(false);
							}
							
						}
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAceptar.setBounds(33, 372, 314, 23);
		frame.getContentPane().add(btnAceptar);
		
		tfSocio = new JTextField();
		tfSocio.setBounds(139, 201, 208, 20);
		frame.getContentPane().add(tfSocio);
		tfSocio.setColumns(10);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(139, 294, 208, 20);
		frame.getContentPane().add(tfNombre);
		tfNombre.setColumns(10);
		
		tfNumero = new JTextField();
		tfNumero.setBounds(139, 325, 208, 20);
		frame.getContentPane().add(tfNumero);
		tfNumero.setColumns(10);
		
		JButton btnOk = new JButton("Verificar datos");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscribirseACurso logica = new InscribirseACurso();
				if(!tfSocio.getText().equals("")){
					if(logica.ExisteElSocio(tfSocio.getText()) && !logica.ExisteLaInscripcion(tfSocio.getText(),tfCurso.getText())){
						tfNombre.setText(logica.SacarNombre(tfSocio.getText()));
						tfNumero.setText(logica.SacarNumeroCuenta(tfSocio.getText()));
						tfSocio.setEnabled(false);
						btnAceptar.setEnabled(true);
						tfDNIext.setEnabled(false);
						bVerificarExt.setEnabled(false);
						tfNombreExterno.setEnabled(false);
						tfDireccion.setEnabled(false);
						tfPoblacion.setEnabled(false);
						tfTelefono.setEnabled(false);
						tfEmpresa.setEnabled(false);
						SI.setEnabled(false);
						NO.setEnabled(false);
						bAceptar.setEnabled(false);
					}
					if(!logica.ExisteElSocio(tfSocio.getText())){
						JOptionPane.showMessageDialog(null,"Usted no es colegiado/precolegiado", "ERROR",JOptionPane.WARNING_MESSAGE);
						
					}
					if(logica.ExisteLaInscripcion(tfSocio.getText(),tfCurso.getText())){
						JOptionPane.showMessageDialog(null,"YA EST� INSCRITO EN EL CURSO", "Aviso",JOptionPane.WARNING_MESSAGE);
					}
				}else{
					JOptionPane.showMessageDialog(null,"Te faltan campos por rellenar", "Aviso",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnOk.setBounds(33, 240, 316, 23);
		frame.getContentPane().add(btnOk);
		
		cb = new JComboBox();
		cb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String curso=(String) cb.getSelectedItem();
				tfCurso.setText(curso.split(" ")[0]);
			}
		});
		cb.setBounds(80, 69, 589, 20);
		frame.getContentPane().add(cb);
		
		JLabel lblLosCursosAbiertos = new JLabel("Selecciona un curso:");
		lblLosCursosAbiertos.setBounds(80, 45, 394, 14);
		frame.getContentPane().add(lblLosCursosAbiertos);
		
		
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(434, 275, 339, 2);
		frame.getContentPane().add(separator_2);
		
		tfDNIext = new JTextField();
		tfDNIext.setBounds(522, 201, 208, 20);
		frame.getContentPane().add(tfDNIext);
		tfDNIext.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("COLEGIADO/PRECOLEGIADO");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_4.setBounds(65, 156, 272, 14);
		frame.getContentPane().add(lblNewLabel_4);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(10, 127, 734, 2);
		frame.getContentPane().add(separator_3);
		
		JLabel lblOtro_1 = new JLabel("OTRO");
		lblOtro_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblOtro_1.setBounds(551, 158, 181, 14);
		frame.getContentPane().add(lblOtro_1);
		
		JLabel lblDni_1 = new JLabel("DNI:");
		lblDni_1.setBounds(416, 204, 74, -23);
		frame.getContentPane().add(lblDni_1);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setOrientation(SwingConstants.VERTICAL);
		separator_4.setBounds(377, 127, 2, 358);
		frame.getContentPane().add(separator_4);
		
		
		
		JLabel lblDireccin_1 = new JLabel("Direcci\u00F3n:");
		lblDireccin_1.setBounds(416, 328, 74, 14);
		frame.getContentPane().add(lblDireccin_1);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono:");
		lblTelfono.setBounds(416, 360, 74, 14);
		frame.getContentPane().add(lblTelfono);
		
		JLabel lblEmpresaopcional = new JLabel("Empresa:");
		lblEmpresaopcional.setBounds(416, 422, 103, 14);
		frame.getContentPane().add(lblEmpresaopcional);
		
		
		JLabel lblDni = new JLabel("DNI:");
		lblDni.setBounds(416, 204, 95, 14);
		frame.getContentPane().add(lblDni);
		
		bVerificarExt = new JButton("Verificar datos");
		bVerificarExt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bAceptar.setEnabled(true);
				if(!tfDNIext.getText().equals("")){
					InscribirseACurso logica = new InscribirseACurso();
					if(logica.VerificarExterno(tfDNIext.getText())){
						tfDNIext.setEnabled(false);
						tfNombreExterno.setText(logica.extraerNombreExterno(tfDNIext.getText()));
						tfNombreExterno.setEnabled(false);
						tfDireccion.setText(logica.extraerDireccionExterno(tfDNIext.getText()));
						tfDireccion.setEnabled(false);
						tfPoblacion.setText(logica.extraerPoblacionExterno(tfDNIext.getText()));
						tfPoblacion.setEnabled(false);
						tfTelefono.setText(logica.extraerTelefonoExterno(tfDNIext.getText()));
						tfTelefono.setEnabled(false);
						tfEmpresa.setText(logica.extraerEmpresaExterno(tfDNIext.getText()));
						tfEmpresa.setEnabled(false);
						if(logica.EsEstudiante(tfDNIext.getText())){
							SI.setSelected(true);
							NO.setSelected(false);
							SI.setEnabled(false);
							NO.setEnabled(false);
						}else{
							SI.setSelected(false);
							NO.setSelected(true);
							SI.setEnabled(false);
							NO.setEnabled(false);
						}
						String id=logica.SacarIdExterno(tfDNIext.getText());
						if(logica.ExisteInscripcionExterno(id, tfCurso.getText())){
							JOptionPane.showMessageDialog(null,"Ya est� inscrito en este curso", "INFORMACION",JOptionPane.WARNING_MESSAGE);
							bAceptar.setEnabled(false);
						}
						
					}else{
						JOptionPane.showMessageDialog(null,"Rellena los siguientes campos", "Atenci�n",JOptionPane.WARNING_MESSAGE);
					}
				}else{
					JOptionPane.showMessageDialog(null,"A�ade tu DNI", "ERROR",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		bVerificarExt.setBounds(416, 240, 316, 23);
		frame.getContentPane().add(bVerificarExt);
		
		
	}
}