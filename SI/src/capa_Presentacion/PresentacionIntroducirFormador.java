package capa_Presentacion;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

import capa_Logica.LogicaIntroducirFormador;

public class PresentacionIntroducirFormador {

	private JFrame frmNuevoFormador;
	private JTextField tfDNI;
	private JTextField tfNombre;
	private JTextField tfTitulacion;
	private JTextField tfRama;
	JLabel lCamposNulos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PresentacionIntroducirFormador window = new PresentacionIntroducirFormador();
					window.frmNuevoFormador.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PresentacionIntroducirFormador() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNuevoFormador = new JFrame();
		frmNuevoFormador.setResizable(false);
		frmNuevoFormador.setTitle("Nuevo Formador");
		frmNuevoFormador.setBounds(100, 100, 450, 283);
		frmNuevoFormador.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmNuevoFormador.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("DNI:");
		lblNewLabel.setBounds(230, 27, 53, 14);
		frmNuevoFormador.getContentPane().add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(10, 63, 97, 14);
		frmNuevoFormador.getContentPane().add(lblNombre);
		
		JLabel lblTitulacin = new JLabel("Titulaci\u00F3n: ");
		lblTitulacin.setBounds(10, 104, 97, 14);
		frmNuevoFormador.getContentPane().add(lblTitulacin);
		
		JLabel lblRamaDeFormacin = new JLabel("Rama de formaci\u00F3n:");
		lblRamaDeFormacin.setBounds(10, 145, 130, 14);
		frmNuevoFormador.getContentPane().add(lblRamaDeFormacin);
		
		JButton bIntroducir = new JButton("Introducir");
		bIntroducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfDNI.getText().isEmpty() || tfNombre.getText().isEmpty() || tfTitulacion.getText().isEmpty() || tfRama.getText().isEmpty()) {
					lCamposNulos.setVisible(true);
				} else {
					String dni = tfDNI.getText();
					String nombre = tfNombre.getText();
					String titulacion = tfTitulacion.getText();
					String rama = tfRama.getText();
					
					if (!LogicaIntroducirFormador.dniRepetido(dni)) {
					
						LogicaIntroducirFormador.nuevoFormador(dni, nombre, titulacion, rama);
					
						JOptionPane.showMessageDialog(frmNuevoFormador, "Formador introducido con �xito.", "Exito", JOptionPane.INFORMATION_MESSAGE );
						
					} else {
						JOptionPane.showMessageDialog(frmNuevoFormador, "Error: el dni ya est� en el registro de formadores.", "Error", JOptionPane.ERROR_MESSAGE );
					}
				}
			}
		});
		bIntroducir.setBounds(10, 220, 104, 23);
		frmNuevoFormador.getContentPane().add(bIntroducir);
		
		JButton bCancelar = new JButton("Cancelar");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmNuevoFormador.dispose();
			}
		});
		bCancelar.setBounds(330, 220, 104, 23);
		frmNuevoFormador.getContentPane().add(bCancelar);
		
		tfDNI = new JTextField();
		tfDNI.setBounds(293, 24, 141, 20);
		frmNuevoFormador.getContentPane().add(tfDNI);
		tfDNI.setColumns(10);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(100, 60, 334, 20);
		frmNuevoFormador.getContentPane().add(tfNombre);
		tfNombre.setColumns(10);
		
		tfTitulacion = new JTextField();
		tfTitulacion.setBounds(100, 101, 334, 20);
		frmNuevoFormador.getContentPane().add(tfTitulacion);
		tfTitulacion.setColumns(10);
		
		tfRama = new JTextField();
		tfRama.setBounds(150, 142, 284, 20);
		frmNuevoFormador.getContentPane().add(tfRama);
		tfRama.setColumns(10);
		
		lCamposNulos = new JLabel("Ningun campo puede ser nulo.");
		lCamposNulos.setForeground(Color.RED);
		lCamposNulos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lCamposNulos.setBounds(10, 195, 207, 14);
		frmNuevoFormador.getContentPane().add(lCamposNulos);
		lCamposNulos.setVisible(false);
	}
}
