package capa_Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLayeredPane;
import javax.swing.JTabbedPane;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class PrincipalGUI extends JFrame {

	private JPanel contentPane;
	private static String argsp[]= null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		argsp=args;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalGUI frame = new PrincipalGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalGUI() {
		setTitle("Herramienta oficial del COIIPA");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 578, 473);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel pBienvenida = new JPanel();
		tabbedPane.addTab("Bienvenido", null, pBienvenida, null);
		pBienvenida.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(PrincipalGUI.class.getResource("/capa_Presentacion/logoCOIIPA.png")));
		lblNewLabel.setBounds(53, 122, 395, 182);
		pBienvenida.add(lblNewLabel);
		
		JLabel lblBienvenidoALa = new JLabel("Bienvenid@ a la herramienta oficial del COIIPA.");
		lblBienvenidoALa.setBounds(74, 40, 276, 44);
		pBienvenida.add(lblBienvenidoALa);
		
		JLabel lblSistemasDe = new JLabel("2018 Sistemas de la Informaci\u00F3n");
		lblSistemasDe.setBounds(324, 351, 203, 16);
		pBienvenida.add(lblSistemasDe);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				System.exit(0);
			}
		});
		btnSalir.setBounds(53, 347, 97, 25);
		pBienvenida.add(btnSalir);
		
		JPanel pAdminCursos = new JPanel();
		tabbedPane.addTab("Administración de cursos", null, pAdminCursos, null);
		GridBagLayout gbl_pAdminCursos = new GridBagLayout();
		gbl_pAdminCursos.columnWidths = new int[] {450, 100, 0};
		gbl_pAdminCursos.rowHeights = new int[] {45, 45, 45, 45, 0};
		gbl_pAdminCursos.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_pAdminCursos.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		pAdminCursos.setLayout(gbl_pAdminCursos);
		
		JLabel lblPlanificacinDeCursos = new JLabel("Planificaci\u00F3n de cursos");
		GridBagConstraints gbc_lblPlanificacinDeCursos = new GridBagConstraints();
		gbc_lblPlanificacinDeCursos.fill = GridBagConstraints.BOTH;
		gbc_lblPlanificacinDeCursos.insets = new Insets(0, 0, 5, 5);
		gbc_lblPlanificacinDeCursos.gridx = 0;
		gbc_lblPlanificacinDeCursos.gridy = 0;
		pAdminCursos.add(lblPlanificacinDeCursos, gbc_lblPlanificacinDeCursos);
		
		JButton btnAcceder = new JButton("Acceder >>");
		btnAcceder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new PresentacionPlanificarCurso();
				PresentacionPlanificarCurso.main(argsp);
				
				
			}
		});
		GridBagConstraints gbc_btnAcceder = new GridBagConstraints();
		gbc_btnAcceder.fill = GridBagConstraints.BOTH;
		gbc_btnAcceder.insets = new Insets(0, 0, 5, 0);
		gbc_btnAcceder.gridx = 1;
		gbc_btnAcceder.gridy = 0;
		pAdminCursos.add(btnAcceder, gbc_btnAcceder);
		
		JLabel lblAbrirCursoA = new JLabel("Abrir curso a inscripciones");
		GridBagConstraints gbc_lblAbrirCursoA = new GridBagConstraints();
		gbc_lblAbrirCursoA.fill = GridBagConstraints.BOTH;
		gbc_lblAbrirCursoA.insets = new Insets(0, 0, 5, 5);
		gbc_lblAbrirCursoA.gridx = 0;
		gbc_lblAbrirCursoA.gridy = 1;
		pAdminCursos.add(lblAbrirCursoA, gbc_lblAbrirCursoA);
		
		JButton button = new JButton("Acceder >>");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new PresentacionAbrirCurso();
				PresentacionAbrirCurso.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.fill = GridBagConstraints.BOTH;
		gbc_button.insets = new Insets(0, 0, 5, 0);
		gbc_button.gridx = 1;
		gbc_button.gridy = 1;
		pAdminCursos.add(button, gbc_button);
		
		JLabel lblCrearNuevaEdicin = new JLabel("Crear nueva edici\u00F3n de curso");
		GridBagConstraints gbc_lblCrearNuevaEdicin = new GridBagConstraints();
		gbc_lblCrearNuevaEdicin.fill = GridBagConstraints.BOTH;
		gbc_lblCrearNuevaEdicin.insets = new Insets(0, 0, 5, 5);
		gbc_lblCrearNuevaEdicin.gridx = 0;
		gbc_lblCrearNuevaEdicin.gridy = 2;
		pAdminCursos.add(lblCrearNuevaEdicin, gbc_lblCrearNuevaEdicin);
		
		JButton button_1 = new JButton("Acceder >>");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				new NuevaEdicionCursoGUI().setVisible(true);
				
			}
		});
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.fill = GridBagConstraints.BOTH;
		gbc_button_1.insets = new Insets(0, 0, 5, 0);
		gbc_button_1.gridx = 1;
		gbc_button_1.gridy = 2;
		pAdminCursos.add(button_1, gbc_button_1);
		
		JLabel lblVisorDeCursos = new JLabel("Visor de cursos");
		GridBagConstraints gbc_lblVisorDeCursos = new GridBagConstraints();
		gbc_lblVisorDeCursos.fill = GridBagConstraints.BOTH;
		gbc_lblVisorDeCursos.insets = new Insets(0, 0, 0, 5);
		gbc_lblVisorDeCursos.gridx = 0;
		gbc_lblVisorDeCursos.gridy = 3;
		pAdminCursos.add(lblVisorDeCursos, gbc_lblVisorDeCursos);
		
		JButton button_2 = new JButton("Acceder >>");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new PresentacionVisorCursos();
				PresentacionVisorCursos.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.fill = GridBagConstraints.BOTH;
		gbc_button_2.gridx = 1;
		gbc_button_2.gridy = 3;
		pAdminCursos.add(button_2, gbc_button_2);
		
		JPanel pPagos = new JPanel();
		tabbedPane.addTab("Pagos", null, pPagos, null);
		GridBagLayout gbl_pPagos = new GridBagLayout();
		gbl_pPagos.columnWidths = new int[] {450, 100, 0};
		gbl_pPagos.rowHeights = new int[] {45, 45, 45, 45, 45, 45};
		gbl_pPagos.columnWeights = new double[]{0.0, 0.0, 0.0};
		gbl_pPagos.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		pPagos.setLayout(gbl_pPagos);
		
		JLabel lblEmitirRecibosDe = new JLabel("Emitir recibos de cuotas");
		lblEmitirRecibosDe.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblEmitirRecibosDe = new GridBagConstraints();
		gbc_lblEmitirRecibosDe.fill = GridBagConstraints.BOTH;
		gbc_lblEmitirRecibosDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmitirRecibosDe.gridx = 0;
		gbc_lblEmitirRecibosDe.gridy = 0;
		pPagos.add(lblEmitirRecibosDe, gbc_lblEmitirRecibosDe);
		
		JButton btnAcceder_1 = new JButton("Acceder >>");
		btnAcceder_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new EmisionReciboGUI().setVisible(true);
			}
		});
		GridBagConstraints gbc_btnAcceder_1 = new GridBagConstraints();
		gbc_btnAcceder_1.fill = GridBagConstraints.BOTH;
		gbc_btnAcceder_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnAcceder_1.gridx = 1;
		gbc_btnAcceder_1.gridy = 0;
		pPagos.add(btnAcceder_1, gbc_btnAcceder_1);
		
		JLabel lblRegistroDePago = new JLabel("Registro de pago de inscripciones");
		GridBagConstraints gbc_lblRegistroDePago = new GridBagConstraints();
		gbc_lblRegistroDePago.fill = GridBagConstraints.BOTH;
		gbc_lblRegistroDePago.insets = new Insets(0, 0, 5, 5);
		gbc_lblRegistroDePago.gridx = 0;
		gbc_lblRegistroDePago.gridy = 1;
		pPagos.add(lblRegistroDePago, gbc_lblRegistroDePago);
		
		JButton button_3 = new JButton("Acceder >>");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new InscritosAUnCursoGUI();
				InscritosAUnCursoGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_3 = new GridBagConstraints();
		gbc_button_3.fill = GridBagConstraints.BOTH;
		gbc_button_3.insets = new Insets(0, 0, 5, 5);
		gbc_button_3.gridx = 1;
		gbc_button_3.gridy = 1;
		pPagos.add(button_3, gbc_button_3);
		
		JLabel lblBalanceEconmicoDel = new JLabel("Balance econ\u00F3mico del colegio");
		GridBagConstraints gbc_lblBalanceEconmicoDel = new GridBagConstraints();
		gbc_lblBalanceEconmicoDel.fill = GridBagConstraints.BOTH;
		gbc_lblBalanceEconmicoDel.insets = new Insets(0, 0, 5, 5);
		gbc_lblBalanceEconmicoDel.gridx = 0;
		gbc_lblBalanceEconmicoDel.gridy = 2;
		pPagos.add(lblBalanceEconmicoDel, gbc_lblBalanceEconmicoDel);
		
		JButton button_4 = new JButton("Acceder >>");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new BalanceEconomicoGUI();
				BalanceEconomicoGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_4 = new GridBagConstraints();
		gbc_button_4.fill = GridBagConstraints.BOTH;
		gbc_button_4.insets = new Insets(0, 0, 5, 5);
		gbc_button_4.gridx = 1;
		gbc_button_4.gridy = 2;
		pPagos.add(button_4, gbc_button_4);
		
		JLabel lblRegistroDePago_1 = new JLabel("Registro de pago de cuotas");
		GridBagConstraints gbc_lblRegistroDePago_1 = new GridBagConstraints();
		gbc_lblRegistroDePago_1.fill = GridBagConstraints.BOTH;
		gbc_lblRegistroDePago_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblRegistroDePago_1.gridx = 0;
		gbc_lblRegistroDePago_1.gridy = 3;
		pPagos.add(lblRegistroDePago_1, gbc_lblRegistroDePago_1);
		
		JButton button_5 = new JButton("Acceder >>");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new RegistroCuotasGUI();
				RegistroCuotasGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_5 = new GridBagConstraints();
		gbc_button_5.fill = GridBagConstraints.BOTH;
		gbc_button_5.insets = new Insets(0, 0, 5, 5);
		gbc_button_5.gridx = 1;
		gbc_button_5.gridy = 3;
		pPagos.add(button_5, gbc_button_5);
		
		JLabel lblRegistroDePago_2 = new JLabel("Registro de pago a formadores");
		GridBagConstraints gbc_lblRegistroDePago_2 = new GridBagConstraints();
		gbc_lblRegistroDePago_2.fill = GridBagConstraints.BOTH;
		gbc_lblRegistroDePago_2.insets = new Insets(0, 0, 0, 5);
		gbc_lblRegistroDePago_2.gridx = 0;
		gbc_lblRegistroDePago_2.gridy = 4;
		pPagos.add(lblRegistroDePago_2, gbc_lblRegistroDePago_2);
		
		JButton btnAcceder_5 = new JButton("Acceder >>");
		btnAcceder_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new PresentacionPagoFormador();
				PresentacionPagoFormador.main(argsp);
				
			}
		});
		GridBagConstraints gbc_btnAcceder_5 = new GridBagConstraints();
		gbc_btnAcceder_5.fill = GridBagConstraints.BOTH;
		gbc_btnAcceder_5.insets = new Insets(0, 0, 0, 5);
		gbc_btnAcceder_5.gridx = 1;
		gbc_btnAcceder_5.gridy = 4;
		pPagos.add(btnAcceder_5, gbc_btnAcceder_5);
		
		JPanel pPericiales = new JPanel();
		tabbedPane.addTab("Periciales", null, pPericiales, null);
		GridBagLayout gbl_pPericiales = new GridBagLayout();
		gbl_pPericiales.columnWidths = new int[] {450, 100, 0};
		gbl_pPericiales.rowHeights = new int[] {45, 45, 45};
		gbl_pPericiales.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_pPericiales.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		pPericiales.setLayout(gbl_pPericiales);
		
		JLabel lblSolicitudDeAlta = new JLabel("Solicitud de alta en la lista de peritos");
		GridBagConstraints gbc_lblSolicitudDeAlta = new GridBagConstraints();
		gbc_lblSolicitudDeAlta.fill = GridBagConstraints.BOTH;
		gbc_lblSolicitudDeAlta.insets = new Insets(0, 0, 5, 5);
		gbc_lblSolicitudDeAlta.gridx = 0;
		gbc_lblSolicitudDeAlta.gridy = 0;
		pPericiales.add(lblSolicitudDeAlta, gbc_lblSolicitudDeAlta);
		
		JButton btnAcceder_3 = new JButton("Acceder >>");
		btnAcceder_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new AltaPerito2GUI();
				AltaPerito2GUI.main(argsp);
			}
		});
		GridBagConstraints gbc_btnAcceder_3 = new GridBagConstraints();
		gbc_btnAcceder_3.fill = GridBagConstraints.BOTH;
		gbc_btnAcceder_3.insets = new Insets(0, 0, 5, 0);
		gbc_btnAcceder_3.gridx = 1;
		gbc_btnAcceder_3.gridy = 0;
		pPericiales.add(btnAcceder_3, gbc_btnAcceder_3);
		
		JLabel lblAsignarPericisl = new JLabel("Asignar pericial");
		GridBagConstraints gbc_lblAsignarPericisl = new GridBagConstraints();
		gbc_lblAsignarPericisl.fill = GridBagConstraints.BOTH;
		gbc_lblAsignarPericisl.insets = new Insets(0, 0, 0, 5);
		gbc_lblAsignarPericisl.gridx = 0;
		gbc_lblAsignarPericisl.gridy = 1;
		pPericiales.add(lblAsignarPericisl, gbc_lblAsignarPericisl);
		
		JButton button_7 = new JButton("Acceder >>");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new AsignarPericialGUI();
				AsignarPericialGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_7 = new GridBagConstraints();
		gbc_button_7.fill = GridBagConstraints.BOTH;
		gbc_button_7.gridx = 1;
		gbc_button_7.gridy = 1;
		pPericiales.add(button_7, gbc_button_7);
		
		JPanel pAreaMiembros = new JPanel();
		tabbedPane.addTab("Area de miembros", null, pAreaMiembros, null);
		GridBagLayout gbl_pAreaMiembros = new GridBagLayout();
		gbl_pAreaMiembros.columnWidths = new int[] {450, 100, 0};
		gbl_pAreaMiembros.rowHeights = new int[] {45, 45, 45, 45, 45, 45, 45, 45, 20};
		gbl_pAreaMiembros.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_pAreaMiembros.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		pAreaMiembros.setLayout(gbl_pAreaMiembros);
		
		JLabel lblDarseDeAlta = new JLabel("Darse de alta como colegiado");
		GridBagConstraints gbc_lblDarseDeAlta = new GridBagConstraints();
		gbc_lblDarseDeAlta.fill = GridBagConstraints.BOTH;
		gbc_lblDarseDeAlta.insets = new Insets(0, 0, 5, 5);
		gbc_lblDarseDeAlta.gridx = 0;
		gbc_lblDarseDeAlta.gridy = 0;
		pAreaMiembros.add(lblDarseDeAlta, gbc_lblDarseDeAlta);
		
		JButton btnAcceder_4 = new JButton("Acceder >>");
		btnAcceder_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new SolicitarAltaGUI();
				SolicitarAltaGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_btnAcceder_4 = new GridBagConstraints();
		gbc_btnAcceder_4.fill = GridBagConstraints.BOTH;
		gbc_btnAcceder_4.insets = new Insets(0, 0, 5, 0);
		gbc_btnAcceder_4.gridx = 1;
		gbc_btnAcceder_4.gridy = 0;
		pAreaMiembros.add(btnAcceder_4, gbc_btnAcceder_4);
		
		JLabel lblDarseDeBaja = new JLabel("Darse de baja como colegiado");
		GridBagConstraints gbc_lblDarseDeBaja = new GridBagConstraints();
		gbc_lblDarseDeBaja.fill = GridBagConstraints.BOTH;
		gbc_lblDarseDeBaja.insets = new Insets(0, 0, 5, 5);
		gbc_lblDarseDeBaja.gridx = 0;
		gbc_lblDarseDeBaja.gridy = 1;
		pAreaMiembros.add(lblDarseDeBaja, gbc_lblDarseDeBaja);
		
		JButton button_8 = new JButton("Acceder >>");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new DarseDeBajaGUI();
				DarseDeBajaGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_8 = new GridBagConstraints();
		gbc_button_8.fill = GridBagConstraints.BOTH;
		gbc_button_8.insets = new Insets(0, 0, 5, 0);
		gbc_button_8.gridx = 1;
		gbc_button_8.gridy = 1;
		pAreaMiembros.add(button_8, gbc_button_8);
		
		JLabel lblInscribirseAUn = new JLabel("Inscribirse a un curso");
		GridBagConstraints gbc_lblInscribirseAUn = new GridBagConstraints();
		gbc_lblInscribirseAUn.fill = GridBagConstraints.BOTH;
		gbc_lblInscribirseAUn.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscribirseAUn.gridx = 0;
		gbc_lblInscribirseAUn.gridy = 2;
		pAreaMiembros.add(lblInscribirseAUn, gbc_lblInscribirseAUn);
		
		JButton button_9 = new JButton("Acceder >>");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new InscribirseACursoGUI();
				InscribirseACursoGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_9 = new GridBagConstraints();
		gbc_button_9.fill = GridBagConstraints.BOTH;
		gbc_button_9.insets = new Insets(0, 0, 5, 0);
		gbc_button_9.gridx = 1;
		gbc_button_9.gridy = 2;
		pAreaMiembros.add(button_9, gbc_button_9);
		
		JLabel lblCancelarInscripcinA = new JLabel("Cancelar inscripci\u00F3n a un curso");
		GridBagConstraints gbc_lblCancelarInscripcinA = new GridBagConstraints();
		gbc_lblCancelarInscripcinA.fill = GridBagConstraints.BOTH;
		gbc_lblCancelarInscripcinA.insets = new Insets(0, 0, 5, 5);
		gbc_lblCancelarInscripcinA.gridx = 0;
		gbc_lblCancelarInscripcinA.gridy = 3;
		pAreaMiembros.add(lblCancelarInscripcinA, gbc_lblCancelarInscripcinA);
		
		JButton button_10 = new JButton("Acceder >>");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new CancelarInscripcionGUI();
				CancelarInscripcionGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_10 = new GridBagConstraints();
		gbc_button_10.fill = GridBagConstraints.BOTH;
		gbc_button_10.insets = new Insets(0, 0, 5, 0);
		gbc_button_10.gridx = 1;
		gbc_button_10.gridy = 3;
		pAreaMiembros.add(button_10, gbc_button_10);
		
		JLabel lblVerRecibos = new JLabel("Ver recibos");
		GridBagConstraints gbc_lblVerRecibos = new GridBagConstraints();
		gbc_lblVerRecibos.fill = GridBagConstraints.BOTH;
		gbc_lblVerRecibos.insets = new Insets(0, 0, 0, 5);
		gbc_lblVerRecibos.gridx = 0;
		gbc_lblVerRecibos.gridy = 4;
		pAreaMiembros.add(lblVerRecibos, gbc_lblVerRecibos);
		
		JButton button_11 = new JButton("Acceder >>");
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new ReciboSocioGUI();
				ReciboSocioGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_11 = new GridBagConstraints();
		gbc_button_11.fill = GridBagConstraints.BOTH;
		gbc_button_11.gridx = 1;
		gbc_button_11.gridy = 4;
		pAreaMiembros.add(button_11, gbc_button_11);
		
		JPanel pAdminMiembros = new JPanel();
		tabbedPane.addTab("Administración de miembros", null, pAdminMiembros, null);
		GridBagLayout gbl_pAdminMiembros = new GridBagLayout();
		gbl_pAdminMiembros.columnWidths = new int[] {450, 100};
		gbl_pAdminMiembros.rowHeights = new int[] {45, 45, 0};
		gbl_pAdminMiembros.columnWeights = new double[]{0.0, 0.0};
		gbl_pAdminMiembros.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		pAdminMiembros.setLayout(gbl_pAdminMiembros);
		
		JLabel lblEmitirSolicitudDe = new JLabel("Emitir solicitud de verificaci\u00F3n de titulaci\u00F3n ");
		GridBagConstraints gbc_lblEmitirSolicitudDe = new GridBagConstraints();
		gbc_lblEmitirSolicitudDe.fill = GridBagConstraints.BOTH;
		gbc_lblEmitirSolicitudDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmitirSolicitudDe.gridx = 0;
		gbc_lblEmitirSolicitudDe.gridy = 0;
		pAdminMiembros.add(lblEmitirSolicitudDe, gbc_lblEmitirSolicitudDe);
		
		JButton btnAcceder_2 = new JButton("Acceder >>");
		btnAcceder_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new EnviarLoteVerificacionGUI();
				EnviarLoteVerificacionGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_btnAcceder_2 = new GridBagConstraints();
		gbc_btnAcceder_2.fill = GridBagConstraints.BOTH;
		gbc_btnAcceder_2.insets = new Insets(0, 0, 5, 0);
		gbc_btnAcceder_2.gridx = 1;
		gbc_btnAcceder_2.gridy = 0;
		pAdminMiembros.add(btnAcceder_2, gbc_btnAcceder_2);
		
		JLabel lblRegistrarVerificacinDe = new JLabel("Registrar verificaci\u00F3n de titulaci\u00F3n");
		GridBagConstraints gbc_lblRegistrarVerificacinDe = new GridBagConstraints();
		gbc_lblRegistrarVerificacinDe.fill = GridBagConstraints.BOTH;
		gbc_lblRegistrarVerificacinDe.insets = new Insets(0, 0, 0, 5);
		gbc_lblRegistrarVerificacinDe.gridx = 0;
		gbc_lblRegistrarVerificacinDe.gridy = 1;
		pAdminMiembros.add(lblRegistrarVerificacinDe, gbc_lblRegistrarVerificacinDe);
		
		JButton button_6 = new JButton("Acceder >>");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new VerificarColegiadosGUI();
				VerificarColegiadosGUI.main(argsp);
				
			}
		});
		GridBagConstraints gbc_button_6 = new GridBagConstraints();
		gbc_button_6.fill = GridBagConstraints.BOTH;
		gbc_button_6.gridx = 1;
		gbc_button_6.gridy = 1;
		pAdminMiembros.add(button_6, gbc_button_6);
	}
}
//