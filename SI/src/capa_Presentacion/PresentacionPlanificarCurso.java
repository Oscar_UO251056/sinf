package capa_Presentacion;

import java.awt.EventQueue;


import java.util.Date;

import javax.swing.JFrame;
import capa_Logica.LogicaPlanificarCurso;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import capa_Logica.LogicaPlanificarCurso;
import capa_Presentacion.PresentacionSesiones;
import capa_Presentacion.Sesion;
import java.util.LinkedList;

public class PresentacionPlanificarCurso {

	public JFrame frmPlanificacinDeNuevo;
	private JTextField tfTitulo;
	private JTextField tfEspacio;
	private JTextField tfColegiados;
	private JTextField tfPrecolegiados;
	private JTextField tfEstudiantes;
	private JTextField tfExternos;
	private PresentacionSesiones ventanaSesiones;
	JLabel lCamposNulos;
	private LinkedList<Sesion> sesiones;
	JTextArea taSesiones;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		 
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PresentacionPlanificarCurso window = new PresentacionPlanificarCurso();
					window.frmPlanificacinDeNuevo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PresentacionPlanificarCurso() {
		initialize();
	}
	
	public PresentacionPlanificarCurso(String name, String espacio, String precio_col, String precio_pre, String precio_est, String precio_ext) {
		initialize();
		tfTitulo.setText(name);
		tfEspacio.setText(espacio);
		tfColegiados.setText(precio_col);
		tfPrecolegiados.setText(precio_pre);
		tfEstudiantes.setText(precio_est);
		tfExternos.setText(precio_ext);
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPlanificacinDeNuevo = new JFrame();
		frmPlanificacinDeNuevo.setTitle("Planificaci\u00F3n de nuevo curso");
		frmPlanificacinDeNuevo.setResizable(false);
		frmPlanificacinDeNuevo.setBounds(100, 100, 716, 422);
		frmPlanificacinDeNuevo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmPlanificacinDeNuevo.getContentPane().setLayout(null);
		
		JLabel lblTtulo = new JLabel("T\u00EDtulo:");
		lblTtulo.setBounds(10, 25, 72, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblTtulo);
		
		JLabel lblEspacio = new JLabel("Espacio: ");
		lblEspacio.setBounds(10, 66, 59, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblEspacio);
		
		tfTitulo = new JTextField();
		tfTitulo.setBounds(64, 22, 218, 20);
		frmPlanificacinDeNuevo.getContentPane().add(tfTitulo);
		tfTitulo.setColumns(10);
		
		tfEspacio = new JTextField();
		tfEspacio.setBounds(74, 63, 208, 20);
		frmPlanificacinDeNuevo.getContentPane().add(tfEspacio);
		tfEspacio.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(309, 0, 27, 338);
		frmPlanificacinDeNuevo.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 336, 710, 14);
		frmPlanificacinDeNuevo.getContentPane().add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(0, 107, 308, 14);
		frmPlanificacinDeNuevo.getContentPane().add(separator_2);
		
		JButton btnIntroducir = new JButton("Introducir");
		btnIntroducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfTitulo.getText().isEmpty() || tfEspacio.getText().isEmpty() || tfColegiados.getText().isEmpty() ||
								tfPrecolegiados.getText().isEmpty() || tfEstudiantes.getText().isEmpty() || tfExternos.getText().isEmpty()) {
					lCamposNulos.setVisible(true);
				} else {
					try {
						String titulo = tfTitulo.getText();
						String espacio = tfEspacio.getText();
						
						double precioColegiados = Double.parseDouble(tfColegiados.getText());
						double precioPrecolegiados = Double.parseDouble(tfPrecolegiados.getText());
						double precioEstudiantes = Double.parseDouble(tfEstudiantes.getText());
						double precioExternos = Double.parseDouble(tfExternos.getText());
						
						if (precioColegiados < 0 || precioPrecolegiados < 0 || precioEstudiantes < 0 || precioExternos < 0) {
							JOptionPane.showMessageDialog(frmPlanificacinDeNuevo, "Error: los precios no pueden ser negativos.", "Error", JOptionPane.ERROR_MESSAGE );
						} else {
							LogicaPlanificarCurso.nuevoCurso(titulo, espacio, precioColegiados, precioPrecolegiados, precioEstudiantes, precioExternos);
							JOptionPane.showMessageDialog(frmPlanificacinDeNuevo, "Curso introducido correctamente.", "Exito", JOptionPane.INFORMATION_MESSAGE );
							for (int i = 0; i < sesiones.size(); i++)
								LogicaPlanificarCurso.nuevaSesion(titulo, sesiones.get(i));
						}
						
					} catch (NumberFormatException excepcion) {
						JOptionPane.showMessageDialog(frmPlanificacinDeNuevo, "Error:  todos los precios deben ser n�meros.", "Error", JOptionPane.ERROR_MESSAGE );
					}
				}
			}
		});
		btnIntroducir.setBounds(10, 361, 112, 23);
		frmPlanificacinDeNuevo.getContentPane().add(btnIntroducir);
		
		JButton btnNewButton = new JButton("Cancelar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmPlanificacinDeNuevo.dispose();
			}
		});
		btnNewButton.setBounds(596, 361, 104, 23);
		frmPlanificacinDeNuevo.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Precios");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(121, 128, 125, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Sesiones");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setBounds(466, 23, 169, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblNewLabel_1);
		
		taSesiones = new JTextArea();
		taSesiones.setLineWrap(true);
		taSesiones.setFont(new Font("Monospaced", Font.PLAIN, 11));
		taSesiones.setText("Fecha y hora -- Duraci\u00F3n(h) -- Formador\r\n\r\n");
		taSesiones.setEditable(false);
		taSesiones.setBackground(new Color(255, 255, 255));
		taSesiones.setBounds(325, 45, 375, 247);
		frmPlanificacinDeNuevo.getContentPane().add(taSesiones);
		
		JLabel lblColegiados = new JLabel("Colegiados:");
		lblColegiados.setBounds(10, 168, 104, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblColegiados);
		
		JLabel lblPrecolegiados = new JLabel("Pre-colegiados:");
		lblPrecolegiados.setBounds(10, 204, 98, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblPrecolegiados);
		
		JLabel lblEstudiantes = new JLabel("Estudiantes:");
		lblEstudiantes.setBounds(10, 247, 92, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblEstudiantes);
		
		JLabel lblExternos = new JLabel("Externos:");
		lblExternos.setBounds(10, 290, 84, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lblExternos);
		
		tfColegiados = new JTextField();
		tfColegiados.setBounds(121, 165, 161, 20);
		frmPlanificacinDeNuevo.getContentPane().add(tfColegiados);
		tfColegiados.setColumns(10);
		
		tfPrecolegiados = new JTextField();
		tfPrecolegiados.setBounds(121, 201, 161, 20);
		frmPlanificacinDeNuevo.getContentPane().add(tfPrecolegiados);
		tfPrecolegiados.setColumns(10);
		
		tfEstudiantes = new JTextField();
		tfEstudiantes.setBounds(121, 244, 161, 20);
		frmPlanificacinDeNuevo.getContentPane().add(tfEstudiantes);
		tfEstudiantes.setColumns(10);
		
		tfExternos = new JTextField();
		tfExternos.setBounds(121, 287, 161, 20);
		frmPlanificacinDeNuevo.getContentPane().add(tfExternos);
		tfExternos.setColumns(10);
		
		JButton btnAadir = new JButton("A\u00F1adir");
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ventanaSesiones.actualizaFormadores();
				ventanaSesiones.setVisible(true);
			}
		});
		btnAadir.setBounds(465, 303, 89, 23);
		frmPlanificacinDeNuevo.getContentPane().add(btnAadir);
		
		lCamposNulos = new JLabel("Ningun campo puede ser nulo.");
		lCamposNulos.setForeground(Color.RED);
		lCamposNulos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lCamposNulos.setBounds(143, 365, 232, 14);
		frmPlanificacinDeNuevo.getContentPane().add(lCamposNulos);
		lCamposNulos.setVisible(false);
		
		sesiones = new LinkedList<Sesion>();
		ventanaSesiones = new PresentacionSesiones(sesiones, this);
		
	}

	public void actualizarSesiones() {
		String text = "";
		text += "Fecha y hora -- Duraci�n(h) -- Formador\n\n";
		DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		for (int i = 0; i < sesiones.size(); i++) {
			text += String.format("%s -- %s -- %s\n", formateador.format(sesiones.get(i).fecha), sesiones.get(i).duracion, sesiones.get(i).nombre);
		}
		
		taSesiones.setText(text);
	}
	
}
