package capa_Logica;

import java.util.LinkedList;
import capa_Datos.DriverDB;
import java.sql.ResultSet;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LogicaPagoFormador {
	
	private static LinkedList<LinkedList<String>> listaCursos;
	private static LinkedList<LinkedList<String>> listaFormadores;

	
	public static LinkedList<LinkedList<String>> getCursos() {
		ResultSet rs = DriverDB.consultaSQL("Select id_curso,titulo,espacio from cursos where estado = 'abierto';");
		LinkedList<LinkedList<String>> cursos = new LinkedList<LinkedList<String>>();
		try {
			while(rs.next()) {
				LinkedList<String> curso = new LinkedList<String>();
				curso.add(rs.getString(1));
				curso.add(rs.getString(2));
				curso.add(rs.getString(3));
				cursos.add(curso);
			}
		} catch (Exception e) {
			System.err.printf("Error al intentar obtener cursos\n");
		}
		listaCursos = cursos;
		return cursos;
	}
	
	public static LinkedList<LinkedList<String>> getFormadores() {
		ResultSet rs = DriverDB.consultaSQL("Select id_formador,nombre,dni from formadores;");
		LinkedList<LinkedList<String>> formadores = new LinkedList<LinkedList<String>>();
		try {
			while(rs.next()) {
				LinkedList<String> formador = new LinkedList<String>();
				formador.add(rs.getString(1));
				formador.add(rs.getString(2));
				formador.add(rs.getString(3));
				formadores.add(formador);
			}
		} catch (Exception e) {
			System.err.printf("Error al intentar obtener cursos\n");
		}
		listaFormadores = formadores;
		return formadores;
	}
	
	public static String getDNI(int index) {
		return listaFormadores.get(index).get(2);
	}
	
	public static boolean codigoRepetido(int codigo) {
		ResultSet rs = DriverDB.consultaSQL("select codigo from facturas;");
		try {
			while (rs.next()) {
				if (codigo == Integer.parseInt(rs.getString(1)))
					return true;
			}
		} catch (Exception e) {
			System.err.printf("Error al intentar obtener c�digos de factura\n");
		}
		return false;
	}
	
	public static void nuevaFactura(int indexCurso, int indexFormador, int codigo, String DNI, String concepto, int horas, double precio, Date fechaEmision, Date fechaPago) {
		DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		
		String values = String.format("('%s', '%s', '%d', '%s', '%s', '%d', '%.2f', '%s', '%s')", listaCursos.get(indexCurso).get(0), listaFormadores.get(indexFormador).get(0), 
				codigo, DNI, concepto, horas, precio, formateador.format(fechaEmision), formateador.format(fechaPago) );
		
		String consulta = String.format("insert into facturas (id_curso, id_formador, codigo, dni, concepto, horas, precio_hora, fecha_emision, fecha_pago) values %s;", values);
		
		try {
			DriverDB.insertSQL(consulta);
		} catch (Exception e) {
			System.err.printf("Error al insertar nueva factura\n");
		}
	}
	
	public static void limpiar() {
		DriverDB.limpiaConexiones();
	}
}
