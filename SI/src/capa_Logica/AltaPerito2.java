package capa_Logica;

import java.sql.ResultSet;
import java.sql.SQLException;

import capa_Datos.DriverDB;

public class AltaPerito2 {

	
	public static boolean introducirPerito(String nsocio){
		
		int contador=0;
		int turno=0;
		
		
	
		//Saber si el socio es colegiado
		boolean colegiado=false;
		String query= "SELECT agno_obtencion FROM socios WHERE id_socio=";
		
		ResultSet resultado=DriverDB.consultaSQL(query+nsocio);
		
		if (!resultado.equals("")){
			colegiado=true;
			System.out.println("El socio esta colegiado, puede formar parte de la lista de peritos");
		}else{
			System.out.println("El socio no esta colegiado y no puede inscribirse a la lista de peritos");
			return false;
		}
		
		//Comprobar si ya esta inscrito en la lista de peritos
		boolean perito=false;
		boolean para=false;
		String query2="SELECT id_socio FROM peritos";
		
		ResultSet listaPeritos=DriverDB.consultaSQL(query2);
		while (!listaPeritos.equals("") && !para){
			try {
				while (listaPeritos.next()){
					if (listaPeritos.getString("id_socio").equals(nsocio)){
						perito=true;
						System.out.println("El colegiado ya se encuentra en la lista de peritos");
						return false;
					}
					else{
						System.out.println("El usuario no se encuentra en la lista de peritos");
						perito=false;
						para=true;
					}
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("Llega!");
		
		//Si es colegiado y todavia no esta en lista de peritos
		//Obtener turno
		if (colegiado && !perito){
			
			String query3="SELECT id_socio FROM peritos";
			ResultSet resultado2=DriverDB.consultaSQL(query3);
			
			
			if (resultado2!=null){
			try {
				while (resultado2.next()){
					
					contador++;
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
			
			turno = contador + 1;
			
			
		}
		
		boolean exito = DriverDB.insertSQL("INSERT INTO peritos (id_socio, turno) values ('"+nsocio+"','"+turno+"')" );
		if (exito){
			System.out.println("Se ha insertado en la lista de peritos");
		}
		else{
			System.out.println("ERROR: No se ha podido insertar");
		}
		return true;
	}
	
	
	public String Nombre(String socio){
		//Para obtener el Nombre, recibiendo el numero de socio 
		String query="SELECT nombre FROM socios WHERE id_socio="+socio;
		ResultSet nombre=DriverDB.consultaSQL(query);
		

		System.out.printf("'%s'\n", socio);
		String NombreSocio="";
		
		
		if(!nombre.equals("")) {
			try {
				if (nombre.next()) {
					NombreSocio= nombre.getString("nombre"); 
				} 
				else{
					System.out.println("El usuario no esta dado de alta�");
				}
			}
			catch (SQLException e) { e.printStackTrace(); new RuntimeException(e);}

		}


		return NombreSocio;
	}

	public String Telefono(String socio){
		//Obtener el telefono de un socio, recibiendo el numero de socio
		ResultSet Telefono=DriverDB.consultaSQL("SELECT telefono FROM socios WHERE id_socio="+socio);

		String TelefonoSocio="";

		if(!Telefono.equals("")) {
			try {
				if (Telefono.next()) {

					TelefonoSocio= Telefono.getString("telefono"); 


				} 
				else{
					System.out.println("El usuario no esta dado de alta�");
				}
			}
			catch (SQLException e) { e.printStackTrace(); new RuntimeException(e);}

		}
		return TelefonoSocio;

	}
	
	public static boolean isNumber(String string) {
	    try {
	        Integer.parseInt(string);
	    } catch (Exception e) {
	        return false;
	    }
	    return true;
	}
	
	
}
