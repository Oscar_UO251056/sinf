package capa_Logica;

import java.awt.EventQueue;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.swing.DefaultListModel;

import capa_Datos.DriverDB;
import capa_Presentacion.PresentacionPlanificarCurso;

public class NuevaEdicionCurso {



	public static boolean obtenerCursos(DefaultListModel model) {


		ResultSet rs=  DriverDB.consultaSQL("SELECT titulo FROM cursos");
		if(rs==null) return false;

		try {
			while(rs.next()) {
				model.addElement(rs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}



		return true;
	}

	public static boolean callCrearCurso(String nombre) {

		String espacio="", precio_col,precio_pre,precio_est,precio_ext;
		String precios=" , , , ";

		ResultSet rs=  DriverDB.consultaSQL("SELECT espacio,precio FROM cursos WHERE titulo='"+nombre+"'" );
		if(rs==null) {
			return false;
		}


		try {
			rs.next();
			espacio= rs.getString(1);
			precios=rs.getString(2);
			rs.close();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		System.out.println(precios);
		String precios_vector[] = precios.split(",");

		precio_col=precios_vector[0];
		precio_pre=precios_vector[1];
		precio_est=precios_vector[2];
		precio_ext=precios_vector[3];
		System.out.println("Llamando a PresentacionPlanificacionCurso...");
		crearVentana(nombre,espacio,precio_col,precio_pre,precio_est,precio_ext);
		return true;
	}

	public static void crearVentana(String nombre,String espacio,String precio_col,String precio_pre,String precio_est,String precio_ext) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PresentacionPlanificarCurso window = new PresentacionPlanificarCurso(nombre,espacio,precio_col,precio_pre,precio_est,precio_ext);
					window.frmPlanificacinDeNuevo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});


	}

}
