package capa_Logica;

import java.util.LinkedList;
import capa_Datos.DriverDB;
import java.sql.ResultSet;
import capa_Presentacion.PresentacionCancelarCurso;
import java.io.FileWriter;

public class LogicaVisorCursos {

	private static LinkedList<LinkedList<String>> listaCursos;
	
	private static double inscripciones;
	private static double patrocinios;
	private static double formadores;
	private static double instalaciones;
	
	public static LinkedList<LinkedList<String>> getCursos() {
		LinkedList<LinkedList<String>> cursos = new LinkedList<LinkedList<String>>();
		
		try {
			ResultSet rs = DriverDB.consultaSQL("select id_curso, titulo, espacio, precio from cursos where estado = 'abierto';");
			
			while (rs.next()) {
				LinkedList<String> curso = new LinkedList<String>();
				
				curso.add(rs.getString(1));
				curso.add(rs.getString(2));
				curso.add(rs.getString(3));
				curso.add(rs.getString(4));
				
				cursos.add(curso);
			}
			
			rs.close();
			
		} catch (Exception e) {
			System.err.printf("Error al intentar obtener la lista de cursos\n");
		}
		listaCursos = cursos;
		return cursos;
	}
	
	public static int getInscritos(int index) {
		int cuenta = 0;
		try {
			String consulta = String.format("select * from inscripciones where id_curso = %s;", listaCursos.get(index).get(0));
			ResultSet rs = DriverDB.consultaSQL(consulta);
			while (rs.next()) {
				cuenta++;
			}
			rs.close();
		} catch (Exception e) {
			System.err.printf("Error al intentar obtener n�mero de inscritos\n");
		}
		return cuenta;
	}
	
	public static int getPlazas(int index) {
		int plazas = 0;
		try {
			String consulta = String.format("select plazas_maximas from cursos where id_curso = %s;", listaCursos.get(index).get(0));
			ResultSet rs = DriverDB.consultaSQL(consulta);
			
			rs.next();
			
			plazas = Integer.parseInt(rs.getString(1));
			
			rs.close();
		} catch (Exception e) {
			System.err.printf("Error al intentar obtener el n�mero de plazas m�ximo\n");
		}
		return plazas;
	}
	
	public static double getInscripciones(int index) {
		double inscripciones = 0;
		
		try {
			
			String precios = listaCursos.get(index).get(3);
			
			double precioColegiados = Double.parseDouble(precios.split(",")[0]);
			double precioPrecolegiados = Double.parseDouble(precios.split(",")[1]);
			double precioEstudiantes = Double.parseDouble(precios.split(",")[2]);
			double precioExternos = Double.parseDouble(precios.split(",")[3]);
			
			String consulta = String.format("select id_colegiado, id_externo from inscripciones where id_curso = %s and estado = 'inscrito';", listaCursos.get(index).get(0));
			
			ResultSet rs = DriverDB.consultaSQL(consulta);
			
			while (rs.next()) {
				String id_colegiado = rs.getString(1);
				String id_externo = rs.getString(2);

				
				ResultSet rs_interno;
				
				if (id_colegiado != null) {
					consulta = String.format("select agno_obtencion from colegiados where id_colegiado = %s;", id_colegiado);
					rs_interno = DriverDB.consultaSQL(consulta);
					
					rs_interno.next();
					
					if (rs_interno.getString(1)!=null) {
						inscripciones += precioColegiados;
					} else {
						inscripciones += precioPrecolegiados;
					}
					rs_interno.close();
				} else {
					consulta = String.format("select estudiante from externos where id_externo = %s;", id_externo);
					
					rs_interno = DriverDB.consultaSQL(consulta);
					
					rs_interno.next();
					
					if(rs_interno.getString(1).equals("SI")) {
						inscripciones += precioEstudiantes;
					} else {
						inscripciones += precioExternos;
					}
					rs_interno.close();
				}
			}
			rs.close();
		} catch (Exception e) {
			System.err.print("Error al intentar obtener los ingresos por inscripciones\n");
		}
		LogicaVisorCursos.inscripciones = inscripciones;
		return inscripciones;
	}
	
	public static double getPatrocinios(int index) {
		double patrocinios = 0;
		
		try {
			String consulta = String.format("select cuantia from patrocinios where id_curso = %s;", listaCursos.get(index).get(0));
			
			ResultSet rs = DriverDB.consultaSQL(consulta);
			
			while (rs.next()) {
				patrocinios += Double.parseDouble(rs.getString(1));
			}
			rs.close();
		} catch (Exception e) {
			System.err.printf("Error al intentar obtener los ingresos por patrocinios\n");
		}
		LogicaVisorCursos.patrocinios = patrocinios;
		return patrocinios;
	}
	
	public static double getFormadores(int index) {
		double formadores = 0;
		try {
			String consulta = String.format("select horas,precio_hora from facturas where id_curso = %s;", listaCursos.get(index).get(0));
			
			ResultSet rs = DriverDB.consultaSQL(consulta);
			
			while (rs.next()) {
				int horas = Integer.parseInt(rs.getString(1));
				double precio = Double.parseDouble(rs.getString(2));
				
				formadores += horas * precio;
			}
			rs.close();
		} catch (Exception e) {
			System.err.print("Error al intentar obtener los gastos por formadores\n");
		}
		LogicaVisorCursos.formadores = formadores;
		return formadores;
	}
	
	public static double getInstalaciones(int index) {
		double instalaciones = 0;
		try {
			String consulta = String.format("select precio_instalacion from cursos where id_curso = %s;", listaCursos.get(index).get(0));
			
			ResultSet rs = DriverDB.consultaSQL(consulta);
			
			rs.next();
			
			instalaciones = Double.parseDouble(rs.getString(1));
			
			rs.close();
		} catch (Exception e) {
			System.err.print("Error al intentar obtener los gastos por coste de instalaciones\n");
		}
		LogicaVisorCursos.instalaciones = instalaciones;
		return instalaciones;
		
	}
	public static double getBalance(int index) {
		return inscripciones + patrocinios - instalaciones - formadores;
	}
	
	public static void limpiar() {
		DriverDB.limpiaConexiones();
	}
	
	public static void rellenar(int index) {
		PresentacionCancelarCurso ventana = PresentacionCancelarCurso.thisFrame;
		
		ventana.lbTitulo.setText(String.format("Titulo: %s", listaCursos.get(index).get(1)));
		ventana.lbEspacio.setText(String.format("Espacio: %s", listaCursos.get(index).get(2)));
		ventana.lbInscritos.setText(String.format("Inscritos: %d/%d", getInscritos(index), getPlazas(index)));
		
		ventana.lbTotal.setText(String.format("El importe total a devolver asciende a: %.2f �", getInscripciones(index)));
	}
	
	public static void cancelarCurso(int index) {
		String consulta = String.format("update cursos set estado = 'cancelado' where id_curso = %s;", listaCursos.get(index).get(0));
		
		DriverDB.insertSQL(consulta);
		
		consulta = String.format("update inscripciones set estado = 'cancelado' where id_curso = %s", listaCursos.get(index).get(0));
		
		DriverDB.insertSQL(consulta);
		
		
	}
	
	public static void genDevs(int index) {
		
		try {
			
			FileWriter writer = new FileWriter("devoluciones.csv", false);
			
			String precios = listaCursos.get(index).get(3);
			
			double precioColegiados = Double.parseDouble(precios.split(",")[0]);
			double precioPrecolegiados = Double.parseDouble(precios.split(",")[1]);
			double precioEstudiantes = Double.parseDouble(precios.split(",")[2]);
			double precioExternos = Double.parseDouble(precios.split(",")[3]);
			
			String consulta = String.format("select id_colegiado, id_externo from inscripciones where id_curso = %s and estado = 'inscrito';", listaCursos.get(index).get(0));
			
			ResultSet rs = DriverDB.consultaSQL(consulta);
			
			while (rs.next()) {
				String id_colegiado = rs.getString(1);
				String id_externo = rs.getString(2);

				
				ResultSet rs_interno;
				
				if (id_colegiado != null) {
					consulta = String.format("select agno_obtencion, dni from colegiados where id_colegiado = %s;", id_colegiado);
					rs_interno = DriverDB.consultaSQL(consulta);
					
					rs_interno.next();
					
					if (rs_interno.getString(1)!=null) {
						writer.write(String.format("%s, -%.2f\n", rs_interno.getString(2), precioColegiados));
					} else {
						writer.write(String.format("%s, -%.2f\n", rs_interno.getString(2), precioPrecolegiados));
					}
					rs_interno.close();
				} else {
					consulta = String.format("select estudiante, dni from externos where id_externo = %s;", id_externo);
					
					rs_interno = DriverDB.consultaSQL(consulta);
					
					rs_interno.next();
					
					if(rs_interno.getString(1).equals("SI")) {
						writer.write(String.format("%s, -%.2f\n", rs_interno.getString(2), precioEstudiantes));
					} else {
						writer.write(String.format("%s, -%.2f\n", rs_interno.getString(2), precioExternos));
					}
					rs_interno.close();
				}
			}
			writer.close();
			rs.close();
		} catch (Exception e) {
			System.err.printf("Error al intentar crear el archivo de devoluciones: %s\n", e);
		}

	}
}
