package capa_Logica;
import capa_Datos.DriverDB;
import capa_Presentacion.Sesion;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LogicaPlanificarCurso {

	public static void nuevoCurso(String titulo, String espacio, double precioColegiados, double precioPrecolegiados, double precioEstudiantes, 
			double precioExternos) {
		String consulta = String.format("insert into cursos (titulo, espacio, precio) values ('%s', '%s', '%.2f,%.2f,%.2f,%.2f');", titulo, espacio,
				precioColegiados, precioPrecolegiados, precioEstudiantes, precioExternos);
		
		DriverDB.insertSQL(consulta);
	}
	
	public static void nuevaSesion(String titulo, Sesion sesion) {
		String id_curso = "";
		ResultSet rs = DriverDB.consultaSQL(String.format("select id_curso from cursos where titulo='%s';", titulo));
		try {
			rs.next();
			id_curso = rs.getString(1);
		} catch(Exception e) {
			System.err.printf("ERROR: obteniendo id de curso.\n");
		}
		
		DateFormat formateador = new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stringFecha = formateador.format(sesion.fecha);
		
		String consulta = String.format("insert into sesiones (id_curso, duracion, fecha_hora_imparticion, id_formador) "+
				"values ('%s', '%d', '%s', '%s');", id_curso, sesion.duracion, stringFecha, sesion.id_formador);
		
		DriverDB.insertSQL(consulta);
	}
	
}
