package capa_Logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import capa_Datos.DriverDB;

public class AsignarPericial {

	//Asignar el perito del primer turno a la pericial
	public static boolean AsignarPericial(){

		String query= "SELECT id_colegiado FROM peritos WHERE turno=1";
		ResultSet resultado=DriverDB.consultaSQL(query);

		return true;
	}

	//Mover el resto .
	public boolean RotarTurnos(){

		String numPeritos="";
		//Contamos el numero de colegiados para obtener turno
		ResultSet Nperitos = DriverDB.consultaSQL("SELECT COUNT(id_colegiado) FROM peritos");
		try {
			Nperitos.next();
			numPeritos = Nperitos.getString("COUNT(id_colegiado)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int turnoUltimo=Integer.parseInt(numPeritos);
		System.out.println("El numero de peritos es:"+turnoUltimo);

		//FUNCIONA

		//Cambiar el turno a cada perito
		ResultSet listPeritos=DriverDB.consultaSQL("SELECT id_colegiado, turno FROM peritos");


		boolean para=false;

		if (listPeritos!=null){
			try {
				while(listPeritos.next()){
					System.out.println("Entra en el while");

					String colegiado=listPeritos.getString("id_colegiado");
					System.out.println("Saca el colegiado que es: "+colegiado);

					int turnoPerito=listPeritos.getInt("turno");



					System.out.println("su turno actual es: "+turnoPerito);


					if (turnoPerito==1){
						System.out.println("Entra");
						DriverDB.insertSQL("UPDATE peritos SET turno="+turnoUltimo+" WHERE id_colegiado="+colegiado);
						System.out.println("Se ha cambiado el turno del colegiado: "+colegiado+" por el turno: "+turnoUltimo);
					}
					else{
						turnoPerito=turnoPerito-1;
						//DriverDB.insertSQL("INSERT turno FROM peritos values ('"+turnoPerito+"') WHERE id_colegiado="+colegiado);
						DriverDB.insertSQL("UPDATE peritos SET turno="+turnoPerito+" WHERE id_colegiado="+colegiado);
						System.out.println("Se ha cambio el turno del colegiado: "+colegiado+" por el turno: "+turnoPerito);
					}


				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;
	}


	private String parseInteger(String turnoPerito) {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<String> peritosActual(){
		ArrayList<String> peritos=new ArrayList<String>();

		//hacer INNER JOIN para sacar el nombre. Mucho mejor
		//ResultSet resultados = DriverDB.consultaSQL("SELECT id_colegiado, turno FROM peritos ORDER BY turno");
		ResultSet resultados = DriverDB.consultaSQL("SELECT colegiados.id_colegiado, colegiados.nombre FROM db.colegiados INNER JOIN db.peritos ON colegiados.id_colegiado=peritos.id_colegiado ORDER BY peritos.turno");

		if (resultados!=null){
			try {
				while(resultados.next()){
					//precolegiados=precolegiados+ "\n" + resultados.getString("nombre");

					peritos.add(resultados.getString("nombre"));

				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return peritos;

	}

	public boolean GuardaPericiales(int id_pericial, String cliente, String descripcion, String fecha_solicitud, int id_perito, String fecha_asignacion ){
		boolean exito=false;

		exito =	DriverDB.insertSQL(  "INSERT INTO periciales(id_pericial ,cliente, descripcion, fecha_solicitud, id_perito, fecha_asignacion) values('" +id_pericial+ "','" +cliente+ "', '" +descripcion+
				"','" +fecha_solicitud+ "','" +id_perito+ "','" +fecha_asignacion+"' )"  );

		return true;
	}


	public int SiguientePericial(){

		String numPericiales="";
		int nper; 
		int pericial=0;

		ResultSet Npericiales = DriverDB.consultaSQL("SELECT COUNT(id_pericial) FROM periciales");
		//Funciona. Saca el numero de las periciales que hay ahora mismo
		try {
			Npericiales.next();
			numPericiales = Npericiales.getString("COUNT(id_pericial)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nper=Integer.parseInt(numPericiales);

		pericial=nper+1;

		return pericial;
	}

	public int peritoAsignado(){
		ResultSet listPeritos=DriverDB.consultaSQL("SELECT id_colegiado FROM peritos WHERE turno=1");
		String peritoj="";
		int perito=0;

		if (listPeritos!=null){
			try {
				listPeritos.next();
				peritoj=listPeritos.getString("id_colegiado");
				System.out.println("El perito asignado es: "+peritoj);

			}
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			perito=Integer.parseInt(peritoj);
		}

		return perito;

	}

}


