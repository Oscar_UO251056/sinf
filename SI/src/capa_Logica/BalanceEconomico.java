package capa_Logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import capa_Datos.DriverDB;

public class BalanceEconomico {

	private static int cuota_pre=30;
	private static int cuota_col=60;

	public static int cuotas(String year) {


		int dinero=0;

		ResultSet rs= DriverDB.consultaSQL("SELECT id_colegiado,agno_obtencion FROM COLEGIADOS WHERE YEAR(fecha_solicitud)<="+year+" AND (fecha_baja IS NULL OR YEAR(fecha_baja)>="+year+")");
		if(rs==null) return -1;

		try {
			while(rs.next()) {
				System.out.print("\n"+rs.getString(1));
				if(rs.getString(2)==null) {
					dinero= dinero + cuota_pre;
					System.out.print("- Precolegiado");
				}
				else {
					dinero= dinero + cuota_col;
					System.out.print("- Colegiado");
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("\n"+dinero+"");
		return dinero;
	}


	public static String balanceTotal(String year) {


		int dinero_ins=0;
		int dinero_ses=0;
		int dinero_inscrip=0;
		int dinero_cuo=0;
		int dinero_pat=0;
		

		String message="";
		DriverDB.limpiaConexiones();
		System.out.println("limpia\n");
		dinero_cuo= cuotas(year);

		ResultSet rs= DriverDB.consultaSQL("SELECT precio_instalacion,id_curso,salario_hora,precio FROM CURSOS WHERE YEAR(fecha_fin_inscripcion)="+year);

		if(rs==null) 
			return "\nError al consultar la base de datos\n";

		try {
			while(rs.next()) {
				System.out.print("\nInstalaciones "+rs.getString(1)+" ");

				dinero_ins= dinero_ins + rs.getInt(1);
				dinero_ses=dinero_ses + sueldoFormadores(rs.getString(2),rs.getInt(3));
				System.out.print("\nSalario "+rs.getInt(3)+"\n");
				dinero_pat=dinero_pat + patrocinios(rs.getString(2));
				dinero_inscrip= dinero_inscrip + inscripciones(rs.getString(2),rs.getString(4).split(","));


			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("\n"+dinero_ins+"");




		message= "\nIngresos de cuotas de precolegiados/colegiados en "+year+": "+dinero_cuo+"";
		message=message + "\n\nGastos de las instalaciones de los cursos en "+year+": "+dinero_ins+"";
		message=message +"\nGastos de pagos a los formadores en "+year+": "+dinero_ses+"";
		message= message+ "\n\nIngresos de inscripciones a cursos en  "+year+": "+dinero_inscrip+"";
		message= message+ "\nIngresos de patrocinios en "+year+": "+dinero_pat+"";

		int ingresos_totales= dinero_cuo+dinero_inscrip+dinero_pat;
		int gastos_totales= dinero_ins+dinero_ses;
		int beneficio= ingresos_totales-gastos_totales;

		message= message+ "\n\nIngresos totales en el año "+year+": "+ingresos_totales+"";
		message= message + "\nGastos totales en el año "+year+": "+gastos_totales+"";
		message= message + "\nBalance total de "+year+": "+beneficio+"";

		return message;
	}

	public static int patrocinios(String id_curso) {
		
		int dinero_pat=0;
		ResultSet rs2= DriverDB.consultaSQL("SELECT cuantia FROM patrocinios WHERE id_curso="+id_curso);
		if(rs2==null) 
			return 0;
		else {

			try {
				while(rs2.next()) {
					System.out.print("\nPatrocinio de curso "+id_curso+": "+rs2.getString(1)+" ");

					dinero_pat= dinero_pat + rs2.getInt(1);


				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return dinero_pat;
	}
	
	
	public static int inscripciones(String id_curso,String precios[]) {
		
		float dinero_ins=0;
		ResultSet rs2= DriverDB.consultaSQL("SELECT id_colegiado,id_externo FROM inscripciones WHERE id_curso="+id_curso+" AND estado='inscrito'");
		if(rs2==null) 
			return 0;
		else {

			try {
				while(rs2.next()) {
					
					if(rs2.getString(1)==null) {//Es externo
						
						dinero_ins= dinero_ins + Float.parseFloat(precios[3].trim());
						System.out.print("\nInscrito Externo de curso "+id_curso);
						
					}
					else
						if (rs2.getString(2)==null) {//Es colegiado
							
							dinero_ins= dinero_ins + Float.parseFloat(precios[1].trim());
							System.out.print("\nInscrito Colegiado de curso"+id_curso);
							
						}

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("\nDinero de inscritos en curso "+id_curso+": "+(int)dinero_ins);
		return (int)dinero_ins ;
	}

	public static int sueldoFormadores(String idcurso,int salario) {


		int dinero=0;

		ResultSet rs= DriverDB.consultaSQL("select COUNT(DISTINCT id_sesion) from sesiones WHERE id_curso="+idcurso);
		if(rs==null) return -1;

		try {
			while(rs.next()) {
				System.out.print("\nSesiones: "+rs.getString(1));

				dinero= dinero + rs.getInt(1)*salario;




			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("\n"+dinero+"");
		return dinero;
	}







	public static boolean isValidYear(String string) {
		try {

			if(string.equals(""))
				return false;

			int agno= Integer.parseInt(string);

			DateFormat df = new SimpleDateFormat("yyyy");
			String agno_s = df.format(new Date()).toString(); // Año actual

			int agno_actual= Integer.parseInt(agno_s);

			if(agno>agno_actual || agno<0)
				return false;

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean isSameYear(String year) {

		DateFormat df = new SimpleDateFormat("yyyy");
		String agno_s = df.format(new Date()).toString(); // Año actual

		return year.trim().equals(agno_s.trim());




	}


}
