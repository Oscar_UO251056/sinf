package capa_Logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import capa_Datos.DriverDB;
	
	public class InscribirseACurso {
		
			//Esta funci�n rellena el combo box del curso con los cursos abiertos y los que tienen abierto el plazo para inscribirse
			public String rellenarCombo(){
				String cursos="";
				int curso=0;
				//Fecha actual
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
				
				ResultSet Cursosabiertos=DriverDB.consultaSQL("select id_curso,titulo from db.cursos where estado='abierto'");
				try {
					while(Cursosabiertos.next()){
						curso=Cursosabiertos.getInt("id_curso");
						String query="select fecha_inicio_inscripcion from cursos where id_curso="+curso;
						
						ResultSet fechaInicio=DriverDB.consultaSQL(query);
						String query2="select fecha_fin_inscripcion from db.cursos where id_curso="+curso;
						
						ResultSet fechaFinal=DriverDB.consultaSQL(query2);
						
						fechaInicio.next();
						fechaFinal.next();
						System.out.println("fallo");
						String inicio=fechaInicio.getString("fecha_inicio_inscripcion");
						String fin=fechaFinal.getString("fecha_fin_inscripcion");
						
						try {
							Date fecha_actual=df.parse(fecha_inscripcion);
							Date fecha_inicial=df.parse(inicio);
							Date fecha_final=df.parse(fin);
							if(fecha_actual.compareTo(fecha_inicial)>=0 && fecha_actual.compareTo(fecha_final)<=0) {
								cursos=cursos+ Cursosabiertos.getString("id_curso")+" - "+ Cursosabiertos.getString("titulo")+"\n";
							}
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				return cursos;
			}
			
			public String SacarIdExterno(String DNI){
				String n="";
				String query="select id_externo from externos where dni='"+DNI+"'";
				System.out.println("ejecutando query "+query);
				ResultSet id = DriverDB.consultaSQL(query);
				try {
					id.next();
					n=id.getString("id_externo");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return n;
			}
			
			public boolean ExisteInscripcionExterno(String externo, String curso){
				int n=0;
				String query="select COUNT(id_externo) from inscripciones where id_curso="+curso+" and id_externo="+externo;
				ResultSet YaInscrito = DriverDB.consultaSQL(query);
				try {
					YaInscrito.next();
					n=YaInscrito.getInt("COUNT(id_externo)");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(n==0){
					return false;
				}return true;
				
			}
			public boolean InscribirExterno(String externo, String curso){
				//Fecha actual
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
				try {
					Date fecha_actual=df.parse(fecha_inscripcion);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				boolean ok =DriverDB.insertSQL(  "INSERT INTO inscripciones (id_externo,id_curso,fecha,estado) values('" +externo+ "','" +curso+ "',' " 
						+fecha_inscripcion+"', 'pre-inscrito' )"  ); 
				return ok;
			}
			
			public boolean ExisteElSocio (String socio){
				int ExisteSocio=0;
				ResultSet socioExiste = DriverDB.consultaSQL("SELECT COUNT(id_colegiado) FROM colegiados WHERE id_colegiado="+socio);
				try {
					socioExiste.next();
					ExisteSocio=socioExiste.getInt("COUNT(id_colegiado)");
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if(ExisteSocio==0){
					return false;
				}
				return true;
			}

			public boolean ExisteLaInscripcion(String socio, String curso){
				int ExisteInscripcion=0;
				ResultSet inscripcionExiste=DriverDB.consultaSQL("select count(id_colegiado) from inscripciones where id_curso="+curso+" and id_colegiado="+socio);
				try {
					inscripcionExiste.next();
					ExisteInscripcion=inscripcionExiste.getInt("count(id_colegiado)");
					if(ExisteInscripcion==0){
						System.out.println("No est� inscrito en el curso");
						return false;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			}
			
			public boolean inscripcionCurso(String socio, String curso) throws ParseException{
				
				//Comprobar si hay plazas libres
				int plazasO=0;
				int plazasM=0;
				ResultSet plazasOcupadas = DriverDB.consultaSQL("SELECT COUNT(id_colegiado) FROM inscripciones WHERE id_curso="+curso);
				ResultSet plazasMaximas = DriverDB.consultaSQL("SELECT plazas_maximas FROM cursos WHERE id_curso="+curso);
				try {
					plazasOcupadas.next();
					plazasO=plazasOcupadas.getInt("COUNT(id_colegiado)");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					plazasMaximas.next();
					plazasM=plazasMaximas.getInt("plazas_maximas");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(plazasO<plazasM){
					System.out.println("Hay plazas libres en el curso");
					
					
					//Fecha actual
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
					Date fecha_actual=df.parse(fecha_inscripcion);
					
					
					DriverDB.insertSQL(  "INSERT INTO inscripciones (id_colegiado,id_curso,fecha,estado) values('" +socio+ "','" +curso+ "',' " 
							+fecha_inscripcion+"', 'pre-inscrito' )"  ); 
					 
				}else{
					return false;
				}
				return true;
			
				
			}
			
			public void insertarExterno (String DNI, String nombre, String dir, String pob, String tel, String est, String emp){
				DriverDB.insertSQL(  "INSERT INTO externos(dni,nombre,direccion,poblacion,telefono,estudiante,empresa ) values('" +DNI+ "','" +nombre+ "','" +dir+
						"','" +pob+ "','" +tel+ "','" +est+ "','" +emp+"' )"  );
					
			}
			
			public String SacarNombre(String socio){
				String NombreSocio="";
				ResultSet Nombre = DriverDB.consultaSQL("SELECT nombre FROM colegiados WHERE id_colegiado="+socio);
				try{
					Nombre.next();
					NombreSocio = Nombre.getString("nombre");
				}catch(SQLException e){
					e.printStackTrace();
					new RuntimeException(e);
				}
				return NombreSocio;
				
			}
			public String SacarNumeroCuenta(String socio){
				String NumeroCuentaSocio="";
				ResultSet Numero = DriverDB.consultaSQL("SELECT dni FROM colegiados WHERE id_colegiado="+socio);
				try{
					Numero.next();
					NumeroCuentaSocio = Numero.getString("dni");
				}catch(SQLException e){
					e.printStackTrace();
					new RuntimeException(e);
				}
				return NumeroCuentaSocio;
				
			}
			public static double SacarCantidadAAbonar(String curso, String tipo){
				double euros=0.0;
				String eurosTipo="";
				ResultSet precio = DriverDB.consultaSQL("SELECT precio FROM cursos WHERE id_curso="+curso);
				try{
					precio.next();
					eurosTipo = precio.getString("precio");
				}catch(SQLException e){
					e.printStackTrace();
					new RuntimeException(e);
				}
				if(tipo.equals("precolegiado")){
					String a=eurosTipo.split(",")[1];
					euros= Double.parseDouble(a);
					return euros;
				}
				String a=eurosTipo.split(",")[0];
				euros= Double.parseDouble(a);
				return euros;
			}
			
			public static double SacarCantidadAAbonarExterno(String curso, String tipo){
				double euros=0.0;
				String eurosTipo="";
				ResultSet precio = DriverDB.consultaSQL("SELECT precio FROM cursos WHERE id_curso="+curso);
				try{
					precio.next();
					eurosTipo = precio.getString("precio");
				}catch(SQLException e){
					e.printStackTrace();
					new RuntimeException(e);
				}
				if(tipo.equals("estudiante")){
					String a=eurosTipo.split(",")[2];
					euros= Double.parseDouble(a);
					return euros;
				}
				String a=eurosTipo.split(",")[3];
				euros= Double.parseDouble(a);
				return euros;
			}
			
			public static String EstudianteOtro(String socio){
				String tipo="";
				ResultSet tipoSocio = DriverDB.consultaSQL("SELECT estudiante FROM externos WHERE id_externo="+socio);
				try {
					tipoSocio.next();
					tipo=tipoSocio.getString("estudiante");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(tipo.equals("SI")){
					return "estudiante";
				}
				return "no estudiante";
			}
			
			public static String ColegiadoOPrecolegiado(String socio){
				String tipo="";
				ResultSet tipoSocio = DriverDB.consultaSQL("SELECT agno_obtencion FROM colegiados WHERE id_colegiado="+socio);
				try {
					tipoSocio.next();
					tipo=tipoSocio.getString("agno_obtencion");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(tipo.equals("")){
					return "precolegiado";
				}
				return "colegiado";
			}
			
			public static boolean VerificarExterno(String DNI){
				int i=0;
				String query="select COUNT(dni) from externos where dni='"+DNI+"'";
				ResultSet tenemosDatos = DriverDB.consultaSQL(query);
				try {
					tenemosDatos.next();
					i=tenemosDatos.getInt("COUNT(dni)");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(i==0){
					return false;
				}
				return true;
				
			}
			
			public static String extraerNombreExterno(String DNI){
				String nombre="";
				String query="SELECT nombre FROM externos WHERE dni='"+DNI+"'";
				ResultSet nombreExt = DriverDB.consultaSQL(query);
				try {
					nombreExt.next();
					nombre=nombreExt.getString("nombre");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return nombre;
				
			}
			
			public static String extraerDireccionExterno(String DNI){
				String dir="";
				String query="SELECT direccion FROM externos WHERE dni='"+DNI+"'";
				ResultSet direccion = DriverDB.consultaSQL(query);
				try {
					direccion.next();
					dir=direccion.getString("direccion");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return dir;
			}
			
			public static String extraerPoblacionExterno(String DNI){
				String pob="";
				String query="SELECT poblacion FROM externos WHERE dni='"+DNI+"'";
				ResultSet poblacion = DriverDB.consultaSQL(query);
				try {
					poblacion.next();
					pob=poblacion.getString("poblacion");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return pob;
			}
			
			public static String extraerTelefonoExterno(String DNI){
				String tel="";
				String query="SELECT telefono FROM externos WHERE dni='"+DNI+"'";
				ResultSet telefono = DriverDB.consultaSQL(query);
				try {
					telefono.next();
					tel=telefono.getString("telefono");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return tel;
			}
			
			public static String extraerEmpresaExterno(String DNI){
				String tel="";
				String query="SELECT empresa FROM externos WHERE dni='"+DNI+"'";
				ResultSet telefono = DriverDB.consultaSQL(query);
				try {
					telefono.next();
					tel=telefono.getString("empresa");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return tel;
			}
			
			public static boolean EsEstudiante(String DNI){
				String tel="";
				String query="SELECT estudiante FROM externos WHERE dni='"+DNI+"'";
				ResultSet telefono = DriverDB.consultaSQL(query);
				try {
					telefono.next();
					tel=telefono.getString("estudiante");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(tel.equals("SI")){
					return true;
				}
				return false;
			}
			
	}