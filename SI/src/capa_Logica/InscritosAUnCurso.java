package capa_Logica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import capa_Datos.DriverDB;

public class InscritosAUnCurso {
	public InscritosAUnCurso(){}
	
	public void CancelarInscripcionesFueraPlazo(String curso){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
		String query="select id_inscripcion from db.inscripciones where estado='pre-inscrito' and id_curso="+curso;
		ResultSet ins=DriverDB.consultaSQL(query);
		try {
			while (ins.next()){
				//Si la fecha en la que se ha pre�nscrito es superior a dos dias actuales la cancelo
				String id=ins.getString("id_inscripcion");
				String query2="select fecha from db.inscripciones where id_inscripcion="+id;
				ResultSet f=DriverDB.consultaSQL(query2);
				f.next();
				String fecha=f.getString("fecha");
				if (!this.DentroPlazo(fecha)){
					DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Fuera de plazo' WHERE id_inscripcion="+id);
					DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_inscripcion="+id);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void ActualizarPagos(String curso){
		String texto="";
		String total="";
		int plazasM=0;
		//Saco las plazas m�ximas del curso
		ResultSet plazas=DriverDB.consultaSQL("select plazas_maximas from cursos where id_curso="+curso);
		try {
			plazas.next();
			plazasM=plazas.getInt("plazas_maximas");
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		//Saco precio del cursos
		ResultSet precio=DriverDB.consultaSQL("select precio from cursos where id_curso="+curso);
		try {
			precio.next();
			total=precio.getString("precio");
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String c=total.split(",")[0];
		double colegiado=Double.parseDouble(c);
		String p=total.split(",")[1];
		double precolegiado=Double.parseDouble(p);
		String es=total.split(",")[2];
		double estudiante=Double.parseDouble(es);
		String ex=total.split(",")[3];
		double externo=Double.parseDouble(ex);
		//Leemos el fichero del banco y sacamos los datos (quedan guardados en texto)
		try{
			BufferedReader bf= new BufferedReader (new FileReader("PagosCursos\\archivo.txt"));
			String temp="";
			String bfRead;
			while((bfRead = bf.readLine())!=null){
				temp=temp+bfRead+"\n";
			}

			texto=temp;
		}catch(Exception e){
			System.err.println("No se encontro archivo");
		}
		//Escribo las incidencias en el fichero incidencias, inscribo los que paguen o paguen de m�s, cancelo los que 
		//paguen de menos y escribo las observaciones necesarias en las incripciones
		FileWriter incidencias = null;
        PrintWriter pw = null;
        try
        {
        	incidencias = new FileWriter("PagosCursos\\incidencias.txt");
           	pw = new PrintWriter(incidencias);
           	int n=texto.split("\n").length;
			String Escolegiado="";
			String Esexterno="";
			for (int cont=0;cont<n;cont++){ //Por cada fila
				String dni=texto.split("\n")[cont].split(";")[0];	
				String precio1=texto.split("\n")[cont].split(";")[1];
				String fecha=texto.split("\n")[cont].split(";")[2];
				double precio2=Double.parseDouble(precio1);
				//Compruebo si el dni es de un colegiado/precolegiado, de un estudiante/externo o no existe
				String query="select dni from db.colegiados where dni='"+dni+"'";
				ResultSet EsColegiado=DriverDB.consultaSQL(query);
				String query2="select dni from db.externos where dni='"+dni+"'";
				ResultSet EsExterno=DriverDB.consultaSQL(query2);
				try {
					if (EsColegiado.next()){ ///ES COLEGIADO O PRECOLEGIADO
						System.out.println("Es colegiado");
						//Saco el id y compruebo si estaba pre-inscrito en dicho curso
						String query5="select id_colegiado from db.colegiados where dni='"+dni+"'";
						ResultSet id=DriverDB.consultaSQL(query5);
						id.next();
						String idc=id.getString("id_colegiado");
						String query6="select id_colegiado from db.inscripciones where id_colegiado='"+idc+" and id_curso="+curso+"'";
						ResultSet esta=DriverDB.consultaSQL(query6);
						if (!esta.next()){ // No estaba pre-inscrito
							pw.println(dni+";"+fecha+";"+precio2+";COLEGIADO/PRECOLEGIADO NO PRE-INSCRITO EN EL CURSO");
						}else{
							//ES COLEGIADO/PRECOLEGIADO Y ESTA PRE-INSCRITO
							if (this.DentroPlazo(fecha)){
								String query3="select agno_obtencion from db.colegiados where dni='"+dni+"'";
								ResultSet Es_colegiado=DriverDB.consultaSQL(query3);
								String query7="select count(id_inscripcion) from db.incripciones where estado='inscrito' or estado='pre-inscrito'";
								if (Es_colegiado.next()){ // ES COLEGIADO
									if(precio2==colegiado){
										//TODO Correcto
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_colegiado="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='OK' WHERE id_colegiado="+idc);
									}else if(precio2>colegiado){
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_colegiado="+idc);
										double resto=precio2-colegiado;
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+resto+" �. Pagado de mas' WHERE id_colegiado="+idc);
									}else{
										DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_colegiado="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+precio2+"�. Pagado de menos' WHERE id_colegiado="+idc);
									}
								}else{ //ES PRECOLEGIADO
									if(precio2==precolegiado){
										//Todo correcto
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_colegiado="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='OK' WHERE id_colegiado="+idc);
									}else if(precio2>precolegiado){
										//Correcto pero hay que devolverle dinero
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_colegiado="+idc);
										double resto=precio2-precolegiado;
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+resto+" �. Pagado de mas.' WHERE id_colegiado="+idc);
									}else{
										DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_colegiado="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+precio2+"�. Pagado de menos.' WHERE id_colegiado="+idc);
									}
								}
							}else{
								DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_colegiado="+idc);
								DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+precio2+"�. Pagado fuera de plazo.' WHERE id_colegiado="+idc);
							}
							
						}
					}else if(EsExterno.next()){ //ES ESTUDIANTE O EXTERNO
						System.out.println("Es externo");
						//Saco el id y compruebo si estaba pre-inscrito en dicho curso
						String query5="select id_externo from db.externos where dni='"+dni+"'";
						ResultSet id=DriverDB.consultaSQL(query5);
						id.next();
						String idc=id.getString("id_externo");
						String query6="select id_externo from db.inscripciones where id_externo='"+idc+" and id_curso="+curso+"'";
						ResultSet esta=DriverDB.consultaSQL(query6);
						if (!esta.next()){ // No estaba pre-inscrito
							pw.println(dni+";"+fecha+";"+precio2+";COLEGIADO/PRECOLEGIADO NO PRE-INSCRITO EN EL CURSO");
						}else{
							if (this.DentroPlazo(fecha)){
								//ES ESTUDIANTE/EXTERNO Y ESTA PRE-INSCRITO
								String query3="select estudiante from db.externos where dni='"+dni+"'";
								ResultSet Es_estudiante=DriverDB.consultaSQL(query3);
								if (Es_estudiante.next() && Es_estudiante.getString("estudiante").equals("SI")){ // ES ESTUDIANTE
									if(precio2==estudiante){
										//TODO Correcto
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_externo="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='OK' WHERE id_externo="+idc);
									}else if(precio2>estudiante){
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_externo="+idc);
										double resto=precio2-estudiante;
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+resto+" �. Pagado de mas' WHERE id_externo="+idc);
									}else{
										DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_externo="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+precio2+"�. Pagado de menos' WHERE id_externo="+idc);
									}
								}else{ //ES EXTERNO	
									if(precio2==externo){
										//Todo correcto
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_externo="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='OK' WHERE id_externo="+idc);
									}else if(precio2>externo){
										//Correcto pero hay que devolverle dinero
										DriverDB.insertSQL("UPDATE inscripciones SET estado='inscrito' WHERE id_externo="+idc);
										double resto=precio2-externo;
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+resto+" �. Pagado de mas' WHERE id_externo="+idc);
									}else{
										DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_externo="+idc);
										DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+precio2+"�. Pagado de menos' WHERE id_externo="+idc);
									}
								}
							}else{
								DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_externo="+idc);
								DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+precio2+"�. Pagado fuera de plazo' WHERE id_externo="+idc);
							}
					}
				}else{ //EL DNI NO EXISTE
					System.out.println("DNI NO EXISTE");
					pw.println(dni+";"+fecha+";"+precio2+";DNI NO EXISTE");
				}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
 
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
        	try {
        		if (null != incidencias)
        			incidencias.close();
        	} catch (Exception e2) {
        		e2.printStackTrace();
        	}
        }
	}


	//M�todo para sacar todos los cursos
	public String DameCursos(){
		String cursos="";
		ResultSet Cursosabiertos=DriverDB.consultaSQL("select id_curso,titulo from db.cursos");
		try {
			while(Cursosabiertos.next()){
				cursos=cursos+ Cursosabiertos.getString("id_curso")+" - "+ Cursosabiertos.getString("titulo")+"\n";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(cursos.equals("")){
			cursos="En este momento no hay cursos";
			return cursos;
		}
		return cursos;
	}

	public String InscritosColegiadosACurso(String curso){
		String nombre="";
		String tipo="";
		String inscritos="";
		String precio="";
		double n=0.0;
		ResultSet inscritos2=DriverDB.consultaSQL("select colegiados.nombre, inscripciones.estado, cursos.precio from db.inscripciones inner join db.colegiados on inscripciones.id_colegiado=colegiados.id_colegiado inner join db.cursos on inscripciones.id_curso=cursos.id_curso where inscripciones.id_curso="+curso+" order by colegiados.nombre asc");
		try {
			while(inscritos2.next()){
				nombre=inscritos2.getString("nombre");
				String query="select agno_obtencion from db.colegiados where nombre='"+nombre+"'";
				ResultSet queEs=DriverDB.consultaSQL(query);
				queEs.next();
				tipo=queEs.getString("agno_obtencion");
				if (tipo==null){
					precio=inscritos2.getString("precio");
					String a =precio.split(",")[1];
					n=Double.parseDouble(a);
				}else{
					precio=inscritos2.getString("precio");
					String a =precio.split(",")[0];
					n=Double.parseDouble(a);
				}
				inscritos=inscritos+ nombre+" - "+ inscritos2.getString("estado")+" - "+ n +"\n";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(inscritos==null){
			inscritos="No hay inscritos en este curso";
		}
		return inscritos;

	}
	
	public boolean DentroPlazo(String fecha){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
		String a�oa=fecha_inscripcion.split("-")[0];
		int a�o=Integer.parseInt(a�oa);
		String mesa=fecha_inscripcion.split("-")[1];
		int mes=Integer.parseInt(mesa);
		String diaa=fecha_inscripcion.split("-")[2];
		int dia=Integer.parseInt(diaa);
		
		//Saco las fechas:
		String a�o2=fecha.split("-")[0];
		int a�ox=Integer.parseInt(a�o2);
		String mes2=fecha.split("-")[1];
		int mesx=Integer.parseInt(mes2);
		String dia2=fecha.split("-")[2];
		int diax=Integer.parseInt(dia2);
		
		boolean ok=false;
		
		if (a�ox==a�o+1 && mesx==1 && mes==12 && ((dia==31 && (diax==1 || diax==2)) || (dia==30 && diax==1))){ //cambia a�o
			ok=true;
		}else if (a�o==a�ox && mesx==mes+1){ //cambia mes
			if (mes==2 && mesx==3){
				if ((dia==27 && diax==1) || (dia==28 && (diax==1 || diax==2)) || (dia==29 && (diax==1 || diax==2))){
					ok=true;
				}
			}
			if(mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10){
				if ((dia==30 && diax==1)   ||    (dia==31 && (diax==1 || diax==2))){
					ok=true;
				}
			}
			if (mes==4 || mes==6 || mes==9 || mes==11){
				if ((dia==29 && diax==1) ||  (dia==30 && (diax==1 || diax==2))){
					ok=true;
				}
			}
		}else if(a�o==a�ox && mes==mesx){
			if (diax<=dia+2){
				ok=true;
			}
		}else{
			ok=false;
		}
		
		return ok;
		
	}

	public String InscritosExternosACurso(String curso){
		String nombre="";
		String tipo="";
		String inscritos="";
		String precio="";
		double n=0.0;
		ResultSet inscritos2=DriverDB.consultaSQL("select externos.nombre, inscripciones.estado, cursos.precio from db.inscripciones inner join db.externos on inscripciones.id_externo=externos.id_externo inner join db.cursos on inscripciones.id_curso=cursos.id_curso where inscripciones.id_curso="+curso+" order by externos.nombre asc");
		try {
			while(inscritos2.next()){
				nombre=inscritos2.getString("nombre");
				String query="select estudiante from db.externos where nombre='"+nombre+"'";
				ResultSet queEs=DriverDB.consultaSQL(query);
				queEs.next();
				tipo=queEs.getString("estudiante");
				if (tipo.equals("SI")){
					precio=inscritos2.getString("precio");
					String a =precio.split(",")[2];
					n=Double.parseDouble(a);
				}else{
					precio=inscritos2.getString("precio");
					String a =precio.split(",")[3];
					n=Double.parseDouble(a);
				}
				inscritos=inscritos+ nombre+" - "+ inscritos2.getString("estado")+" - "+ n +"\n";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(inscritos.equals("")){
			inscritos="No hay inscritos en este curso";
		}
		return inscritos;

	}


	//Ingresos del curso, el curso tiene el mismo precio para todos, y solo se cuentan como infresos los que ya estan inscritos
	public double ingresos(String curso){
		double beneficios=0.0;
		String total="";
		ResultSet precio=DriverDB.consultaSQL("select precio from cursos where id_curso="+curso);
		try {
			precio.next();
			total=precio.getString("precio");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String c=total.split(",")[0];
		double colegiado=Double.parseDouble(c);
		String p=total.split(",")[1];
		double precolegiado=Double.parseDouble(p);
		String es=total.split(",")[2];
		double estudiante=Double.parseDouble(es);
		String ex=total.split(",")[3];
		double externo=Double.parseDouble(ex);


		ResultSet inscritos=DriverDB.consultaSQL("select * from db.inscripciones where id_curso="+curso+" and estado='inscrito'");
		try {
			while (inscritos.next()){
				String a=inscritos.getString("id_colegiado");
				if(a==null){
					String tipo2=inscritos.getString("id_externo");
					ResultSet estud=DriverDB.consultaSQL("select estudiante from db.externos where id_externo="+tipo2);
					estud.next();
					String preono=estud.getString("estudiante");
					if(preono.equals("SI")){
						beneficios=beneficios+ estudiante;
					}else{ //Es colegiado
						beneficios=beneficios+ externo;
					}
				}else{
					ResultSet preco=DriverDB.consultaSQL("select agno_obtencion from db.colegiados where id_colegiado="+a);
					preco.next();
					String preono=preco.getString("agno_obtencion");
					//Es precolegiado
					if(preono==null){
						beneficios=beneficios+ precolegiado;
					}else{ //Es colegiado
						a=total.split(",")[0];
						beneficios=beneficios+ colegiado;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return beneficios;

	}


}