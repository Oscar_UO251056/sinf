package capa_Logica;

import capa_Datos.DriverDB;
import java.util.LinkedList;
import java.sql.ResultSet;

public class LogicaSesiones {
	
	public static LinkedList<LinkedList<String>> getFormadores() {
		LinkedList<LinkedList<String>> formadores = new LinkedList<LinkedList<String>>();
		
		ResultSet rs = DriverDB.consultaSQL("select id_formador, nombre from formadores;");
		
		try {
			
			while (rs.next()) {
				LinkedList<String> formador = new LinkedList<String>();
				formador.add(rs.getString(1));
				formador.add(rs.getString(2));
				formadores.add(formador);
			}
			
		}catch(Exception e) {
			System.err.printf("ERROR: obteniendo formadores.\n");
		}
		
		return formadores;
	}
	
}
