package capa_Logica;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;

import capa_Datos.DriverDB;

public class EnviarLoteVerificacion {

	//Pasar el metodo a presentacion y meterlo en un modelo
	//asignar el modelo a la Jlist

	//Sacar lista pre-colegiados, pendientes de verificar la titulacion
	public ArrayList<String> ListaPrecolegiados(){
		ArrayList<String> precole = new ArrayList<String>();
		//String precolegiados="";


		ResultSet resultados = DriverDB.consultaSQL("SELECT nombre, agno_obtencion, titulacion, dni FROM colegiados WHERE estado_solicitud='pendiente'");
		if (resultados!=null){
			try {
				while(resultados.next()){
					if (resultados.getString("agno_obtencion")!=null){
					//precolegiados=precolegiados+ "\n" + resultados.getString("nombre");
					String lista=resultados.getString("nombre")+"-"+resultados.getString("dni")+"-"+resultados.getString("titulacion")+"-"+resultados.getString("agno_obtencion");
					precole.add(lista);
					//precole.add(resultados.getString("nombre"));
					}


				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(precole);
		return precole;

	}


	//Enviar lote al ministero (fichero.csv)
	public boolean enviarLote(ArrayList<String> colegiados) {
		ResultSet datos;
		String linea="";
		String Ncolegiado="";

		try{
			FileWriter writer = new FileWriter("LoteMinisterio/LoteMinisterio.csv", false);
			for(int i=0; i<colegiados.size();i++){
				System.out.println(i);
				//Saco los datos de los colegiados a enviar al ministerio
				Ncolegiado=colegiados.get(i);
				System.out.println("El nombre del colegiado que debemos buscar es: "+Ncolegiado);

				String query="SELECT colegiados.id_colegiado, colegiados.titulacion, colegiados.agno_obtencion FROM db.colegiados WHERE colegiados.nombre='"+Ncolegiado+"'";
				datos = DriverDB.consultaSQL(query);
				System.out.println(datos);

				datos.next();


				//Lo metemos en una linea
				String colegiado_o=datos.getString("id_colegiado");
				int id_colegiado=Integer.parseInt(colegiado_o);
				System.out.println("A�adimos al lote al colegiado: "+id_colegiado);

				String titulacion=datos.getString("titulacion");
				System.out.println(titulacion);

				String fecha=datos.getString("agno_obtencion");
				System.out.println(fecha);


				linea=id_colegiado+";"+Ncolegiado+";"+titulacion+";"+fecha+";\n";
				System.out.println(linea);

				//Escribimos en el fichero
				writer.write(linea);
				System.out.println("OK");


			}

			writer.close();

		}catch(IOException e){
			System.out.println("Error E/S: "+e);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return true;
	}


}
