package capa_Logica;

import capa_Datos.DriverDB;
import java.sql.ResultSet;

public class LogicaIntroducirFormador {
	
	public static void nuevoFormador(String dni, String nombre, String titulacion, String rama) {
		String consulta = String.format("insert into formadores (dni, nombre, titulacion, rama_formacion) " +
				"values ('%s', '%s', '%s', '%s');", dni, nombre, titulacion, rama);
		DriverDB.insertSQL(consulta);
	}
	
	public static boolean dniRepetido(String dni) {
		ResultSet rs = DriverDB.consultaSQL("select dni from formadores;");
		try {
			while (rs.next()) {
				if (rs.getString(1).equals(dni))
					return true;
			}
		} catch (Exception e) {
			System.err.printf("ERROR: consultando DNIs de formadores\n");
		}
		
		return false;
	}
	
}
