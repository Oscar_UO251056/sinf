package capa_Logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import capa_Datos.DriverDB;

public class ReciboSocio {

	public static String recibosAnuales(String nsocio){

		String lista = "";
		
		//Comprobar que el numero de socio es correcto
		String query1="SELECT id_socio FROM socios WHERE id_socio="+nsocio;
		ResultSet listaSocios=DriverDB.consultaSQL(query1);

		boolean socio=false;
		
		String Essocio="";
		try {
			listaSocios.next();
			Essocio=listaSocios.getString("id_socio");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("antes");
		//Si es socio, obtener los recibos para el a�o actual
		if (!Essocio.equals("")){
			
			DateFormat df=new SimpleDateFormat("yyyy-mm-dd");
			String fechaActual= df.format(new Date()).toString(); //Fecha actual
			String a�o=fechaActual.split("-")[0];  //A�o actual
			System.out.println(a�o);
			


			//Sacar recibos del socio para el a�o actual. 
			String query= "SELECT id_recibo, cantidad, estado FROM recibos WHERE id_socio="+nsocio+" AND substr(fecha, 1, 4)="+a�o;
			//Para saber si la fecha de recibo es de este a�o
			ResultSet A�oRecibo = DriverDB.consultaSQL(query);
			
			
			if(!A�oRecibo.equals("")){
			try {
				while (A�oRecibo.next()){
					String recibo=A�oRecibo.getString("id_recibo");
					String cantidad=A�oRecibo.getString("cantidad");
					String estado=A�oRecibo.getString("estado");
					
					lista=lista+"Numero de recibo:"+recibo+"  Cantidad: "+cantidad+"    Estado: "+estado;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			else{
				System.out.println("No tiene recibos emitidos");
			}

		}
		else{
			String resultado="No existe el socio";
			return resultado;
		}
	
			
		return lista;
	}

	
	public String Nombre(String socio){
		//Para obtener el Nombre, recibiendo el numero de socio 
		String query="SELECT nombre FROM socios WHERE id_socio="+socio;
		ResultSet nombre=DriverDB.consultaSQL(query);
		

		System.out.printf("'%s'\n", socio);
		String NombreSocio="";
		
		
		if(!nombre.equals("")) {
			try {
				if (nombre.next()) {
					NombreSocio= nombre.getString("nombre"); 
				} 
				else{
					System.out.println("El usuario no esta dado de alta�");
				}
			}
			catch (SQLException e) { e.printStackTrace(); new RuntimeException(e);}

		}


		return NombreSocio;
	}
	
}
