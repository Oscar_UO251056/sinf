package capa_Logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


import capa_Datos.DriverDB;

public class VerificarColegiados {

	//leer del fichero y sacar los DNis
	public static ArrayList<String> leerFichero(String ruta){

		ArrayList<String> ministerio=new ArrayList<String>();
		int flag=-1; //  0=Exito / -1= Esta vacio  / 1=Error de lectura de fichero / 2=Error de SQL / 3= Error de escritura 
		// Ruta a meter por el administrativo
		try(BufferedReader bf = new BufferedReader(new FileReader(ruta))) {
			//StringBuilder sb = new StringBuilder();

			while(true) {


				String line = bf.readLine();
				if (line==null){
					return ministerio;
				}
				System.out.println(line);
				String[] linea = line.split(";");
				//  linea[0]= DNI / linea[1]=Numero indicativo 

				String DNI=linea[0];
				String numero=linea[1];

				System.out.println(DNI);
				System.out.println(numero);

				ministerio.add(line);
				//flag=0; //Ya no esta vacio


			}

		} catch(IOException e1) {
			e1.printStackTrace();
			//return 1; //Error de lectura
		}

		return ministerio;
	}



	public static boolean isCSV(String nombreFichero) {

		int len= nombreFichero.length();
		String extension="";

		for(int i=len-4; i<len ; i++) {	
			extension= extension+ nombreFichero.charAt(i);
		}

		System.out.println(extension);

		if(extension.equals(".csv")) 
			return true; //El fichero es .CSV

		return false; //El fichero no es .CSV
	}

	public static boolean PerteneceBBDD(String DNI){
		ResultSet pertenece1 = DriverDB.consultaSQL("SELECT colegiados.dni, colegiados.agno_obtencion FROM db.colegiados");
		if (pertenece1!=null){
			try {
				while (pertenece1.next()){
					if (DNI.equals(pertenece1.getString("colegiados.dni"))){
						if (pertenece1.getString("agno_obtencion")!=null){
							System.out.println("El DNI "+DNI+" pertenece a la BBDD");
							return true;
						}else{
							System.out.println("No tiene a�o de obtencion");
							return false;
						}

					}else{
						System.out.println("El DNI "+DNI+" no pertenece a la BBDD del COIIPA");

					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return false;

	}


	public static int Clasificar(String resolucion){
		
		
		//Retorna 1 -> Se ha cambiado a inscrito
		//Retorna 2 -> El miembro ya se ha inscrito con anterioridad
		//Retorna 3 -> Se ha cancelado la inscripcion por no haber verificado la titulacion 
		// Retorna 0 -> No pertenece a la BBDD
		String[] linea=resolucion.split("-");
		String DNI=linea[0];
		String numeroID=linea[2];
		int numero=Integer.parseInt(numeroID);

		//Mirar si los DNIs estan en la tabla de colegiados

		if (!PerteneceBBDD(DNI)){
			System.out.println("El usuario NO pertence a la BBDD");
			return 0;
		}

		ResultSet pertenece = DriverDB.consultaSQL("SELECT colegiados.estado_solicitud FROM db.colegiados WHERE colegiados.dni='"+DNI+"'");
		System.out.println(pertenece);
		if (pertenece!=null){
			try {
				pertenece.next();

				if (pertenece.getString("estado_solicitud").equals("pendiente")){
					System.out.println("El estado del colegiado actualmente es pendiente");
					//Si el numero recibido es 1, se cambia a inscrito
					if (numero==1){
						DriverDB.insertSQL("UPDATE colegiados SET estado_solicitud='inscrito' WHERE dni='"+DNI+"'");
						return 1;
					}
					else if (numero==0){
						//A�adir fecha de baja a los que cancelo
						System.out.println("No tiene titulaci�n");
						//Cancelar
						DriverDB.insertSQL("UPDATE colegiados SET estado_solicitud='cancelado' WHERE dni='"+DNI+"'");
						
						
						
						return 3;
					}
					else if (numero==2){
						System.out.println("La titulaci�n no es de Ing.Informatica");
						//Cancelar
						DriverDB.insertSQL("UPDATE colegiados SET estado_solicitud='cancelado' WHERE dni='"+DNI+"'");
						return 3;
					}
				}
				else if(pertenece.getString("estado_solicitud").equals("inscrito")){
					System.out.println("El colegiado con DNI "+DNI+" ya ha sido inscrito con anterioridad");
					return 2;

				}
				else{

					System.out.println("El colegiado con DNI "+DNI+" ha sido cancelado con anterioridad");
					return 3;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return 0;
	}

	public static String Nombre(String DNI){
		ResultSet nombres = DriverDB.consultaSQL("SELECT colegiados.nombre FROM db.colegiados WHERE dni='"+DNI+"'");
		String resultado = null;
		try {
			nombres.next();
			resultado=nombres.getString("nombre");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultado;

	}

}
