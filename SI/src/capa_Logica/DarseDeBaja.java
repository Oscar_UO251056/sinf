package capa_Logica;

import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;

import capa_Datos.DriverDB;

public class DarseDeBaja {
	
	
	

	public static boolean obtenerColegiados(DefaultListModel model,String nom) {


		ResultSet rs=  DriverDB.consultaSQL("SELECT id_colegiado,nombre FROM colegiados WHERE estado_solicitud='inscrito' AND nombre LIKE '%"+nom+"%'");
		if(rs==null) return false;

		try {
			while(rs.next()) {
				model.addElement( rs.getString(1)+":"+rs.getString(2) );
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}



		return true;
	}
	
	
	
	public static boolean comprobarID(String id,JLabel nombre, JLabel titulacion ,JLabel fecha_sol, JLabel listas) {
		
		ResultSet rs= DriverDB.consultaSQL("SELECT nombre,titulacion,fecha_solicitud,dni FROM colegiados WHERE id_colegiado="+id+" AND estado_solicitud='inscrito' AND NOT titulacion=''");
		String dni="";
		
		if(rs==null) {
			return false;
		}
		
		try {
			if(rs.next()) {
				nombre.setText(rs.getString(1));
				titulacion.setText(rs.getString(2));
				fecha_sol.setText(rs.getString(3));
				dni=rs.getString(4);
				listas.setText("--");
				
				ResultSet rs_per= DriverDB.consultaSQL("SELECT id_colegiado FROM peritos WHERE id_colegiado="+id);
				if(rs_per!=null && rs_per.next()) {
					
					listas.setText("-Peritos-");
					
				}
				
				ResultSet rs_for= DriverDB.consultaSQL("SELECT dni FROM formadores WHERE dni="+dni);
				if(rs_for!=null && rs_for.next()) {
					
					listas.setText(listas.getText()+"-Formadores-");
					
				}
				
				ResultSet rs_ins= DriverDB.consultaSQL("SELECT id_colegiado FROM inscripciones WHERE id_colegiado="+id);
				if(rs_ins!=null && rs_ins.next()) {
					
					listas.setText(listas.getText()+"-Inscripciones-");
					
				}
				
				
				return true;
					
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return false;
		
	}
	

	
	
	public static boolean darBaja(String id) {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha_baja = df.format(new Date()).toString(); // Fecha actual
		String dni="";
		
		ResultSet rs=  DriverDB.consultaSQL("SELECT dni FROM colegiados WHERE id_colegiado="+id);
		if(rs==null) return false;

		try {
			if(rs.next()) {
				dni=rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
		boolean exito = DriverDB.insertSQL("UPDATE colegiados SET estado_solicitud = 'CANCELADO',fecha_baja='"+fecha_baja+"' WHERE id_colegiado="+id+"");
		boolean exito2= DriverDB.insertSQL("DELETE FROM peritos WHERE id_colegiado="+id);
		boolean exito3= DriverDB.insertSQL("DELETE FROM formadores WHERE dni='"+dni+"'");
		boolean exito4= DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_colegiado="+id);
		
		return exito && exito2 && exito3 && exito4;
		
	}
	

}
