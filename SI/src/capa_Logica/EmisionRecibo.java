// HISTORIA 7

package capa_Logica;

import java.io.FileWriter;
import java.sql.ResultSet;

import capa_Datos.DriverDB;

public class EmisionRecibo {

	public static boolean escribirRecibos (String NombreFichero) {

		try (FileWriter writer = new FileWriter("Cobros/"+NombreFichero+".csv",false)) {

			
			ResultSet result = DriverDB.consultaSQL("select id_recibo,fecha,dni,nombre,cuenta_bancaria,cantidad from colegiados,recibos \r\n" + 
					"WHERE colegiados.id_colegiado=recibos.id_colegiado AND NOT recibos.estado='EMITIDO'");
			while(result.next()) {
				
				String linea = result.getString(1)+";"+result.getString(2)+";"+result.getString(3)+
						";"+result.getString(4)+";"+result.getString(5)+";"+result.getString(6)+";\n";
				
				writer.write(linea);
			}
			
			
			writer.close();
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
		boolean update = DriverDB.insertSQL("UPDATE recibos SET estado='EMITIDO' WHERE NOT recibos.estado='Emitido'");
		
		return update;
	}
	
	

}