package capa_Logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JTextArea;

import capa_Datos.DriverDB;

public class RegistroCuotas {

	public static int leerFichero(String ruta) {

		int flag=-1; //  0=Exito / -1= Esta vacio  / 1=Error de lectura de fichero / 2=Error de SQL / 3= Error de escritura 
		try(BufferedReader br = new BufferedReader(new FileReader(ruta))) {
			//StringBuilder sb = new StringBuilder();

			while(true) {
				String line = br.readLine();
				if (line==null) return flag;
				System.out.println(line);
				String[] linea = line.split(";");
				//  linea[0]= id_recibo / linea[1]=estado  /  linea[2]=fecha

				ResultSet rs = DriverDB.consultaSQL("SELECT fecha FROM recibos WHERE id_recibo="+linea[0]);
				if(rs == null) {
					escribirRecibos(line);
					return 2;
					}
		
				rs.next();
				Date date_bd= rs.getDate(1);
				if (date_bd.before(Date.valueOf(linea[2])) ) { //Comprueba las fechas

					boolean sql_upd=DriverDB.insertSQL("UPDATE recibos SET estado='"+linea[1]+"',fecha='"+linea[2]+"' WHERE id_recibo="+linea[0]);	

					if(linea[1].equals("DEVUELTO") && !escribirRecibos(line) ) 
						return 3; //Error al escribir csv de incidencias 
					if(linea.length!=4) {
						escribirRecibos(line);
						return 0;
					}


				}
				flag=0; //Ya no esta vacio

			}

		} catch(IOException e1) {
			e1.printStackTrace();
			return 1; //Error de lectura
		} catch(SQLException e2) {
			e2.printStackTrace();
			return 2;

		}

	}


	public static boolean escribirRecibos (String linea) {

		try (FileWriter writer = new FileWriter("Cobros/Incidencias_Cuotas.csv",true)) {


			writer.write(linea+"\n");
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static boolean isCSV(String nombreFichero) {

		int len= nombreFichero.length();
		String extension="";

		for(int i=len-4; i<len ; i++) {	
			extension= extension+ nombreFichero.charAt(i);
		}

		System.out.println(extension);

		if(extension.equals(".csv")) 
			return true; //El fichero es .CSV

		return false; //El fichero no es .CSV
	}

	public static boolean getRecibos(JTextArea area) {
		try {
			ResultSet rs= DriverDB.consultaSQL("SELECT * FROM recibos");
			if(rs==null) return false;
			while(rs.next()) {


				String linea= rs.getString(1) +";"+rs.getString(2)+";"+ rs.getString(3)+";" + rs.getString(4) +";"+ rs.getString(5)+";\n";
				area.setText(area.getText()+linea);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;			
	}
}


