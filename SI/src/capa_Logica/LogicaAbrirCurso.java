package capa_Logica;
import java.util.LinkedList;
import capa_Datos.DriverDB;
import java.sql.ResultSet;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LogicaAbrirCurso {
	
	private static LinkedList<LinkedList<String>> cursos;
	
	public static void init() {
		cursos = new LinkedList<LinkedList<String>>();
	}
	
	public static LinkedList<LinkedList<String>> getCursos() {
		
		ResultSet rs = DriverDB.consultaSQL("select id_curso, titulo, estado, espacio from cursos");
		cursos.clear();
		try {
			while (rs.next()) {
				if (!rs.getString("estado").equals("abierto")) {
					LinkedList<String> curso = new LinkedList<String>();
					curso.add(rs.getString("id_curso"));
					curso.add(rs.getString("titulo"));
					curso.add(rs.getString("espacio"));
					
					cursos.add(curso);
				}
			}
		} catch (Exception e) {
			System.out.println("ERROR: pidiendo listado de cursos.");
		}
		
		return cursos;
	}
	
	public static void abreCurso(int indiceCurso, Date fechaInicio, Date fechaFin, int plazas) {
		DateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		String update = String.format("update cursos set fecha_inicio_inscripcion='%s', fecha_fin_inscripcion='%s', plazas_maximas=%s, estado='abierto' where id_curso=%s;",
				formateador.format(fechaInicio), formateador.format(fechaFin), plazas, cursos.get(indiceCurso).get(0));
		DriverDB.insertSQL(update);
	}
}
