package capa_Logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import capa_Datos.DriverDB;

public class CancelarInscripcion {
	public CancelarInscripcion(){}
	
	//Saco cursos abiertos
		public String DameCursos(){
			String cursos="";
			ResultSet Cursosabiertos=DriverDB.consultaSQL("select id_curso,titulo from db.cursos");
			try {
				while(Cursosabiertos.next()){
					cursos=cursos+ Cursosabiertos.getString("id_curso")+" - "+ Cursosabiertos.getString("titulo")+"\n";
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(cursos.equals("")){
				cursos="En este momento no hay cursos";
				return cursos;
			}
			return cursos;
		}
		
		//Compruebo que es colegiado/precolegiado
		public boolean Colegiado(String dni){
			String query5="select id_colegiado from db.colegiados where dni='"+dni+"'";
			ResultSet id=DriverDB.consultaSQL(query5);
			try {
				if (id.next()){
					return true;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Es colegiado");
			return false;
		}
		
		//Compruebo que es estudiante/externo
		public boolean Externo(String dni){
			String query5="select id_externo from db.externos where dni='"+dni+"'";
			ResultSet id=DriverDB.consultaSQL(query5);
			try {
				if (id.next()){
					return true;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Es externo");
			return false;
			
		}
		
		public boolean ExisteInscripcion(String dni, String curso){
			boolean ok=false;
			String idC;
			if (this.Colegiado(dni)){ // Si es colegiado, sacamos el id y vemos si existe la inscripcion
				String query5="select id_colegiado from db.colegiados where dni='"+dni+"'";
				ResultSet id=DriverDB.consultaSQL(query5);
				try {
					id.next();
					idC=id.getString("id_colegiado");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String query6="select id_colegiado from db.inscripciones where id_curso="+curso;
				ResultSet Existe=DriverDB.consultaSQL(query6);
				try {
					if (Existe.next()){
						ok=true;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if (this.Externo(dni)){
				String query5 = "select id_externo from db.externos where dni='"+dni+"'";
				ResultSet id=DriverDB.consultaSQL(query5);
				try {
					id.next();
					idC=id.getString("id_externo");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String query6="select id_externo from db.inscripciones where id_curso="+curso;
				ResultSet Existe=DriverDB.consultaSQL(query6);
				try {
					if (Existe.next()){
						ok=true;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (ok){
				System.out.println("Existe inscripcion");
			}else{
				System.out.println("No existe inscripcion");
			}
			return ok;
		}
		public boolean InscripcionYaCancelada(String dni, String curso){
			boolean c=false;
			boolean ex=false;
			String SiCancelada="";
			if (this.ExisteInscripcion(dni, curso)){
				String idC="";
				if (this.Colegiado(dni)){ // Si es colegiado, sacamos el id y vemos si existe la inscripcion
					String query5="select id_colegiado from db.colegiados where dni='"+dni+"'";
					ResultSet id=DriverDB.consultaSQL(query5);
					try {
						id.next();
						idC=id.getString("id_colegiado");
						c=true;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (c==true){
						String query6="select estado from db.inscripciones where id_curso="+curso+" and id_colegiado="+idC;
						ResultSet Existe=DriverDB.consultaSQL(query6);
						try {
							Existe.next();
							SiCancelada=Existe.getString("estado");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (SiCancelada.equals("cancelado")){
							return true;
						}	
					}
				}else if (this.Externo(dni)){
					String query5 = "select id_externo from db.externos where dni='"+dni+"'";
					ResultSet id=DriverDB.consultaSQL(query5);
					try {
						id.next();
						idC=id.getString("id_externo");
						ex=true;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (ex==true){
						String query6="select estado from db.inscripciones where id_curso="+curso+" and id_externo="+idC;
						ResultSet Existe=DriverDB.consultaSQL(query6);
						try {
							Existe.next();
							SiCancelada=Existe.getString("estado");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (SiCancelada.equals("cancelado")){
							return true;
						}
					}
				}else{
					return false;
				}
			}
			return false;
			
		}
		
		public boolean InscripcionPreinscrita(String dni, String curso){
			boolean c=false;
			boolean ex=false;
			String SiCancelada="";
			if (this.ExisteInscripcion(dni, curso)){
				String idC="";
				if (this.Colegiado(dni)){ // Si es colegiado, sacamos el id y vemos si existe la inscripcion
					String query5="select id_colegiado from db.colegiados where dni='"+dni+"'";
					ResultSet id=DriverDB.consultaSQL(query5);
					try {
						id.next();
						idC=id.getString("id_colegiado");
						c=true;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (c==true){
						String query6="select estado from db.inscripciones where id_curso="+curso+" and id_colegiado="+idC;
						ResultSet Existe=DriverDB.consultaSQL(query6);
						try {
							Existe.next();
							SiCancelada=Existe.getString("estado");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (SiCancelada.equals("pre-inscrito")){
							return true;
						}	
					}
				}else if (this.Externo(dni)){
					String query5 = "select id_externo from db.externos where dni='"+dni+"'";
					ResultSet id=DriverDB.consultaSQL(query5);
					try {
						id.next();
						idC=id.getString("id_externo");
						ex=true;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (ex==true){
						String query6="select estado from db.inscripciones where id_curso="+curso+" and id_externo="+idC;
						ResultSet Existe=DriverDB.consultaSQL(query6);
						try {
							Existe.next();
							SiCancelada=Existe.getString("estado");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (SiCancelada.equals("pre-inscrito")){
							return true;
						}
					}
				}else{
					return false;
				}
			}
			return false;
			
		}
		
		public double CancelarInscripcion(String dni, String curso){
			double cantidad=0.0;
			String total="";
			//Saco precio del cursos
			ResultSet precio=DriverDB.consultaSQL("select precio from cursos where id_curso="+curso);
			try {
				precio.next();
				total=precio.getString("precio");
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String c=total.split(",")[0];
			double colegiado=Double.parseDouble(c);
			String p=total.split(",")[1];
			double precolegiado=Double.parseDouble(p);
			String es=total.split(",")[2];
			double estudiante=Double.parseDouble(es);
			String ex=total.split(",")[3];
			double externo=Double.parseDouble(ex);
			
				String idI="";
				String query5="select id_colegiado from db.colegiados where dni='"+dni+"'";
				ResultSet id=DriverDB.consultaSQL(query5);
				try {
					if (id.next()){
						idI=id.getString("id_colegiado");
						DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_colegiado="+idI);
						String query3="select agno_obtencion from db.colegiados where dni='"+dni+"'";
						ResultSet Es_colegiado=DriverDB.consultaSQL(query3);
						if (Es_colegiado.next()){ //Es colegiado
							cantidad = colegiado*0.7;
							DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+cantidad+" �. Inscripci�n cancelada' WHERE id_colegiado="+idI);
						}else{ //Es precolegiado
							cantidad = precolegiado*0.7;
							DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+cantidad+" �. Inscripci�n cancelada' WHERE id_colegiado="+idI);
						}
					}else{
						String query6="select id_externo from db.externos where dni='"+dni+"'";
						ResultSet x=DriverDB.consultaSQL(query6);
						x.next();
						idI=x.getString("id_externo");
						DriverDB.insertSQL("UPDATE inscripciones SET estado='cancelado' WHERE id_externo="+idI);
						String query3="select estudiante from db.externos where dni='"+dni+"'";
						ResultSet Es_estudiante=DriverDB.consultaSQL(query3);
						if (Es_estudiante.next() && Es_estudiante.getString("estudiante").equals("SI")){ //Es estudiante
							cantidad = estudiante*0.7;
							DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+cantidad+" �. Inscripci�n cancelada' WHERE id_externo="+idI);
						}else{ //Es externo
							cantidad = precolegiado*0.7;
							DriverDB.insertSQL("UPDATE inscripciones SET observaciones='Devolver "+cantidad+" �. Inscripci�n cancelada' WHERE id_externo="+idI);
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Cancelada."+cantidad+"� a devolver.");
				return cantidad;
				
		}
		
		public boolean DentroPlazo(String curso){
			boolean ok=false;
			String fecha="";
			String query="select fecha_fin_inscripcion from db.cursos where id_curso="+curso;
			System.out.println(query);
			ResultSet fecha_fin=DriverDB.consultaSQL(query);
			try {
				if (fecha_fin.next()){
					fecha=fecha_fin.getString("fecha_fin_inscripcion");
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String fecha_inscripcion = df.format(new Date()).toString(); // Fecha actual
					String a�oa=fecha_inscripcion.split("-")[0];
					int a�ox=Integer.parseInt(a�oa);
					String mesa=fecha_inscripcion.split("-")[1];
					int mesx=Integer.parseInt(mesa);
					String diaa=fecha_inscripcion.split("-")[2];
					int diax=Integer.parseInt(diaa);
					
					//Saco las fechas:
					String a�o2=fecha.split("-")[0];
					int a�o=Integer.parseInt(a�o2);
					String mes2=fecha.split("-")[1];
					int mes=Integer.parseInt(mes2);
					String dia2=fecha.split("-")[2];
					int dia=Integer.parseInt(dia2);
					
					if (a�ox==a�o+1 && mesx==1 && mes==12 && ((dia==31 && (diax==1 || diax==2)) || (dia==30 && diax==1))){ //cambia a�o
						return true;
					}else if (a�o==a�ox && mesx==mes+1){ //cambia mes
						if (mes==2 && mesx==3){
							if ((dia==27 && diax==1) || (dia==28 && (diax==1 || diax==2)) || (dia==29 && (diax==1 || diax==2))){
								return true;
							}
						}
						if(mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10){
							if ((dia==30 && diax==1)   ||    (dia==31 && (diax==1 || diax==2))){
								return true;
							}
						}
						if (mes==4 || mes==6 || mes==9 || mes==11){
							if ((dia==29 && diax==1) ||  (dia==30 && (diax==1 || diax==2))){
								return true;
							}
						}
					}else if(a�o==a�ox && mes==mesx){
						if (diax<=dia+2){
							return true;
						}
					}else if(a�ox==a�o){
						if (mesx<mes){
							return true;
						}
					}else{
						ok=false;
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
			
		}
		
		
		

}
