package capa_Datos;


import java.sql.*;
import java.util.LinkedList;

public class DriverDB {

	private DriverDB() {}
	
	private static LinkedList<Connection> conexiones = new LinkedList<Connection>();
	

	public  static ResultSet consultaSQL (String consulta) { //Devuelve un ResultSet con la respuesta a la consulta a la BD

		Connection conec=null;
		Statement stmt=null;
		ResultSet result=null;


		try {
			conec= DriverManager.getConnection("jdbc:mysql://93.156.189.209:55555/db?verifyServerCertificate=false&useSSL=false&requireSSL=false","root","SINF2018pl3$");
			conexiones.add(conec);
			System.out.println("Conexion realizada con la BD");

			stmt = conec.createStatement();
			result = stmt.executeQuery(consulta); 

		}catch(Exception e) {
			new RuntimeException(e);
		}

		return result;	
	}



	public static boolean insertSQL (String insertar) { //Inserta en la base de datos

		Connection conec=null;

		try {

			conec = DriverManager.getConnection("jdbc:mysql://93.156.189.209:55555/db?verifyServerCertificate=false&useSSL=false&requireSSL=false","root","SINF2018pl3$");
			System.out.println("Conexion realizada con la BD");

			try(Statement stmt = conec.createStatement() ){
				stmt.execute(insertar);
				stmt.close();

			}catch (Exception e) {
				e.printStackTrace();
				return false; //Retorna falso si existe algun problema al realizar la inserción
			}	

			conec.close();

		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		}

		return true; // Retorna verdadero si no hubo ningun problema
	}
	
	public static void limpiaConexiones() { 
		for (int i = 0; i < conexiones.size(); i++) {
			try {
				conexiones.get(i).close();
			} catch(Exception e) {
				System.err.print("Error limpiando conexiones\n");
			}
		}
		conexiones.clear();
	}


}



